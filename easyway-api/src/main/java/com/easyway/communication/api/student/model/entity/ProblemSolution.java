package com.easyway.communication.api.student.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easyway.communication.api.student.model.entity.ProblemConfig;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.entity.Teacher;

@Entity
@Table(name = "problem_solution")
public class ProblemSolution {

	@Id
	@Column(name = "solution_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long solutionId;

	@Column(name = "sol_by_write")
	private String solutionByWrite;

	@Column(name = "sol_By_Photo1")
	private String solutionByPhoto1;

	@Column(name = "sol_By_Photo2")
	private String solutionByPhoto2;

	@Column(name = "solution_note")
	private String solutionNote;

	@Column(name = "solution_date")
	private Date solutionDate;

	@Column(name = "active_status")
	private int activeStatus;

	@Column(name = "solution_status")
	private int solutionStatus = 2;

	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;

	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;

	@ManyToOne
	@JoinColumn(name = "problem_id")
	private ProblemConfig problemConfig;

	public Long getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(Long solutionId) {
		this.solutionId = solutionId;
	}

	public String getSolutionByWrite() {
		return solutionByWrite;
	}

	public void setSolutionByWrite(String solutionByWrite) {
		this.solutionByWrite = solutionByWrite;
	}

	public String getSolutionByPhoto1() {
		return solutionByPhoto1;
	}

	public void setSolutionByPhoto1(String solutionByPhoto1) {
		this.solutionByPhoto1 = solutionByPhoto1;
	}

	public String getSolutionByPhoto2() {
		return solutionByPhoto2;
	}

	public void setSolutionByPhoto2(String solutionByPhoto2) {
		this.solutionByPhoto2 = solutionByPhoto2;
	}

	public String getSolutionNote() {
		return solutionNote;
	}

	public Date getSolutionDate() {
		return solutionDate;
	}

	public void setSolutionDate(Date solutionDate) {
		this.solutionDate = solutionDate;
	}

	public void setSolutionNote(String solutionNote) {
		this.solutionNote = solutionNote;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ProblemConfig getProblemConfig() {
		return problemConfig;
	}

	public void setProblemConfig(ProblemConfig problemConfig) {
		this.problemConfig = problemConfig;
	}

	public int getSolutionStatus() {
		return solutionStatus;
	}

	public void setSolutionStatus(int solutionStatus) {
		this.solutionStatus = solutionStatus;
	}

}
