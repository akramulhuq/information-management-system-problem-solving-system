package com.easyway.communication.api.teacher.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.teacher.model.entity.Teacher;
import com.easyway.communication.api.teacher.model.entity.Topic;

public interface TopicRepo extends JpaRepository<Topic, Long> {

	List<Topic> findByFolder_ForlderId(Long folderId);

}
