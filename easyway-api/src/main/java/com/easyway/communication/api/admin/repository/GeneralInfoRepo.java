package com.easyway.communication.api.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.admin.model.entity.GeneralInfo;

public interface GeneralInfoRepo extends JpaRepository<GeneralInfo, Long>{

}
