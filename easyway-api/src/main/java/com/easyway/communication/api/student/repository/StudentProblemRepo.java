package com.easyway.communication.api.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.student.model.entity.ProblemConfig;

public interface StudentProblemRepo extends JpaRepository<ProblemConfig, Long> {

}
