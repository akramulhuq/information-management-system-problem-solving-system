package com.easyway.communication.api.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.users.model.entity.UserDetails;

public interface UsersDetailRepository extends JpaRepository<UserDetails, Long> {

	UserDetails findByUsername(String username);

	UserDetails findByTeacher_TeacherId(Long instituteId);

}
