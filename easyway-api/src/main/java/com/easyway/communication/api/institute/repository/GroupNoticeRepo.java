package com.easyway.communication.api.institute.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.institute.model.entity.Group;
import com.easyway.communication.api.institute.model.entity.GroupNotice;

public interface GroupNoticeRepo extends JpaRepository<GroupNotice, Long> {

}
