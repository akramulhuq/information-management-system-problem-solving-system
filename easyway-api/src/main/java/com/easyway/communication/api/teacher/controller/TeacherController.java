package com.easyway.communication.api.teacher.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.common.response.BaseResponse;
import com.easyway.communication.api.student.model.dto.ProblemConfigDTO;
import com.easyway.communication.api.student.model.dto.StudentDTO;
import com.easyway.communication.api.student.service.StudentProblemService;
import com.easyway.communication.api.student.service.StudentService;
import com.easyway.communication.api.teacher.model.dto.FolderDTO;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.teacher.model.dto.TopicDTO;
import com.easyway.communication.api.teacher.service.TeacherService;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;

@RestController
@RequestMapping("teachers/")
public class TeacherController {

	@Autowired
	private TeacherService teacherService;

	@RequestMapping(value = "add/topic/by-teacher", method = RequestMethod.POST)
	public void addTopicByTeacher(@RequestBody TopicDTO topicDTO) {
		teacherService.addTopicByTeacher(topicDTO);
	}

	@RequestMapping(value = "find/topic/by-folder", method = RequestMethod.GET)
	public ResponseEntity<List<TopicDTO>> findTopicByTeacher(@RequestParam Long folderId) {
		return new ResponseEntity<>(teacherService.findTopicByTeacher(folderId), HttpStatus.OK);
	}

	@RequestMapping(value = "find/folder/by-teacher", method = RequestMethod.GET)
	public ResponseEntity<List<FolderDTO>> findAllFolderByTeacherId(@RequestParam Long techerId) {
		return new ResponseEntity<>(teacherService.findAllFolderByTeacherId(techerId), HttpStatus.OK);
	}

	@RequestMapping(value = "create/folder/by-teacher", method = RequestMethod.POST)
	public void createFolderByTeacher(@RequestBody FolderDTO folderDTO, @RequestParam Long teacherId) {
		teacherService.createFolderByTeacher(folderDTO, teacherId);

	}
	
	@RequestMapping(value = "find/teacher-details/by-userName", method = RequestMethod.GET)
	public ResponseEntity<List<TeacherDTO>> findTeacherInfo() {
		return new ResponseEntity<>(teacherService.findTeacherInfo(), HttpStatus.OK);
	}
}
