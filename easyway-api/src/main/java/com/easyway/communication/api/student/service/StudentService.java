package com.easyway.communication.api.student.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.stereotype.Service;

import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.common.enums.Topic;
import com.easyway.communication.api.common.response.BaseResponse;
import com.easyway.communication.api.common.response.CustomMessage;
import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.student.model.dto.StudentDTO;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.student.repository.StudentRepo;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;

import net.minidev.json.writer.BeansMapper.Bean;

@Service
@Transactional
public class StudentService {

	@Autowired
	private StudentRepo studentRepo;

	/**
	 * This method used for find student info and sending response to the
	 * Controller
	 * 
	 * @author habib sumon
	 * @return response of find studentsInfo
	 * @since 3/27/2018
	 */
	public List<StudentDTO> findStudentInfo() {
		List<Student> studentEntities = studentRepo.findAll();
		if (studentEntities != null) {
			return studentEntities.stream().map(student -> copyStudentEntityToDto(student))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	private StudentDTO copyStudentEntityToDto(Student student) {
		StudentDTO studentDTO = new StudentDTO();
		BeanUtils.copyProperties(student, studentDTO);
		return studentDTO;
	}

	public void updateUserDetails(UserDetailsDTO userDetailsDTO) {
		studentRepo.save(provideStudentInfo(userDetailsDTO));
	}

	private Student provideStudentInfo(UserDetailsDTO userDetailsDTO) {
		Student student = new Student();
		BeanUtils.copyProperties(userDetailsDTO.getStudent(), student);
		student.setPhotoName(userDetailsDTO.getStudent().getPhotoName());
		student.setCreateDate(ApplicationUtils.convertStringToDate(userDetailsDTO.getStudent().getStudentName()));
		if (userDetailsDTO.getStudent().getDepartment().getId() != null) {
			student.setDepartment(provideGeneralInfo(userDetailsDTO.getStudent().getDepartment().getId()));
		} else {
			student.setDepartment(null);
		}
		return student;
	}

	private GeneralInfo provideGeneralInfo(Long id) {
		GeneralInfo generalInfo = new GeneralInfo();
		generalInfo.setId(id);
		return generalInfo;
	}

	/**
	 * This method used for save student info and sending response to the
	 * Controller
	 * 
	 * @author habib sumon
	 * @return response of saved studentsInfo
	 * @since 3/27/2018
	 */
	public BaseResponse saveOrUpdateStudentInfo(StudentDTO studentDTO) {
		studentRepo.save(provideStudent(studentDTO));
		return new BaseResponse(Topic.STUDENT.getName() + CustomMessage.SAVE_FAILED_MESSAGE);
	}

	private Student provideStudent(StudentDTO studentDTO) {
		Student studentEntity = new Student();
		BeanUtils.copyProperties(studentDTO, studentEntity);
		if (studentEntity.getStudentId() == null) {
			studentEntity.setStudentName(ApplicationUtils.convertCamelcaseString(studentDTO.getStudentName()));
		}
		return studentEntity;
	}

}
