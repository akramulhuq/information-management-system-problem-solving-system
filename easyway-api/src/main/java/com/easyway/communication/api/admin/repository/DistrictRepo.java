package com.easyway.communication.api.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.address.entity.District;

public interface DistrictRepo extends JpaRepository<District, Long> {

}
