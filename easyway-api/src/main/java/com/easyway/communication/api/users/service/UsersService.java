package com.easyway.communication.api.users.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.hibernate.mapping.Table;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easyway.communication.api.address.entity.District;
import com.easyway.communication.api.common.enums.SignUpAs;
import com.easyway.communication.api.common.enums.StatusId;
import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.institute.repository.InstituteRepo;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.student.repository.StudentRepo;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.teacher.model.entity.Teacher;
import com.easyway.communication.api.teacher.repository.TeacherRepo;
import com.easyway.communication.api.users.model.dto.SignUpDTO;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;
import com.easyway.communication.api.users.model.dto.UsersDTO;
import com.easyway.communication.api.users.model.dto.UsersRoleDTO;
import com.easyway.communication.api.users.model.entity.SignUp;
import com.easyway.communication.api.users.model.entity.UserDetails;
import com.easyway.communication.api.users.model.entity.Users;
import com.easyway.communication.api.users.model.entity.UsersRole;
import com.easyway.communication.api.users.repository.SignUpRepository;
import com.easyway.communication.api.users.repository.UsersDetailRepository;
import com.easyway.communication.api.users.repository.UsersRepository;
import com.easyway.communication.api.users.repository.UsersRoleRepository;

@Service
@Transactional
public class UsersService {

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private UsersRoleRepository usersRoleRepository;

	@Autowired
	private UsersDetailRepository userDetailRepository;

	@Autowired
	private SignUpRepository signUpRepository;

	@Autowired
	private StudentRepo studentRepo;

	@Autowired
	private InstituteRepo instituteRepo;

	@Autowired
	private TeacherRepo teacherRepo;

	public void adminRegistration(SignUpDTO signUpDTO, int signUpAs) {
		if (SignUpAs.ADMIN.getCode() == signUpAs) {
			signUpDTO.setName("Admineasy Link");
			usersRepository.save(provideSignedUpUser(signUpDTO));
			usersRoleRepository.save(provideSignedUpUserRole(signUpDTO, signUpAs));
			userDetailRepository.save(provideUsersDetals(signUpDTO, signUpAs));
		} else if (SignUpAs.INSTITUTE.getCode() == signUpAs) {
			signUpRepository.save(provideInstituteInfo(signUpDTO, signUpAs));
		} else if (SignUpAs.STUDENT.getCode() == signUpAs) {
			Student student = studentRepo.save(provideStudentInfo(signUpDTO, signUpAs));
			signUpDTO.setUserName(signUpDTO.getEmail());
			usersRepository.save(provideSignedUpUser(signUpDTO));
			usersRoleRepository.save(provideSignedUpUserRole(signUpDTO, signUpAs));
			userDetailRepository.save(provideUsersDetals(signUpDTO, signUpAs, student));
		}
	}

	public void approveSingleUsers(SignUpDTO signUpDTO) {
		Institute institute = instituteRepo.save(provideInstituteInfo(signUpDTO));
		usersRepository.save(provideSignedUpUser(signUpDTO));
		usersRoleRepository.save(provideSignedUpUserRole(signUpDTO, SignUpAs.INSTITUTE.getCode()));
		userDetailRepository.save(provideUsersDetals(signUpDTO, SignUpAs.INSTITUTE.getCode(), institute));
		signUpRepository.save(provideSignUp(signUpDTO));
	}

	private SignUp provideSignUp(SignUpDTO signUpDTO) {
		SignUp signUp = signUpRepository.findBySignUpId(signUpDTO.getSignUpId());
		signUp.setApproveStatus(StatusId.ACTIVE.getId());
		return signUp;
	}

	private UserDetails provideUsersDetals(SignUpDTO signUpDTO, int signUpAs, Institute institute) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserType(ApplicationUtils.provideUsersType(signUpAs));
		userDetails.setUsername(signUpDTO.getUserName());
		userDetails.setInstitute(provideInstituteId(institute.getInstituteId()));
		return userDetails;
	}

	private Institute provideInstituteId(Long instituteId) {
		Institute institute = new Institute();
		institute.setInstituteId(instituteId);
		return institute;
	}

	private Institute provideInstituteInfo(SignUpDTO signUpDTO) {
		Institute institute = new Institute();
		institute.setContactNo(signUpDTO.getContactNo());
		institute.setContactEmail(signUpDTO.getEmail());
		institute.setInstituteName(signUpDTO.getName());
		institute.setAddress(signUpDTO.getPresentAddress());
		institute.setDistrict(provideDistrictId(signUpDTO.getDistrict().getDistrictId()));
		return institute;
	}

	private District provideDistrictId(long districtId) {
		District district = new District();
		district.setDistrictId(districtId);
		return district;
	}

	private UserDetails provideUsersDetals(SignUpDTO signUpDTO, int signUpAs, Student student) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserType(ApplicationUtils.provideUsersType(signUpAs));
		userDetails.setUsername(signUpDTO.getUserName());
		userDetails.setStudent(provideStudent(student.getStudentId()));
		return userDetails;
	}

	private Student provideStudent(Long studentId) {
		Student student = new Student();
		student.setStudentId(studentId);
		return student;
	}

	private Student provideStudentInfo(SignUpDTO signUpDTO, int signUpAs) {
		Student student = new Student();
		student.setStudentName(ApplicationUtils.convertCamelcaseString(signUpDTO.getName()));
		student.setEmail(signUpDTO.getEmail());
		student.setBirthDate(signUpDTO.getBirthDate());
		student.setGender(signUpDTO.getGender());
		student.setCreateDate(new Date());
		student.setInstitute(provideInstituteId(signUpDTO.getInstituteId()));
		return student;
	}

	private SignUp provideInstituteInfo(SignUpDTO signUpDTO, int signUpAs) {
		SignUp signUp = new SignUp();
		BeanUtils.copyProperties(signUpDTO, signUp);
		signUp.setSignedUpDate(new Date());
		signUp.setSignedUpAs(signUpAs);
		signUp.setApproveStatus(StatusId.INACTIVE.getId());
		signUp.setUserName(signUpDTO.getEmail());
		return signUp;
	}

	private UserDetails provideUsersDetals(SignUpDTO signUpDTO, int signUpAs) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserType(ApplicationUtils.provideUsersType(signUpAs));
		userDetails.setUsername(signUpDTO.getUserName());
		return userDetails;
	}

	public UsersRole provideSignedUpUserRole(SignUpDTO signUpDTO, int signupAs) {
		UsersRole userRole = new UsersRole();
		userRole.setRoleName(ApplicationUtils.provideSignedUpUserRole(signupAs));
		userRole.setUsername(signUpDTO.getUserName());
		return userRole;
	}

	public Users provideSignedUpUser(SignUpDTO signUpDTO) {
		Users user = new Users();
		user.setUsername(signUpDTO.getUserName());
		user.setPassword(signUpDTO.getPassword());
		user.setNickName(signUpDTO.getName());
		user.setEnabled(true);
		return user;
	}

	public UsersDTO findLoginUser(String username, String password) {
		Users user = usersRepository.findByUsernameAndPasswordAndEnabled(username, password, true);
		if (user != null) {
			UsersDTO userDTO = copyUserToUserDto(user);
			return userDTO;
		} else {
			return null;
		}
	}

	public UsersDTO copyUserToUserDto(Users user) {
		UsersDTO userDTO = new UsersDTO();
		BeanUtils.copyProperties(user, userDTO);
		return userDTO;

	}

	public UsersDTO findLoginUserByUsername(String username) {
		Users user = usersRepository.findByUsernameAndEnabled(username, true);
		if (user != null) {
			UsersDTO userDTO = copyUserToUserDto(user);
			return userDTO;
		} else {
			return null;
		}
	}

	public List<TeacherDTO> findInstituteIdByTeacher(Long instituteId) {
		List<Teacher> teachers = teacherRepo.findByInstitute_instituteId(instituteId);
		if (teachers != null) {
			List<TeacherDTO> teacherDTOs = new ArrayList<>();
			for (Teacher teacher : teachers) {
				TeacherDTO teacherDTO = new TeacherDTO();
				BeanUtils.copyProperties(teacher, teacherDTO);
				teacherDTOs.add(teacherDTO);
			}
			return teacherDTOs;
		} else {
			return new ArrayList<>();
		}
	}

	public UserDetailsDTO findUserDetailsOfLogingUser(String username) {
		UserDetails userDetails = userDetailRepository.findByUsername(username);
		UserDetailsDTO userDetailsDTO = null;
		if (userDetails != null) {
			userDetailsDTO = new UserDetailsDTO();
			BeanUtils.copyProperties(userDetails, userDetailsDTO);
		}

		return userDetailsDTO;
	}

	public List<UsersRoleDTO> findUserRolesByUsername(String username) {
		List<UsersRole> roles = new ArrayList<>();
		roles = usersRoleRepository.findByUsername(username);
		if (roles != null) {
			return roles.stream().map(role -> copyUserRoleToUserRoleDTO(role)).collect(Collectors.toList());
		}
		return null;
	}

	public UsersRoleDTO copyUserRoleToUserRoleDTO(UsersRole userRole) {

		UsersRoleDTO usersRoleDTO = new UsersRoleDTO();
		BeanUtils.copyProperties(userRole, usersRoleDTO);
		return usersRoleDTO;
	}

	public Users findLoginUser(String username) {
		Users user = usersRepository.findByUsername(username);
		return user;
	}

	public List<SignUpDTO> findAllPendingUsers() {
		List<SignUp> pendingUsers = signUpRepository.findByApproveStatusOrderBySignedUpDate(0);
		if (pendingUsers != null) {
			return pendingUsers.stream().map(signUp -> copySignUpToSignUpDTO(signUp)).collect(Collectors.toList());
		} else {
			return null;
		}
	}

	public SignUpDTO copySignUpToSignUpDTO(SignUp signUp) {
		SignUpDTO signUpDTO = new SignUpDTO();
		BeanUtils.copyProperties(signUp, signUpDTO);
		signUpDTO.setHospitalName(signUp.getName());
		signUpDTO.setPresentAddress(signUp.getPresentAddress());
		signUpDTO.setUserName(signUp.getUserName());
		signUpDTO.setSignedUpDate(signUp.getSignedUpDate());
		return signUpDTO;
	}
}
