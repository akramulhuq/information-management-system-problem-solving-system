package com.easyway.communication.api.address.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "district")
public class District implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "district_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long districtId;

	@Column(name = "district_name")
	private String districtName;

	@ManyToOne
	@JoinColumn(name = "division_id")
	private Division division;

	@Column(name = "note")
	private String note;

	public long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
