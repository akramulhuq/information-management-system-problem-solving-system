package com.easyway.communication.api.institute.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.easyway.communication.api.institute.model.entity.Institute;

public interface InstituteRepo extends JpaRepository<Institute, Long> {

}
