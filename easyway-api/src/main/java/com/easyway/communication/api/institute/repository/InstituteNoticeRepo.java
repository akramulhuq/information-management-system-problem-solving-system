package com.easyway.communication.api.institute.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.easyway.communication.api.institute.model.entity.InstituteNotice;

public interface InstituteNoticeRepo extends JpaRepository<InstituteNotice, Long> {

	List<InstituteNotice> findByInstitute_InstituteId(Long instituteId);

}
