package com.easyway.communication.api.student.model.dto;

import java.util.Date;

import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.institute.model.entity.Institute;

public class StudentDTO {

	private Long studentId;

	private String studentName;

	private int studentRoll;

	private String registrationId;

	private String photoName;

	private String batchName;

	private String birthDate;

	private String contactNumber;

	private String email;

	private String bloodGroup;

	private String expertiesSubject;

	private Date createDate;

	private GeneralInfo department;

	private Institute institute;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(int studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getExpertiesSubject() {
		return expertiesSubject;
	}

	public void setExpertiesSubject(String expertiesSubject) {
		this.expertiesSubject = expertiesSubject;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public GeneralInfo getDepartment() {
		return department;
	}

	public void setDepartment(GeneralInfo department) {
		this.department = department;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Institute etInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
