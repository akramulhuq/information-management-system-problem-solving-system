package com.easyway.communication.api.institute.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.easyway.communication.api.institute.model.entity.Institute;

@Entity
@Table(name = "group_name")
public class Group {

	@Id
	@Column(name = "group_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long groupId;

	@Column(name = "group_name")
	private String groupName;

	@Column(name = "group_title")
	private String groupTitle;

	@Column(name = "active_status")
	private int activeStatus;

	@Column(name = "like_as")
	private int likeAs;

	@ManyToOne
	@JoinColumn(name = "institute_id")
	private Institute institute;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupTitle() {
		return groupTitle;
	}

	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public int getLikeAs() {
		return likeAs;
	}

	public void setLikeAs(int likeAs) {
		this.likeAs = likeAs;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
