package com.easyway.communication.api.address.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "division")
public class Division implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "division_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long divisionId;

	@Column(name = "division_name")
	private String divisionName;

	@Column(name = "note")
	private String note;

	public Long getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
