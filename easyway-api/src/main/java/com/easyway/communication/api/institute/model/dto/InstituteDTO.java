package com.easyway.communication.api.institute.model.dto;

import com.easyway.communication.api.address.entity.District;

public class InstituteDTO {

	private Long instituteId;

	private String instituteName;

	private String articaalOfInstitute;

	private String contactNo;

	private String contactEmail;

	private String coverPhoto;

	private String logoPhoto;

	private String address;

	private District district;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getArticaalOfInstitute() {
		return articaalOfInstitute;
	}

	public void setArticaalOfInstitute(String articaalOfInstitute) {
		this.articaalOfInstitute = articaalOfInstitute;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCoverPhoto() {
		return coverPhoto;
	}

	public void setCoverPhoto(String coverPhoto) {
		this.coverPhoto = coverPhoto;
	}

	public String getLogoPhoto() {
		return logoPhoto;
	}

	public void setLogoPhoto(String logoPhoto) {
		this.logoPhoto = logoPhoto;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public District getDistrict() {
		if (district == null)
			district = new District();
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

}
