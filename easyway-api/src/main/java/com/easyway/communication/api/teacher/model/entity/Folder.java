package com.easyway.communication.api.teacher.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easyway.communication.api.teacher.model.entity.Teacher;

@Entity
@Table(name = "folder")
public class Folder {

	@Id
	@Column(name = "forlder_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long forlderId;

	@Column(name = "folder_name")
	private String folderName;

	@Column(name = "create_date")
	private Date createDate;

	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;

	public Long getForlderId() {
		return forlderId;
	}

	public void setForlderId(Long forlderId) {
		this.forlderId = forlderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
