package com.easyway.communication.api.common.enums;

public enum StatusId {

	/* used for doctor configuration */
	INACTIVE(0), ACTIVE(1), WAITING(2), DEACTIVATE(3),
	
	/* used for student problem maintain */
	DELETE_POST(0), ACTIVE_POST(1);
	
	private final int Id;

	private StatusId(int Id) {
		this.Id = Id;
	}

	public int getId() {
		return Id;
	}

}
