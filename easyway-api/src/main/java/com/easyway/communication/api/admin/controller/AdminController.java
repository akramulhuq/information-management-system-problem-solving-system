package com.easyway.communication.api.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.address.dto.DistrictDTO;
import com.easyway.communication.api.address.dto.DivisionDTO;
import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.admin.service.AdminService;

@RestController
@RequestMapping("admins/")
public class AdminController {

	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "save/division", method = RequestMethod.POST)
	public void saveOrEditDivision(@RequestBody DivisionDTO divisionDTO) {
		adminService.saveOrEditDivision(divisionDTO);
	}

	@RequestMapping(value = "save/district", method = RequestMethod.POST)
	public void saveOrEditDistrict(@RequestBody DistrictDTO districtDTO) {
		adminService.saveOrEditDistrict(districtDTO);
	}

	@RequestMapping(value = "find/all/division", method = RequestMethod.GET)
	public ResponseEntity<List<DivisionDTO>> findDivisionsList() {
		return new ResponseEntity<>(adminService.findDivisionsList(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/all/district", method = RequestMethod.GET)
	public ResponseEntity<List<DistrictDTO>> findDistrictsList() {
		return new ResponseEntity<>(adminService.findDistrictsList(), HttpStatus.OK);

	}
	@RequestMapping(value="save/generalinfo", method = RequestMethod.POST)
	public void saveGeneralInfo(@RequestBody GeneralInfoDTO generalInfoDTO) {
		adminService.saveGeneralInfo(generalInfoDTO);
	}

	@RequestMapping(value = "find/all/generalinfo", method = RequestMethod.GET)
	public ResponseEntity<List<GeneralInfoDTO>> findGeneralInfoList(){
		return new ResponseEntity<>(adminService.findGeneralInfoList(), HttpStatus.OK);
	}
}
