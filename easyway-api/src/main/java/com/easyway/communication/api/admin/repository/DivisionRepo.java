package com.easyway.communication.api.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.easyway.communication.api.address.entity.Division;

public interface DivisionRepo extends JpaRepository<Division, Long> {

}
