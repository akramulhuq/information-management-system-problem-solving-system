package com.easyway.communication.api.institute.model.dto;

import com.easyway.communication.api.institute.model.entity.Group;
import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.entity.Teacher;

public class GroupNoticeDTO {

	private Long noticeId;

	private String noticType;

	private String noticeDate;

	private String noticeTitle;

	private String noticeBody;

	private String noticeWithPhoto;

	private int activeStatus;

	private Teacher teacher;

	private Institute institute;

	private Student student;

	private Group group;

	private String noticePhotoBase64;

	public Long getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getNoticType() {
		return noticType;
	}

	public void setNoticType(String noticType) {
		this.noticType = noticType;
	}

	public String getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeBody() {
		return noticeBody;
	}

	public void setNoticeBody(String noticeBody) {
		this.noticeBody = noticeBody;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public String getNoticeWithPhoto() {
		return noticeWithPhoto;
	}

	public void setNoticeWithPhoto(String noticeWithPhoto) {
		this.noticeWithPhoto = noticeWithPhoto;
	}

	public String getNoticePhotoBase64() {
		return noticePhotoBase64;
	}

	public void setNoticePhotoBase64(String noticePhotoBase64) {
		this.noticePhotoBase64 = noticePhotoBase64;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
