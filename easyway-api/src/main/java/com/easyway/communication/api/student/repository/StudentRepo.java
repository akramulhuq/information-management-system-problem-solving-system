package com.easyway.communication.api.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.student.model.entity.Student;

public interface StudentRepo extends JpaRepository<Student, Long> {

}
