package com.easyway.communication.api.teacher.model.dto;

import java.util.Date;

import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.institute.repository.InstituteNoticeRepo;

public class TeacherDTO {

	private Long teacherId;

	private String teacherName;

	private GeneralInfo designation;

	private GeneralInfo department;

	private String registrationId;

	private String qualification;

	private String contactNumber;

	private String email;

	private String photoName;

	private String address;

	private String gender;

	private String birthDate;

	private Date createDate;

	private String userName;

	private String password;

	private GeneralInfoDTO generalInfoDTO;

	private Institute institute;

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public GeneralInfo getDesignation() {
		return designation;
	}

	public void setDesignation(GeneralInfo designation) {
		this.designation = designation;
	}

	public GeneralInfo getDepartment() {
		return department;
	}

	public void setDepartment(GeneralInfo department) {
		this.department = department;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public GeneralInfoDTO getGeneralInfoDTO() {
		return generalInfoDTO;
	}

	public void setGeneralInfoDTO(GeneralInfoDTO generalInfoDTO) {
		this.generalInfoDTO = generalInfoDTO;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
