package com.easyway.communication.api.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.easyway.communication.api.student.model.entity.ProblemSolution;

public interface ProblemSolutionRepo extends JpaRepository<ProblemSolution, Long> {

	List<ProblemSolution> findByProblemConfig_ProblemIdAndActiveStatus(Long problemId, int id);

	List<ProblemSolution> findByActiveStatus(int id);

	List<ProblemSolution> findByactiveStatus(int id);

	ProblemSolution findBySolutionId(Long solutionId);

}
