package com.easyway.communication.api.student.model.dto;

import com.easyway.communication.api.institute.model.entity.Group;
import com.easyway.communication.api.student.model.entity.Student;

public class GroupConfigDTO {

	private Long configId;

	private String addDate;

	private int joinAs;

	private Student student;

	private Group group;

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public int getJoinAs() {
		return joinAs;
	}

	public void setJoinAs(int joinAs) {
		this.joinAs = joinAs;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
