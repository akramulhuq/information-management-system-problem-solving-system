package com.easyway.communication.api.institute.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easyway.communication.api.teacher.model.entity.Teacher;

@Entity
@Table(name = "Institute_notice")
public class InstituteNotice {

	@Id
	@Column(name = "notice_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long noticeId;

	@Column(name = "notic_type")
	private String noticType;

	@Column(name = "notice_date")
	private Date noticeDate;

	@Column(name = "notice_title")
	private String noticeTitle;

	@Column(name = "notice_body")
	private String noticeBody;

	@Column(name = "notice_with_photo")
	private String noticeWithPhoto;

	@Column(name = "active_status")
	private int activeStatus;

	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;

	@ManyToOne
	@JoinColumn(name = "institute_id")
	private Institute institute;

	public Long getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getNoticType() {
		return noticType;
	}

	public void setNoticType(String noticType) {
		this.noticType = noticType;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeBody() {
		return noticeBody;
	}

	public void setNoticeBody(String noticeBody) {
		this.noticeBody = noticeBody;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public String getNoticeWithPhoto() {
		return noticeWithPhoto;
	}

	public void setNoticeWithPhoto(String noticeWithPhoto) {
		this.noticeWithPhoto = noticeWithPhoto;
	}

}
