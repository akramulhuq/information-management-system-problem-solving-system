package com.easyway.communication.api.institute.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.institute.model.entity.Group;

public interface GroupRepo extends JpaRepository<Group, Long> {

}
