package com.easyway.communication.api.institute.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.institute.model.dto.GroupDTO;
import com.easyway.communication.api.institute.model.dto.GroupNoticeDTO;
import com.easyway.communication.api.institute.model.dto.InstituteDTO;
import com.easyway.communication.api.institute.model.dto.InstituteNoticeDTO;
import com.easyway.communication.api.institute.service.InstituteInfoService;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;

@RestController
@RequestMapping("institutes/")
public class InstituteInfoController {

	@Autowired
	private InstituteInfoService instituteInfoService;

	@RequestMapping(value = "find/all/group/notice", method = RequestMethod.GET)
	public ResponseEntity<List<GroupNoticeDTO>> findAllGroupNotice() {
		return new ResponseEntity<>(instituteInfoService.findAllGroupNotice(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/all/group", method = RequestMethod.GET)
	public ResponseEntity<List<GroupDTO>> findAllGroup() {
		return new ResponseEntity<>(instituteInfoService.findAllGroup(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/all/institute-info", method = RequestMethod.GET)
	public ResponseEntity<List<InstituteDTO>> findAllInstituteInfo() {
		return new ResponseEntity<>(instituteInfoService.findAllInstituteInfo(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/all/institute/notice", method = RequestMethod.GET)
	public ResponseEntity<List<InstituteNoticeDTO>> findInstituteAllNotices() {
		return new ResponseEntity<>(instituteInfoService.findInstituteAllNotices(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/institute/notice/by-id", method = RequestMethod.GET)
	public ResponseEntity<List<InstituteNoticeDTO>> findInstituteNoticeById(@RequestParam Long instituteId) {
		return new ResponseEntity<>(instituteInfoService.findInstituteNoticeById(instituteId), HttpStatus.OK);
	}

	@RequestMapping(value = "post/group/notice/with-out/images", method = RequestMethod.POST)
	public ResponseEntity<GroupNoticeDTO> saveGrpNoticeWithOutPhto(@RequestBody GroupNoticeDTO groupNoticeDTO) {
		return new ResponseEntity<>(instituteInfoService.saveGrpNoticeWithOutPhto(groupNoticeDTO), HttpStatus.OK);
	}

	@RequestMapping(value = "post/group/notice/with-out/images/by-student", method = RequestMethod.POST)
	public ResponseEntity<GroupNoticeDTO> saveGrpNoticeWithOutPhtoByStdnt(@RequestBody GroupNoticeDTO groupNoticeDTO) {
		return new ResponseEntity<>(instituteInfoService.saveGrpNoticeWithOutPhto(groupNoticeDTO,
				groupNoticeDTO.getStudent().getStudentId()), HttpStatus.OK);

	}

	@RequestMapping(value = "post/group/notice", method = RequestMethod.POST)
	public void postGroupNotice(@RequestBody GroupNoticeDTO groupNoticeDTO) {
		instituteInfoService.postGroupNotice(groupNoticeDTO);
	}

	@RequestMapping(value = "post/group/notice/by-student", method = RequestMethod.POST)
	public void postGroupNoticeByStudent(@RequestBody GroupNoticeDTO groupNoticeDTO) {
		instituteInfoService.postGroupNotice(groupNoticeDTO, groupNoticeDTO.getStudent().getStudentId());
	}

	@RequestMapping(value = "create/group", method = RequestMethod.POST)
	public void postNoticeByInstitute(@RequestBody GroupDTO groupDTO) {
		instituteInfoService.postNoticeByInstitute(groupDTO);
	}

	@RequestMapping(value = "post/institute/notice/by-id", method = RequestMethod.POST)
	public ResponseEntity<InstituteNoticeDTO> postNoticeByInstitute(
			@RequestBody InstituteNoticeDTO instituteNoticeDTO) {
		return new ResponseEntity<>(instituteInfoService.postNoticeByInstitute(instituteNoticeDTO), HttpStatus.OK);
	}

	@RequestMapping(value = "save/teacherinfo", method = RequestMethod.POST)
	public ResponseEntity<TeacherDTO> saveTeacher(@RequestBody TeacherDTO teacherDTO, int signUpAs) {
		return new ResponseEntity<>(instituteInfoService.saveTeacher(teacherDTO, signUpAs), HttpStatus.OK);
	}

	@RequestMapping(value = "update/institute", method = RequestMethod.POST)
	public void updateInstituteInfo(@RequestBody UserDetailsDTO userDetailsDTO) {
		instituteInfoService.updateInstituteInfo(userDetailsDTO);
	}

	@RequestMapping(value = "find/all/teacher-info", method = RequestMethod.GET)
	public ResponseEntity<List<TeacherDTO>> findAllTeacherInfo() {
		return new ResponseEntity<List<TeacherDTO>>(instituteInfoService.findAllTeacherInfo(), HttpStatus.OK);
	}

}
