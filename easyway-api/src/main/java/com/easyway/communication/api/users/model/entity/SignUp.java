package com.easyway.communication.api.users.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.easyway.communication.api.address.entity.District;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;

@Entity
@Table(name = "sign_up", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "branch_name" }))
public class SignUp {
	@Id
	@Column(name = "signup_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long signUpId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "name")
	private String name;

	@Column(name = "branch_name")
	private String branchName;

	@Column(name = "contact_no")
	private String contactNo;

	@Column(name = "email")
	private String email;

	@Column(name = "present_Address")
	private String presentAddress;

	@Column(name = "gender")
	private String gender;

	@Column(name = "birth_date")
	private String birthDate;

	@Column(name = "signed_up_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date signedUpDate;

	@Column(name = "signedup_as")
	private int signedUpAs = 0;

	@Column(name = "approve_status")
	private int approveStatus = 0;

	@ManyToOne
	@JoinColumn(name = "district_id")
	private District district;

	public Long getSignUpId() {
		return signUpId;
	}

	public void setSignUpId(Long signUpId) {
		this.signUpId = signUpId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Date getSignedUpDate() {
		return signedUpDate;
	}

	public void setSignedUpDate(Date signedUpDate) {
		this.signedUpDate = signedUpDate;
	}

	public int getSignedUpAs() {
		return signedUpAs;
	}

	public void setSignedUpAs(int signedUpAs) {
		this.signedUpAs = signedUpAs;
	}

	public int getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

}
