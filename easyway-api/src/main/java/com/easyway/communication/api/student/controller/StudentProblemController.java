package com.easyway.communication.api.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.institute.model.dto.GroupDTO;
import com.easyway.communication.api.student.model.dto.GroupConfigDTO;
import com.easyway.communication.api.student.model.dto.ProblemConfigDTO;
import com.easyway.communication.api.student.model.dto.ProblemSolutionDTO;
import com.easyway.communication.api.student.service.StudentProblemService;

@RestController
@RequestMapping("students/problem/")
public class StudentProblemController {

	@Autowired
	private StudentProblemService studentProblemService;

	@RequestMapping(value = "find/all-problem", method = RequestMethod.GET)
	public ResponseEntity<List<ProblemConfigDTO>> findAllProblems() {
		return new ResponseEntity<>(studentProblemService.findAllProblems(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/all/active/problem-solution", method = RequestMethod.GET)
	public ResponseEntity<List<ProblemSolutionDTO>> findAllActiveProblemSolution() {
		return new ResponseEntity<>(studentProblemService.findAllActiveProblemSolution(), HttpStatus.OK);
	}

	@RequestMapping(value = "find/solution/by-problemId", method = RequestMethod.GET)
	public ResponseEntity<List<ProblemSolutionDTO>> findProblemSolutionByPrId(@RequestParam Long problemId) {
		return new ResponseEntity<>(studentProblemService.findProblemSolutionByPrId(problemId), HttpStatus.OK);
	}
	
	@RequestMapping(value = "find/all/group-config", method = RequestMethod.GET)
	public ResponseEntity<List<GroupConfigDTO>> findAllGroupConfig() {
		return new ResponseEntity<>(studentProblemService.findAllGroupConfig(), HttpStatus.OK);
	}

	@RequestMapping(value = "post/problem/by-student", method = RequestMethod.POST)
	public ProblemConfigDTO postProblemByStudent(@RequestBody ProblemConfigDTO problemConfigDTO) {
		return studentProblemService.postProblemByStudent(problemConfigDTO);
	}

	@RequestMapping(value = "joinOrUnJoin/gropu/by-student", method = RequestMethod.POST)
	public void joinOrUnJoinGroup(@RequestBody GroupDTO groupDTO, @RequestParam int likeAs,
			@RequestParam Long studentId) {
		studentProblemService.joinOrUnJoinGroup(groupDTO, likeAs, studentId);
	}

	@RequestMapping(value = "like/student/solution/problem", method = RequestMethod.POST)
	public void problmSolutionLikeByStdnt(@RequestBody ProblemSolutionDTO problemSolutionDTO,
			@RequestParam int likeAs) {
		studentProblemService.problmSolutionLikeByStdnt(problemSolutionDTO, likeAs);
	}

	@RequestMapping(value = "solution/problem/by-student", method = RequestMethod.POST)
	public void problemSolutionByStudent(@RequestBody ProblemSolutionDTO problemSolutionDTO) {
		studentProblemService.problemSolutionByStudent(problemSolutionDTO);
	}
}
