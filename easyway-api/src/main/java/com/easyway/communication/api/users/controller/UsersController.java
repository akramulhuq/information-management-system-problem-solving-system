package com.easyway.communication.api.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.common.response.BaseResponse;
import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.users.model.dto.SignUpDTO;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;
import com.easyway.communication.api.users.model.dto.UsersDTO;
import com.easyway.communication.api.users.model.dto.UsersRoleDTO;
import com.easyway.communication.api.users.model.entity.Users;
import com.easyway.communication.api.users.service.UsersService;

@RestController
@RequestMapping("users/")
public class UsersController {

	@Autowired
	private UsersService usersService;

	@RequestMapping(value = "registration/admin-user", method = RequestMethod.POST)
	public void adminRegistration(@RequestBody SignUpDTO signUpDTO, @RequestParam int signUpAs) {
		usersService.adminRegistration(signUpDTO, signUpAs);
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public UsersDTO findLoginUser(@RequestParam String username, @RequestParam String password) {
		UsersDTO userDTO = usersService.findLoginUser(username, password);
		return userDTO;

	}

	@RequestMapping(value = "/user/by-username", method = RequestMethod.GET)
	public UsersDTO findLoginUserByUsername(@RequestParam String username) {
		UsersDTO userDTO = usersService.findLoginUserByUsername(username);
		return userDTO;

	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public UserDetailsDTO findUserDetailsOfLogingUser(@RequestParam String username) {
		UserDetailsDTO userDetailsDTO = usersService.findUserDetailsOfLogingUser(username);
		return userDetailsDTO;

	}

	@RequestMapping(value = "find/instituteId/by-teacher", method = RequestMethod.GET)
	public List<TeacherDTO> findInstituteIdByTeacher(@RequestParam Long instituteId) {
		return usersService.findInstituteIdByTeacher(instituteId);

	}

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public List<UsersRoleDTO> findLoginUserRoles(@RequestParam String username) {
		List<UsersRoleDTO> roles = usersService.findUserRolesByUsername(username);
		return roles;
	}

	@RequestMapping(value = "/success/user", method = RequestMethod.GET)
	public Users findLoginSuccessUser(@RequestParam String username) {
		Users user = usersService.findLoginUser(username);
		return user;
	}

	@RequestMapping(value = "/pending/find-all", method = RequestMethod.GET)
	public ResponseEntity<List<SignUpDTO>> findAllPendingUsers() {
		return new ResponseEntity<>(usersService.findAllPendingUsers(), HttpStatus.OK);
	}

	@RequestMapping(value = "approve/single-user", method = RequestMethod.POST)
	public void approveSingleUsers(@RequestBody SignUpDTO signUpDTO) {
		usersService.approveSingleUsers(signUpDTO);

	}

}
