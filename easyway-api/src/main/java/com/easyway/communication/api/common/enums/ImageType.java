package com.easyway.communication.api.common.enums;

public enum ImageType {

	STUDENT_IMAGE(1000),

	TEACHER_IMAGE(2000),

	INSTITUTE_LOGO(3000),

	INSTITUTE_COVER_PHTO(4001);

	private final int code;

	private ImageType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
