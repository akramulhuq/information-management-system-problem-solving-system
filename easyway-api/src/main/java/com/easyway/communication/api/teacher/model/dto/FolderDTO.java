package com.easyway.communication.api.teacher.model.dto;

import com.easyway.communication.api.teacher.model.entity.Teacher;

public class FolderDTO {

	private Long forlderId;

	private String folderName;

	private String createDate;

	private Teacher teacher;

	public Long getForlderId() {
		return forlderId;
	}

	public void setForlderId(Long forlderId) {
		this.forlderId = forlderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
