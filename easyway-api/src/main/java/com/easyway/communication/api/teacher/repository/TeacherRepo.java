package com.easyway.communication.api.teacher.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.teacher.model.entity.Teacher;

public interface TeacherRepo extends JpaRepository<Teacher, Long>{

	List<Teacher> findByInstitute_instituteId(Long instituteId);

	

}
