package com.easyway.communication.api.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.easyway.communication.api.common.response.BaseResponse;
import com.easyway.communication.api.student.model.dto.ProblemConfigDTO;
import com.easyway.communication.api.student.model.dto.StudentDTO;
import com.easyway.communication.api.student.service.StudentProblemService;
import com.easyway.communication.api.student.service.StudentService;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;

@RestController
@RequestMapping("students/")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@Autowired
	private StudentProblemService studentProblemService;

	/**
	 * This method used for find student info and sending response to the client
	 * 
	 * @author habib sumon
	 * @return response with http status to the client
	 * @since 3/27/2018
	 */
	@RequestMapping(value = "find/student-details/by-userName", method = RequestMethod.GET)
	public ResponseEntity<List<StudentDTO>> findStudentInfo() {
		return new ResponseEntity<>(studentService.findStudentInfo(), HttpStatus.OK);
	}

	/**
	 * This method used for saved student info and sending response to the
	 * client
	 * 
	 * @author habib sumon
	 * @return response with http status to the client
	 * @since 3/27/2018
	 */
	@RequestMapping(value = "save/or/update/student", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> saveStudentInfo(@RequestBody StudentDTO studentDTO) {
		BaseResponse response = studentService.saveOrUpdateStudentInfo(studentDTO);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@RequestMapping(value = "update/user-details", method = RequestMethod.POST)
	public void updateUserDetails(@RequestBody UserDetailsDTO userDetailsDTO) {
		studentService.updateUserDetails(userDetailsDTO);
	}

	@RequestMapping(value = "post/problem/by-student", method = RequestMethod.POST)
	public void postProblemByStudent(@RequestBody ProblemConfigDTO problemConfigDTO) {
		studentProblemService.postProblemByStudent(problemConfigDTO);
	}

}
