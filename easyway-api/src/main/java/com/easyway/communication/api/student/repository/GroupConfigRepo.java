package com.easyway.communication.api.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.student.model.entity.GroupConfig;
import com.easyway.communication.api.student.model.entity.Student;

public interface GroupConfigRepo extends JpaRepository<GroupConfig, Long> {

	GroupConfig findByGroup_GroupId(Long groupId);

	GroupConfig findByGroup_GroupIdAndStudent_StudentId(Long groupId, Long studentId);

}
