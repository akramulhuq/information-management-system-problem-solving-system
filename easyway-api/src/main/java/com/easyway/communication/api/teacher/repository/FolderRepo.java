package com.easyway.communication.api.teacher.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.teacher.model.entity.Folder;
import com.easyway.communication.api.teacher.model.entity.Teacher;

public interface FolderRepo extends JpaRepository<Folder, Long> {

	List<Folder> findByTeacher_TeacherId(Long techerId);

}
