package com.easyway.communication.api.institute.model.dto;

import com.easyway.communication.api.institute.model.entity.Institute;

public class GroupDTO {

	private Long groupId;

	private String groupName;

	private String groupTitle;

	private int activeStatus;

	private int likeAs;

	private Institute institute;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupTitle() {
		return groupTitle;
	}

	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public int getLikeAs() {
		return likeAs;
	}

	public void setLikeAs(int likeAs) {
		this.likeAs = likeAs;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
