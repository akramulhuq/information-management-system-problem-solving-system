package com.easyway.communication.api.student.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.easyway.communication.api.common.enums.StatusId;
import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.institute.model.dto.GroupDTO;
import com.easyway.communication.api.institute.model.entity.Group;
import com.easyway.communication.api.institute.repository.GroupRepo;
import com.easyway.communication.api.student.model.dto.GroupConfigDTO;
import com.easyway.communication.api.student.model.dto.ProblemConfigDTO;
import com.easyway.communication.api.student.model.dto.ProblemSolutionDTO;
import com.easyway.communication.api.student.model.entity.GroupConfig;
import com.easyway.communication.api.student.model.entity.ProblemConfig;
import com.easyway.communication.api.student.model.entity.ProblemSolution;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.student.repository.GroupConfigRepo;
import com.easyway.communication.api.student.repository.ProblemSolutionRepo;
import com.easyway.communication.api.student.repository.StudentProblemRepo;
import com.easyway.communication.api.teacher.model.entity.Teacher;

import javassist.expr.NewArray;

@Service
@Transactional
public class StudentProblemService {

	@Autowired
	private StudentProblemRepo studentProblemRepo;

	@Autowired
	private ProblemSolutionRepo problemSolutionRepo;

	@Autowired
	private GroupRepo groupRepo;

	@Autowired
	private GroupConfigRepo groupConfigRepo;

	public List<GroupConfigDTO> findAllGroupConfig() {
		List<GroupConfig> groupConfigs = groupConfigRepo.findAll();
		if (groupConfigs != null) {
			List<GroupConfigDTO> groupConfigDTOs = new ArrayList<>();
			for (GroupConfig groupConfig : groupConfigs) {
				GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
				BeanUtils.copyProperties(groupConfig, groupConfigDTO);
				groupConfigDTOs.add(groupConfigDTO);
			}
			return groupConfigDTOs;
		} else {
			return new ArrayList<>();
		}
	}

	public void joinOrUnJoinGroup(GroupDTO groupDTO, int likeAs, Long studentId) {
		Group group = new Group();
		if (likeAs == 1) {
			GroupConfig findGroup = groupConfigRepo.findByGroup_GroupIdAndStudent_StudentId(groupDTO.getGroupId(),
					studentId);
			if (findGroup == null) {
				GroupConfig groupConfig = new GroupConfig();
				BeanUtils.copyProperties(groupDTO, group);
				group.setLikeAs(1);
				groupConfig.setAddDate(new Date());
				groupConfig.setJoinAs(1);
				groupConfig.setGroup(group);
				groupConfig.setStudent(provideSudent(studentId));
				groupConfigRepo.save(groupConfig);
			}
			if (findGroup != null) {
				BeanUtils.copyProperties(groupDTO, group);
				group.setLikeAs(1);
				findGroup.setJoinAs(1);
				groupConfigRepo.save(findGroup);
			}

		} else if (likeAs == 2) {
			BeanUtils.copyProperties(groupDTO, group);
			GroupConfig groupConfig = new GroupConfig();
			group.setLikeAs(0);
			groupConfig = groupConfigRepo.findByGroup_GroupIdAndStudent_StudentId(groupDTO.getGroupId(), studentId);
			groupConfig.setJoinAs(0);
			groupConfigRepo.save(groupConfig);
		}
	}

	public Student provideSudent(Long studentId) {
		Student student = new Student();
		student.setStudentId(studentId);
		return student;
	}

	public List<ProblemSolutionDTO> findAllActiveProblemSolution() {
		List<ProblemSolution> problemSolutions = problemSolutionRepo.findByActiveStatus(StatusId.ACTIVE.getId());
		if (problemSolutions != null) {
			return problemSolutions.stream().map(solution -> copyProblemSolutionEntityToDto(solution))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	public List<ProblemSolutionDTO> findProblemSolutionByPrId(Long problemId) {
		List<ProblemSolution> problemSolutions = problemSolutionRepo
				.findByProblemConfig_ProblemIdAndActiveStatus(problemId, StatusId.ACTIVE_POST.getId());
		if (problemSolutions != null) {
			return problemSolutions.stream().map(solution -> copyProblemSolutionEntityToDto(solution))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private ProblemSolutionDTO copyProblemSolutionEntityToDto(ProblemSolution solution) {
		ProblemSolutionDTO problemSolutionDTO = new ProblemSolutionDTO();
		BeanUtils.copyProperties(solution, problemSolutionDTO);
		problemSolutionDTO
				.setSolutionDate(ApplicationUtils.convertDateToLocalDateTimeWithName(solution.getSolutionDate()));
		return problemSolutionDTO;
	}

	public void problmSolutionLikeByStdnt(ProblemSolutionDTO problemSolutionDTO, int likeAs) {
		ProblemSolution problemSolution = problemSolutionRepo.findBySolutionId(problemSolutionDTO.getSolutionId());
		if (likeAs == 1) {
			problemSolution.setSolutionStatus(1);
			problemSolutionRepo.save(problemSolution);
		} else if (likeAs == 2) {
			problemSolution.setSolutionStatus(2);
			problemSolutionRepo.save(problemSolution);
		}
	}

	public void problemSolutionByStudent(ProblemSolutionDTO problemSolutionDTO) {
		problemSolutionRepo.save(provideProblemSolution(problemSolutionDTO));
	}

	private ProblemSolution provideProblemSolution(ProblemSolutionDTO problemSolutionDTO) {
		ProblemSolution problemSolution = new ProblemSolution();
		BeanUtils.copyProperties(problemSolutionDTO, problemSolution);
		problemSolution.setSolutionDate(new Date());
		problemSolution.setActiveStatus(StatusId.ACTIVE_POST.getId());
		problemSolution.setSolutionStatus(StatusId.WAITING.getId());
		if (problemSolutionDTO.getStudent().getStudentId() != null) {
			problemSolution.setStudent(provideStudent(problemSolutionDTO.getStudent().getStudentId()));
		} else {
			problemSolution.setStudent(null);
		}
		if (problemSolutionDTO.getTeacher().getTeacherId() != null) {
			problemSolution.setTeacher(provideTeacher(problemSolutionDTO.getTeacher().getTeacherId()));
		} else {
			problemSolution.setTeacher(null);
		}
		if (problemSolutionDTO.getProblemConfig().getProblemId() != null) {
			problemSolution
					.setProblemConfig(provideProblemConfig(problemSolutionDTO.getProblemConfig().getProblemId()));
		} else {
			problemSolution.setProblemConfig(null);
		}
		return problemSolution;
	}

	private ProblemConfig provideProblemConfig(Long problemId) {
		ProblemConfig problemConfig = new ProblemConfig();
		problemConfig.setProblemId(problemId);
		return problemConfig;
	}

	private Teacher provideTeacher(Long teacherId) {
		Teacher teacher = new Teacher();
		teacher.setTeacherId(teacherId);
		return teacher;
	}

	private Student provideStudent(Long studentId) {
		Student student = new Student();
		student.setStudentId(studentId);
		return student;
	}

	public ProblemConfigDTO postProblemByStudent(ProblemConfigDTO problemConfigDTO) {
		ProblemConfig problemConfig = studentProblemRepo.save(provideProblemForPost(problemConfigDTO));
		ProblemConfigDTO problemConfigDTO2 = new ProblemConfigDTO();
		BeanUtils.copyProperties(problemConfig, problemConfigDTO2);
		return problemConfigDTO2;
	}

	private ProblemConfig provideProblemForPost(ProblemConfigDTO problemConfigDTO) {
		ProblemConfig problemConfig = new ProblemConfig();
		BeanUtils.copyProperties(problemConfigDTO, problemConfig);
		problemConfig.setPostDate(new Date());
		problemConfig.setActiveStatus(StatusId.ACTIVE_POST.getId());
		return problemConfig;
	}

	public List<ProblemConfigDTO> findAllProblems() {
		List<ProblemConfig> problemConfigs = studentProblemRepo.findAll();
		if (problemConfigs != null) {
			return problemConfigs.stream().map(problem -> copyProblemConfigEntityToDto(problem))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private ProblemConfigDTO copyProblemConfigEntityToDto(ProblemConfig problem) {
		ProblemConfigDTO problemConfigDTO = new ProblemConfigDTO();
		BeanUtils.copyProperties(problem, problemConfigDTO);
		problemConfigDTO.setPostDate(ApplicationUtils.convertDateToLocalDateTime(problem.getPostDate()));
		return problemConfigDTO;
	}

}
