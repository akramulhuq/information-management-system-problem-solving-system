package com.easyway.communication.api.student.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;

@Entity
@Table(name = "problem")
public class ProblemConfig {

	@Id
	@Column(name = "problem_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long problemId;

	@Column(name = "subject_name")
	private String subjectName;

	@Column(name = "topic_name")
	private String topicName;

	@Column(name = "writin_post")
	private String writinPost;

	@Column(name = "picture_post")
	private String picturePost;

	@Column(name = "post_date")
	private Date postDate;

	@Column(name = "active_status")
	private int activeStatus;

	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private GeneralInfo department;

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getWritinPost() {
		return writinPost;
	}

	public void setWritinPost(String writinPost) {
		this.writinPost = writinPost;
	}

	public String getPicturePost() {
		return picturePost;
	}

	public void setPicturePost(String picturePost) {
		this.picturePost = picturePost;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public GeneralInfo getDepartment() {
		return department;
	}

	public void setDepartment(GeneralInfo department) {
		this.department = department;
	}

}
