package com.easyway.communication.api.common.enums;

public enum GeneralInfoType {

	DEPARTMENT("1100", "Department"), DESIGNATION("2200", "Teacher Designation");

	private final String code;
	private final String typeName;

	GeneralInfoType(String code, String typeName) {
		this.code = code;
		this.typeName = typeName;
	}

	public String getCode() {
		return code;
	}

	public String getTypeName() {
		return typeName;
	}

}
