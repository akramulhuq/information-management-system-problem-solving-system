package com.easyway.communication.api.users.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easyway.communication.api.users.model.entity.SignUp;

public interface SignUpRepository extends JpaRepository<SignUp, Long> {

	SignUp findByUserName(String userName);

	List<SignUp> findByApproveStatusOrderBySignedUpDate(int i);

	SignUp findBySignUpId(Long signUpId);

}
