package com.easyway.communication.api.common.enums;

public enum UserType {

	INSTITUTE("Institute"), TEACHER("Teacher"), STUDENT("Student"), ADMIN("Admin");

	private final String userType;

	private UserType(String userType) {
		this.userType = userType;
	}

	public String getUserType() {
		return userType;
	}

}
