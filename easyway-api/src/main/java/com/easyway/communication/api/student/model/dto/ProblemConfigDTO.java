package com.easyway.communication.api.student.model.dto;

import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.student.model.entity.Student;

public class ProblemConfigDTO {

	private Long problemId;

	private String writinPost;

	private String picturePost;

	private String postDate;

	private String subjectName;

	private String topicName;

	private int activeStatus;

	private Student student;

	private GeneralInfo department;

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public String getWritinPost() {
		return writinPost;
	}

	public void setWritinPost(String writinPost) {
		this.writinPost = writinPost;
	}

	public String getPicturePost() {
		return picturePost;
	}

	public void setPicturePost(String picturePost) {
		this.picturePost = picturePost;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public GeneralInfo getDepartment() {
		return department;
	}

	public void setDepartment(GeneralInfo department) {
		this.department = department;
	}

}
