package com.easyway.communication.api.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easyway.communication.api.address.dto.DistrictDTO;
import com.easyway.communication.api.address.dto.DivisionDTO;
import com.easyway.communication.api.address.entity.District;
import com.easyway.communication.api.address.entity.Division;
import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.admin.repository.DistrictRepo;
import com.easyway.communication.api.admin.repository.DivisionRepo;
import com.easyway.communication.api.admin.repository.GeneralInfoRepo;
import com.easyway.communication.api.common.utils.ApplicationUtils;

@Service
@Transactional
public class AdminService {

	@Autowired
	private DivisionRepo divisionRepo;

	@Autowired
	private DistrictRepo districtRepo;
	
	@Autowired
	private GeneralInfoRepo generalInfoRepo;

	public void saveOrEditDivision(DivisionDTO divisionDTO) {
		divisionRepo.save(provideDivision(divisionDTO));

	}

	private Division provideDivision(DivisionDTO divisionDTO) {
		Division division = new Division();
		BeanUtils.copyProperties(divisionDTO, division);
		division.setDivisionName(ApplicationUtils.convertCamelcaseString(divisionDTO.getDivisionName()));
		return division;
	}

	public List<DivisionDTO> findDivisionsList() {
		List<Division> divisions = divisionRepo.findAll();
		if (divisions != null) {
			return divisions.stream().map(division -> copyDivisionEntityToDto(division)).collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private DivisionDTO copyDivisionEntityToDto(Division division) {
		DivisionDTO divisionDTO = new DivisionDTO();
		BeanUtils.copyProperties(division, divisionDTO);
		return divisionDTO;
	}

	public void saveOrEditDistrict(DistrictDTO districtDTO) {
		districtRepo.save(provideDistrict(districtDTO));
	}

	private District provideDistrict(DistrictDTO districtDTO) {
		District district = new District();
		BeanUtils.copyProperties(districtDTO, district);
		district.setDistrictName(ApplicationUtils.convertCamelcaseString(districtDTO.getDistrictName()));
		return district;
	}

	public List<DistrictDTO> findDistrictsList() {
		List<District> districts = districtRepo.findAll();
		if (districts != null) {
			return districts.stream().map(district -> copyDistrictEntityToDto(district)).collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private DistrictDTO copyDistrictEntityToDto(District district) {
		DistrictDTO districtDTO = new DistrictDTO();
		BeanUtils.copyProperties(district, districtDTO);
		return districtDTO;
	}

	public void saveGeneralInfo(GeneralInfoDTO generalInfoDTO) {
		generalInfoRepo.save(provideGeneralInfo(generalInfoDTO));
		
	}
	
	public GeneralInfo provideGeneralInfo( GeneralInfoDTO generalInfoDTO) {
		GeneralInfo generalInfo= new GeneralInfo();
		BeanUtils.copyProperties(generalInfoDTO, generalInfo);
		return generalInfo;
	}

	public List<GeneralInfoDTO> findGeneralInfoList() {
		List<GeneralInfo> generalInfoList = new ArrayList<>();
		List<GeneralInfoDTO> generalInfoDTOList= new ArrayList<>();
		generalInfoList=generalInfoRepo.findAll();
		for (GeneralInfo generalInfo : generalInfoList) {
			generalInfoDTOList.add(copyGeneralInfoToDto(generalInfo));
		}
		return generalInfoDTOList;
	}
	
	public GeneralInfoDTO copyGeneralInfoToDto(GeneralInfo generalInfo) {
		GeneralInfoDTO generalInfoDTO=new GeneralInfoDTO();
		BeanUtils.copyProperties(generalInfo, generalInfoDTO);
		return generalInfoDTO;
	}

}
