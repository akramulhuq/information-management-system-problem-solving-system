package com.easyway.communication.api.institute.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easyway.communication.api.address.entity.District;
import com.easyway.communication.api.admin.model.dto.GeneralInfoDTO;
import com.easyway.communication.api.admin.model.entity.GeneralInfo;
import com.easyway.communication.api.common.enums.SignUpAs;
import com.easyway.communication.api.common.enums.StatusId;
import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.institute.model.dto.GroupDTO;
import com.easyway.communication.api.institute.model.dto.GroupNoticeDTO;
import com.easyway.communication.api.institute.model.dto.InstituteDTO;
import com.easyway.communication.api.institute.model.dto.InstituteNoticeDTO;
import com.easyway.communication.api.institute.model.entity.Group;
import com.easyway.communication.api.institute.model.entity.GroupNotice;
import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.institute.model.entity.InstituteNotice;
import com.easyway.communication.api.institute.repository.GroupNoticeRepo;
import com.easyway.communication.api.institute.repository.GroupRepo;
import com.easyway.communication.api.institute.repository.InstituteNoticeRepo;
import com.easyway.communication.api.institute.repository.InstituteRepo;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.teacher.model.entity.Teacher;
import com.easyway.communication.api.teacher.repository.TeacherRepo;
import com.easyway.communication.api.users.model.dto.SignUpDTO;
import com.easyway.communication.api.users.model.dto.UserDetailsDTO;
import com.easyway.communication.api.users.model.entity.UserDetails;
import com.easyway.communication.api.users.model.entity.Users;
import com.easyway.communication.api.users.model.entity.UsersRole;
import com.easyway.communication.api.users.repository.UsersDetailRepository;
import com.easyway.communication.api.users.repository.UsersRepository;
import com.easyway.communication.api.users.repository.UsersRoleRepository;

@Service
@Transactional
public class InstituteInfoService {

	@Autowired
	private InstituteRepo instituteRepo;

	@Autowired
	private InstituteNoticeRepo instituteNoticeRepo;

	@Autowired
	private TeacherRepo teacherRepo;

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private UsersRoleRepository usersRoleRepository;

	@Autowired
	private UsersDetailRepository usersDetailRepository;

	@Autowired
	private GroupRepo groupRepo;

	@Autowired
	private GroupNoticeRepo groupNoticeRepo;

	private static final String customStudentIdPrefix1 = "T";
	private static final Long customStudentIdPrefix2 = 100000L;

	public GroupNoticeDTO saveGrpNoticeWithOutPhto(GroupNoticeDTO groupNoticeDTO) {
		GroupNotice groupNotice = new GroupNotice();
		BeanUtils.copyProperties(groupNoticeDTO, groupNotice);
		if (groupNoticeDTO.getInstitute().getInstituteId() != null
				&& groupNoticeDTO.getTeacher().getTeacherId() == null) {
			groupNotice.setTeacher(null);
			groupNotice.setStudent(null);
		}
		if (groupNoticeDTO.getInstitute().getInstituteId() != null
				&& groupNoticeDTO.getTeacher().getTeacherId() != null) {
			groupNotice.setStudent(null);
		}
		groupNotice.setNoticeDate(new Date());
		groupNotice.setActiveStatus(1);
		groupNoticeRepo.saveAndFlush(groupNotice);
		BeanUtils.copyProperties(groupNotice, groupNoticeDTO);
		return groupNoticeDTO;
	}

	public GroupNoticeDTO saveGrpNoticeWithOutPhto(GroupNoticeDTO groupNoticeDTO, Long studentId) {
		GroupNotice groupNotice = new GroupNotice();
		BeanUtils.copyProperties(groupNoticeDTO, groupNotice);
		groupNotice.setTeacher(null);
		groupNotice.setNoticeDate(new Date());
		groupNotice.setActiveStatus(1);
		groupNoticeRepo.saveAndFlush(groupNotice);
		BeanUtils.copyProperties(groupNotice, groupNoticeDTO);
		return groupNoticeDTO;
	}

	public void postGroupNotice(GroupNoticeDTO groupNoticeDTO, Long studentId) {
		GroupNotice groupNotice = new GroupNotice();
		BeanUtils.copyProperties(groupNoticeDTO, groupNotice);
		groupNotice.setTeacher(null);
		groupNotice.setNoticeDate(new Date());
		groupNotice.setActiveStatus(1);
		groupNoticeRepo.save(groupNotice);
	}

	public void postGroupNotice(GroupNoticeDTO groupNoticeDTO) {
		GroupNotice groupNotice = new GroupNotice();
		BeanUtils.copyProperties(groupNoticeDTO, groupNotice);
		if (groupNoticeDTO.getInstitute().getInstituteId() != null
				&& groupNoticeDTO.getTeacher().getTeacherId() == null) {
			groupNotice.setTeacher(null);
			groupNotice.setStudent(null);
		}
		if (groupNoticeDTO.getInstitute().getInstituteId() != null
				&& groupNoticeDTO.getTeacher().getTeacherId() != null) {
			groupNotice.setStudent(null);
		}
		groupNotice.setNoticeDate(new Date());
		groupNotice.setActiveStatus(1);
		groupNoticeRepo.save(groupNotice);
	}

	public List<GroupNoticeDTO> findAllGroupNotice() {
		List<GroupNoticeDTO> groupNoticeDTOs = new ArrayList<>();
		List<GroupNotice> groupNotices = groupNoticeRepo.findAll();
		if (groupNotices != null && !groupNotices.isEmpty()) {
			for (GroupNotice groupNotice : groupNotices) {
				GroupNoticeDTO groupNoticeDTO = new GroupNoticeDTO();
				BeanUtils.copyProperties(groupNotice, groupNoticeDTO);
				groupNoticeDTO.setNoticeDate(
						ApplicationUtils.convertDateToLocalDateTimeWithName(groupNotice.getNoticeDate()));
				groupNoticeDTOs.add(groupNoticeDTO);
			}
			return groupNoticeDTOs;
		} else {
			return new ArrayList<>();
		}
	}

	public void postNoticeByInstitute(GroupDTO groupDTO) {
		Group group = new Group();
		BeanUtils.copyProperties(groupDTO, group);
		group.setActiveStatus(1);
		group.setLikeAs(0);
		groupRepo.save(group);
	}

	public List<GroupDTO> findAllGroup() {
		List<GroupDTO> groupDTOs = new ArrayList<>();
		List<Group> groups = groupRepo.findAll();
		if (groups != null) {
			for (Group group : groups) {
				GroupDTO groupDTO = new GroupDTO();
				BeanUtils.copyProperties(group, groupDTO);
				groupDTOs.add(groupDTO);
			}
			return groupDTOs;
		} else {
			return new ArrayList<>();
		}
	}

	public InstituteNoticeDTO postNoticeByInstitute(InstituteNoticeDTO instituteNoticeDTO) {
		InstituteNoticeDTO inNoticeDTO = new InstituteNoticeDTO();
		if (instituteNoticeDTO.getNoticeId() == null) {
			InstituteNotice InstituteNotice = instituteNoticeRepo.save(provideInstituteNotice(instituteNoticeDTO));
			BeanUtils.copyProperties(InstituteNotice, inNoticeDTO);
			return inNoticeDTO;
		} else {
			instituteNoticeRepo.save(provideInstituteNotice(instituteNoticeDTO));
			return null;
		}
	}

	private InstituteNotice provideInstituteNotice(InstituteNoticeDTO instituteNoticeDTO) {
		InstituteNotice instituteNotice = new InstituteNotice();
		BeanUtils.copyProperties(instituteNoticeDTO, instituteNotice);
		instituteNotice.setNoticeDate(new Date());
		if (instituteNoticeDTO.getInstitute().getInstituteId() != null) {
			instituteNotice.setInstitute(provideInstituteId(instituteNoticeDTO.getInstitute().getInstituteId()));
		} else {
			instituteNotice.setInstitute(null);
		}
		if (instituteNoticeDTO.getTeacher().getTeacherId() != null) {
			instituteNotice.setTeacher(provideTeacherId(instituteNoticeDTO.getTeacher().getTeacherId()));
		} else {
			instituteNotice.setTeacher(null);
		}
		instituteNotice.setActiveStatus(StatusId.ACTIVE_POST.getId());
		return instituteNotice;
	}

	private Teacher provideTeacherId(Long teacherId) {
		Teacher teacher = new Teacher();
		teacher.setTeacherId(teacherId);
		return teacher;
	}

	private Institute provideInstituteId(Long instituteId) {
		Institute institute = new Institute();
		institute.setInstituteId(instituteId);
		return institute;
	}

	public List<InstituteNoticeDTO> findInstituteAllNotices() {
		List<InstituteNotice> instituteNotices = instituteNoticeRepo.findAll();
		if (instituteNotices != null) {
			return instituteNotices.stream().map(notice -> copyInstituteNoticeEntityToDto(notice))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	public List<InstituteNoticeDTO> findInstituteNoticeById(Long instituteId) {
		List<InstituteNotice> instituteNotices = instituteNoticeRepo.findByInstitute_InstituteId(instituteId);
		if (instituteNotices != null) {
			return instituteNotices.stream().map(notice -> copyInstituteNoticeEntityToDto(notice))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	private InstituteNoticeDTO copyInstituteNoticeEntityToDto(InstituteNotice notice) {
		InstituteNoticeDTO instituteNoticeDTO = new InstituteNoticeDTO();
		BeanUtils.copyProperties(notice, instituteNoticeDTO);
		instituteNoticeDTO.setNoticeDate(ApplicationUtils.convertDateToLocalDateTime(notice.getNoticeDate()));
		return instituteNoticeDTO;
	}

	public List<InstituteDTO> findAllInstituteInfo() {
		List<Institute> institutes = instituteRepo.findAll();
		if (institutes != null) {
			return institutes.stream().map(institute -> copyInstituteEntityToDto(institute))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private InstituteDTO copyInstituteEntityToDto(Institute institute) {
		InstituteDTO instituteDTO = new InstituteDTO();
		BeanUtils.copyProperties(institute, instituteDTO);
		return instituteDTO;
	}

	public List<TeacherDTO> findAllTeacherInfo() {
		List<TeacherDTO> teacherDTOs = new ArrayList<>();
		List<Teacher> teachers = teacherRepo.findAll();
		for (Teacher teacher : teachers) {
			TeacherDTO teacherDTO = new TeacherDTO();
			BeanUtils.copyProperties(teacher, teacherDTO);
			teacherDTOs.add(teacherDTO);
		}
		return teacherDTOs;
	}

	/*
	 * public TeacherDTO copyTeacherEntityToDto(Teacher teacher) { TeacherDTO
	 * teacherDTO=new TeacherDTO(); BeanUtils.copyProperties(teacher,
	 * teacherDTO); return teacherDTO; }
	 */

	public void updateInstituteInfo(UserDetailsDTO userDetailsDTO) {
		instituteRepo.save(provideUpdateInstituteDTO(userDetailsDTO));

	}

	private Institute provideUpdateInstituteDTO(UserDetailsDTO userDetailsDTO) {
		Institute institute = new Institute();
		BeanUtils.copyProperties(userDetailsDTO.getInstitute(), institute);
		institute.setArticaalOfInstitute(userDetailsDTO.getInstitute().getArticaalOfInstitute());
		if (userDetailsDTO.getInstitute().getDistrict() != null) {
			District district = new District();
			district.setDistrictId(userDetailsDTO.getInstitute().getDistrict().getDistrictId());
			institute.setDistrict(district);
		}
		if (userDetailsDTO.getLogoName() != null) {
			institute.setLogoPhoto(userDetailsDTO.getLogoName());
		} else {
			institute.setLogoPhoto(userDetailsDTO.getInstitute().getLogoPhoto());
		}
		return institute;
	}

	public TeacherDTO saveTeacher(TeacherDTO teacherDTO, int signUpAs) {
		TeacherDTO teacherDTO2 = new TeacherDTO();
		if (teacherDTO.getTeacherId() == null) {
			Teacher teacher = teacherRepo.save(provideTeacherInfo(teacherDTO, signUpAs));
			BeanUtils.copyProperties(teacher, teacherDTO2);

		}
		if (teacherDTO.getTeacherId() != null) {
			Teacher teacher = teacherRepo.save(provideTeacherInfo(teacherDTO, signUpAs));
			usersRepository.save(provideSignedUpTeacher(teacherDTO));
			usersRoleRepository.save(provideSignedUpTeacherRole(teacherDTO, SignUpAs.TEACHER.getCode()));
			usersDetailRepository.save(provideTeacherDetails(teacherDTO, signUpAs));
		}
		return teacherDTO2;
	}

	public Teacher provideTeacherInfo(TeacherDTO teacherDTO, int signUpAs) {
		Teacher teacher = new Teacher();

		BeanUtils.copyProperties(teacherDTO, teacher);
		teacher.setCreateDate(new Date());
		teacher.setInstitute(provideInstituteId(teacherDTO.getInstitute().getInstituteId()));
		if (teacherDTO.getDepartment().getId() != null) {
			teacher.setDepartment(providegenerealInfoid(teacherDTO.getDepartment().getId()));
		} else {
			teacher.setDepartment(null);
		}
		if (teacherDTO.getDesignation().getId() != null) {
			teacher.setDesignation(providegenerealInfoid(teacherDTO.getDesignation().getId()));
		} else {
			teacher.setDesignation(null);
		}
		teacher.setTeacherName(ApplicationUtils.convertCamelcaseString(teacherDTO.getTeacherName()));
		return teacher;
	}

	public Users provideSignedUpTeacher(TeacherDTO teacherDTO) {
		Users user = new Users();
		user.setUsername(teacherDTO.getUserName());
		user.setPassword(teacherDTO.getPassword());
		user.setNickName(teacherDTO.getTeacherName());
		user.setEnabled(true);
		return user;
	}

	public UsersRole provideSignedUpTeacherRole(TeacherDTO teacherDTO, int signupAs) {
		UsersRole userRole = new UsersRole();
		userRole.setRoleName(ApplicationUtils.provideSignedUpUserRole(signupAs));
		userRole.setUsername(teacherDTO.getUserName());
		return userRole;
	}

	private UserDetails provideTeacherDetails(TeacherDTO teacher, int signUpAs) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserType(ApplicationUtils.provideUsersType(signUpAs));
		userDetails.setUsername(teacher.getUserName());
		userDetails.setTeacher(provideTeacherId(teacher.getTeacherId()));
		return userDetails;
	}

	public String prepareCustomId() {
		List<Teacher> teachers = teacherRepo.findAll();
		Long maxTeacherId = teachers.get(teachers.size() - 1).getTeacherId();
		maxTeacherId++;
		Long maxTeaId = customStudentIdPrefix2 + maxTeacherId;
		return customStudentIdPrefix1 + maxTeaId;

	}

	private GeneralInfo providegenerealInfoid(Long infoId) {
		GeneralInfo generalInfo = new GeneralInfo();
		generalInfo.setId(infoId);
		return generalInfo;
	}

}
