package com.easyway.communication.api.teacher.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easyway.communication.api.common.utils.ApplicationUtils;
import com.easyway.communication.api.teacher.model.dto.FolderDTO;
import com.easyway.communication.api.teacher.model.dto.TeacherDTO;
import com.easyway.communication.api.teacher.model.dto.TopicDTO;
import com.easyway.communication.api.teacher.model.entity.Folder;
import com.easyway.communication.api.teacher.model.entity.Teacher;
import com.easyway.communication.api.teacher.model.entity.Topic;
import com.easyway.communication.api.teacher.repository.FolderRepo;
import com.easyway.communication.api.teacher.repository.TeacherRepo;
import com.easyway.communication.api.teacher.repository.TopicRepo;

@Service
@Transactional
public class TeacherService {
	@Autowired
	private TeacherRepo teacherRepo;

	@Autowired
	private FolderRepo folderRepo;

	@Autowired
	private TopicRepo topicRepo;

	public void addTopicByTeacher(TopicDTO topicDTO) {
		Topic topic = new Topic();
		BeanUtils.copyProperties(topicDTO, topic);
		topic.setUploadDate(new Date());
		topicRepo.save(topic);
	}

	public List<TopicDTO> findTopicByTeacher(Long folderId) {
		List<Topic> topics = topicRepo.findByFolder_ForlderId(folderId);
		if (topics != null) {
			return topics.stream().map(topic -> copyTopicEntityToDto(topic)).collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	private TopicDTO copyTopicEntityToDto(Topic topic) {
		TopicDTO topicDTO = new TopicDTO();
		BeanUtils.copyProperties(topic, topicDTO);
		topicDTO.setUploadDate(ApplicationUtils.convertDateToLocalDateTime(topic.getUploadDate()));
		return topicDTO;
	}

	public List<FolderDTO> findAllFolderByTeacherId(Long techerId) {
		List<Folder> folders = folderRepo.findByTeacher_TeacherId(techerId);
		if (folders != null) {
			return folders.stream().map(folder -> copyFolderEntityToDto(folder)).collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	public void createFolderByTeacher(FolderDTO folderDTO, Long techerId) {
		if (folderDTO != null) {
			Folder folder = new Folder();
			BeanUtils.copyProperties(folderDTO, folder);
			folder.setCreateDate(new Date());
			folder.setTeacher(provideTeacherId(techerId));
			folderRepo.save(folder);
		}
	}

	public List<TeacherDTO> findTeacherInfo() {
		List<Teacher> teacherEntities = teacherRepo.findAll();
		if (teacherEntities != null) {
			return teacherEntities.stream().map(teacher -> copyTeacherEntityToTeacherDto(teacher))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	private TeacherDTO copyTeacherEntityToTeacherDto(Teacher teacher) {
		TeacherDTO teacherDTO = new TeacherDTO();
		BeanUtils.copyProperties(teacher, teacherDTO);
		return teacherDTO;
	}

	private Teacher provideTeacherId(Long techerId) {
		Teacher teacher = new Teacher();
		teacher.setTeacherId(techerId);
		return teacher;
	}

	private FolderDTO copyFolderEntityToDto(Folder folder) {
		FolderDTO folderDTO = new FolderDTO();
		BeanUtils.copyProperties(folder, folderDTO);
		folderDTO.setCreateDate(ApplicationUtils.convertDateToLocalDateTime(folder.getCreateDate()));
		return folderDTO;
	}

}
