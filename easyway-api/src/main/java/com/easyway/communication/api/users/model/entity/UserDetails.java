package com.easyway.communication.api.users.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.entity.Teacher;

@Entity
@Table(name = "user_details")
public class UserDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "user_details_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long loginUserDetailsId;

	@Column(name = "username", unique = true)
	private String username;

	@Column(name = "user_type")
	private String userType;

	@ManyToOne
	@JoinColumn(name = "institute_id")
	private Institute institute;

	@ManyToOne
	@JoinColumn(name = "teaher_id")
	private Teacher teacher;

	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;

	public Long getLoginUserDetailsId() {
		return loginUserDetailsId;
	}

	public void setLoginUserDetailsId(Long loginUserDetailsId) {
		this.loginUserDetailsId = loginUserDetailsId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
