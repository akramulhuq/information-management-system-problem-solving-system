package com.easyway.communication.api.common.enums;

public enum SignUpAs {

	INSTITUTE(1, "ROLE_INSTITUTE"), TEACHER(2, "ROLE_TEACHER"), STUDENT(3, "ROLE_STUDENT"), ADMIN(4,
			"ROLE_ADMIN"), GUEST(0, "ROLE_GUEST");

	private final int code;

	private final String roleName;

	private SignUpAs(int code, String roleName) {
		this.code = code;
		this.roleName = roleName;
	}

	public int getCode() {
		return code;
	}

	public String getRoleName() {
		return roleName;
	}

}
