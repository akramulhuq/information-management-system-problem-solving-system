package com.easyway.communication.api.users.model.dto;

import com.easyway.communication.api.institute.model.entity.Institute;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.entity.Teacher;

public class UserDetailsDTO {

	private Long loginUserDetailsId;

	private String username;

	private String userType;

	private Institute institute;

	private Teacher teacher;

	private Student student;

	private String logoName;

	private String birthDate;

	public Long getLoginUserDetailsId() {
		return loginUserDetailsId;
	}

	public void setLoginUserDetailsId(Long loginUserDetailsId) {
		this.loginUserDetailsId = loginUserDetailsId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

}
