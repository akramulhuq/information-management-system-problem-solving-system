package com.easyway.communication.api.institute.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easyway.communication.api.address.entity.District;

@Entity
@Table(name = "institute")
public class Institute {

	@Id
	@Column(name = "institute_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long instituteId;

	@Column(name = "institute_name")
	private String instituteName;

	@Column(name = "institute_artical")
	private String articaalOfInstitute;

	@Column(name = "contact_no")
	private String contactNo;

	@Column(name = "cntact_email")
	private String contactEmail;

	@Column(name = "cover_photo")
	private String coverPhoto;

	@Column(name = "logo_photo")
	private String logoPhoto;

	@Column(name = "address")
	private String address;

	@ManyToOne
	@JoinColumn(name = "district_id")
	private District district;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getArticaalOfInstitute() {
		return articaalOfInstitute;
	}

	public void setArticaalOfInstitute(String articaalOfInstitute) {
		this.articaalOfInstitute = articaalOfInstitute;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCoverPhoto() {
		return coverPhoto;
	}

	public void setCoverPhoto(String coverPhoto) {
		this.coverPhoto = coverPhoto;
	}

	public String getLogoPhoto() {
		return logoPhoto;
	}

	public void setLogoPhoto(String logoPhoto) {
		this.logoPhoto = logoPhoto;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

}
