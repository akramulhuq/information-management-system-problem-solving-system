package com.easyway.communication.api.common.enums;

public enum Topic {

	INSTITUTE("Institute"), TEACHER("Teacher"), STUDENT("Student");

	private final String name;

	private Topic(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
