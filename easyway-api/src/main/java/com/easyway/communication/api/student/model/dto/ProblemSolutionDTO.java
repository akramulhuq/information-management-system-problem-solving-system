package com.easyway.communication.api.student.model.dto;

import com.easyway.communication.api.student.model.entity.ProblemConfig;
import com.easyway.communication.api.student.model.entity.Student;
import com.easyway.communication.api.teacher.model.entity.Teacher;

public class ProblemSolutionDTO {

	private Long solutionId;

	private String solutionByWrite;

	private String solutionByPhoto1;

	private String solutionByPhoto2;

	private String solutionNote;

	private String solutionDate;

	private int activeStatus;

	private int solutionStatus;

	private Teacher teacher;

	private Student student;

	private ProblemConfig problemConfig;

	public Long getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(Long solutionId) {
		this.solutionId = solutionId;
	}

	public String getSolutionByWrite() {
		return solutionByWrite;
	}

	public void setSolutionByWrite(String solutionByWrite) {
		this.solutionByWrite = solutionByWrite;
	}

	public String getSolutionByPhoto1() {
		return solutionByPhoto1;
	}

	public void setSolutionByPhoto1(String solutionByPhoto1) {
		this.solutionByPhoto1 = solutionByPhoto1;
	}

	public String getSolutionByPhoto2() {
		return solutionByPhoto2;
	}

	public void setSolutionByPhoto2(String solutionByPhoto2) {
		this.solutionByPhoto2 = solutionByPhoto2;
	}

	public String getSolutionNote() {
		return solutionNote;
	}

	public void setSolutionNote(String solutionNote) {
		this.solutionNote = solutionNote;
	}

	public String getSolutionDate() {
		return solutionDate;
	}

	public void setSolutionDate(String solutionDate) {
		this.solutionDate = solutionDate;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ProblemConfig getProblemConfig() {
		return problemConfig;
	}

	public void setProblemConfig(ProblemConfig problemConfig) {
		this.problemConfig = problemConfig;
	}

	public int getSolutionStatus() {
		return solutionStatus;
	}

	public void setSolutionStatus(int solutionStatus) {
		this.solutionStatus = solutionStatus;
	}

}
