package com.easyway.communication.api.users.model.dto;

import java.util.Date;

import com.easyway.communication.api.address.entity.District;

public class SignUpDTO {

	private Long signUpId;

	private String password;

	private String name;

	private String branchName;

	private String contactNo;

	private String email;

	private String presentAddress;

	private String gender;

	private String birthDate;

	private String userRole;

	private int signedUpAs;

	private boolean check;

	private String userName;

	private String doctorName;

	private String hospitalName;

	private Date signedUpDate;

	private District district;

	private Long instituteId;

	public Long getSignUpId() {
		return signUpId;
	}

	public void setSignUpId(Long signUpId) {
		this.signUpId = signUpId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public int getSignedUpAs() {
		return signedUpAs;
	}

	public void setSignedUpAs(int signedUpAs) {
		this.signedUpAs = signedUpAs;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public Date getSignedUpDate() {
		return signedUpDate;
	}

	public void setSignedUpDate(Date signedUpDate) {
		this.signedUpDate = signedUpDate;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

}
