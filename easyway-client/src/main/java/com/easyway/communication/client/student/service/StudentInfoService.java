package com.easyway.communication.client.student.service;

import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.common.util.StudentDataExporter;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class StudentInfoService {

	public void updateStudentInfo(UserDetailsDTO studentInfos) {
		studentInfos.setLogoWithBase64(null);
		if (studentInfos.getInstitute().getInstituteId() == null) {
			studentInfos.setInstitute(null);
		}
		if (studentInfos.getTeacher().getTeacherId() == null) {
			studentInfos.setTeacher(null);
		}
		StudentDataExporter.userDetailsDtoToApi(StaticValueProvider.LOGIN_STUDENT_URI, "update/user-details",
				studentInfos);
	}

	public UserDetailsDTO findStudentInfoByHttp() {
		UserDetailsDTO userDetailsDTO = ApplicationUtilsMB.provideCurrentUserDetils();
		userDetailsDTO.setPrepareName(provideInstitute(userDetailsDTO.getStudent().getInstitute().getInstituteName()));
		return userDetailsDTO;
	}

	private String provideInstitute(String provideName) {
		String[] arryOfName = provideName.split(" ");
		String singleName = Arrays.asList(arryOfName).stream().findFirst().orElse(null);
		return singleName;
	}

	public Long provideStudentId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getStudent().getStudentId();
	}
}
