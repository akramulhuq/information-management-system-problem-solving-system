package com.easyway.communication.client.institute.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.DataExporter;
import com.easyway.communication.client.common.util.InstituteDataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class InstituteInfoService {

	public void updateInstituteInfo(UserDetailsDTO userDetailsDTO) {
		userDetailsDTO.setLogoWithBase64(null);
		DataExporter.sendInstituteDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI, "update/institute", userDetailsDTO);
	}

	public UserDetailsDTO findInstituteInfoByHttp() {
		UserDetailsDTO userDetailsDTO = ApplicationUtilsMB.provideCurrentUserDetils();
		userDetailsDTO.setPrepareName(provideInstitute(userDetailsDTO.getInstitute().getInstituteName()));
		return userDetailsDTO;
	}

	private String provideInstitute(String provideName) {
		String[] arryOfName = provideName.split(" ");
		String singleName = Arrays.asList(arryOfName).stream().findFirst().orElse(null);
		return singleName;
	}

	public List<InstituteDTO> findAllInstituteInfo() {
		return ApiConsumer.consumeInstituteDtos(StaticValueProvider.LOGIN_INSTITUTE_URI, "find/all/institute-info");
	}

	public List<TeacherDTO> findTeacherByInstituteId() {
		return findAllTeacherInfo().stream()
				.filter(institute -> institute.getInstitute().getInstituteId().equals(provideInstituteId()))
				.collect(Collectors.toList());
	}

	public List<TeacherDTO> findAllTeacherInfo() {
		return ApiConsumer.consumeTeacherDtos(StaticValueProvider.LOGIN_INSTITUTE_URI, "find/all/teacher-info");
	}

	public ResponseEntity<TeacherDTO> saveTeacher(TeacherDTO teacherDTO, int signUpAs) {
		InstituteDTO institute = new InstituteDTO();
		institute.setInstituteId(provideInstituteId());
		teacherDTO.setInstitute(institute);
		if (teacherDTO.getTeacherId() != null) {
			teacherDTO.setPassword(ApplicationUtils.generateBcryptHash12(teacherDTO.getPassword()));
		}
		return InstituteDataExporter.sendInstituteTeacherDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI,
				"save/teacherinfo?signUpAs=" + signUpAs, teacherDTO);
	}

	public Long provideInstituteId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getInstitute().getInstituteId();
	}

}
