package com.easyway.communication.client.users.model;

import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;

public class UserDetailsDTO {

	private Long loginUserDetailsId;

	private String username;

	private String userType;

	private InstituteDTO institute;

	private TeacherDTO teacher;

	private StudentDTO student;

	private String logoWithBase64;

	private String birthDate;

	private String prepareName;

	private String logoName;

	private Long totalProblemSolution;

	public Long getLoginUserDetailsId() {
		return loginUserDetailsId;
	}

	public void setLoginUserDetailsId(Long loginUserDetailsId) {
		this.loginUserDetailsId = loginUserDetailsId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public InstituteDTO getInstitute() {
		if (institute == null) {
			institute = new InstituteDTO();
		}
		return institute;
	}

	public void setInstitute(InstituteDTO institute) {
		this.institute = institute;
	}

	public TeacherDTO getTeacher() {
		if (teacher == null)
			teacher = new TeacherDTO();
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public StudentDTO getStudent() {
		if (student == null)
			student = new StudentDTO();
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public String getLogoWithBase64() {
		return logoWithBase64;
	}

	public void setLogoWithBase64(String logoWithBase64) {
		this.logoWithBase64 = logoWithBase64;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPrepareName() {
		return prepareName;
	}

	public void setPrepareName(String prepareName) {
		this.prepareName = prepareName;
	}

	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}

	public Long getTotalProblemSolution() {
		return totalProblemSolution;
	}

	public void setTotalProblemSolution(Long totalProblemSolution) {
		this.totalProblemSolution = totalProblemSolution;
	}

}
