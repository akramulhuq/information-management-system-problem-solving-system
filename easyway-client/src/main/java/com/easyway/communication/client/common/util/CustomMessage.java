package com.easyway.communication.client.common.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class CustomMessage {

	public static final String SAVE_SUCCESS_MESSAGE = " has been created successfully!";
	public static final String UPDATE_SUCCESS_MESSAGE = " has been updated successfully!";
	public static final String DELETE_SUCCESS_MESSAGE = " has been deleted successfully!";
	public static final String SAVE_FAILED_MESSAGE = " could not be created successfully!";
	public static final String UPDATE_FAILED_MESSAGE = " could not be updated successfully!";
	public static final String DELETE_FAILED_MESSAGE = " could not be deleted successfully!";
	public static final String APPROVE_SUCCESS_MESSAGE = " has been approved successfully!";
	public static final String APPROVE_FAILED_MESSAGE = " could not be approved successfully!";
	public static final String ADD_SUCCESS_MESSAGE = " has been add successfully!";
	public static final String ADD_FAILED_MESSAGE = " could not be add successfully!";

	// 1 is from save // 2 is for update // 0 is for delete
	public static void sendMessageToClient(FacesContext context, int httpStatus, String topic, int messageCode) {
		switch (messageCode) {
		case 1:
			if (httpStatus >= 200 && httpStatus < 300) {
				context.addMessage(null, new FacesMessage(topic + SAVE_SUCCESS_MESSAGE));
			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, topic + SAVE_FAILED_MESSAGE, ""));
			}
			break;

		case 2:
			if (httpStatus >= 200 && httpStatus < 300) {
				context.addMessage(null, new FacesMessage(topic + UPDATE_SUCCESS_MESSAGE));
			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, topic + UPDATE_FAILED_MESSAGE, ""));
			}
			break;

		case 0:
			if (httpStatus >= 200 && httpStatus < 300) {
				context.addMessage(null, new FacesMessage(topic + DELETE_SUCCESS_MESSAGE));
			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, topic + DELETE_FAILED_MESSAGE, ""));
			}
			break;

		case 4:
			if (httpStatus >= 200 && httpStatus < 300) {
				context.addMessage(null, new FacesMessage(topic + ADD_SUCCESS_MESSAGE));
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, topic + ADD_FAILED_MESSAGE, ""));
			}
			break;

		}
	}
}
