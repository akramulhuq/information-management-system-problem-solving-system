package com.easyway.communication.client.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.users.model.SignUpDTO;

public class AdminApiConsumer {
	public static List<SignUpDTO> consumeSignUpDTOs(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<SignUpDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				SignUpDTO[].class);
		List<SignUpDTO> studentDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(studentDTOs);
	}

	public static List<DivisionDTO> consumeDivisionDtos(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<DivisionDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, DivisionDTO[].class);
		List<DivisionDTO> divisionDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(divisionDTOs);
	}

	public static List<DistrictDTO> consumeDistrictDtos(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<DistrictDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, DistrictDTO[].class);
		List<DistrictDTO> divisionDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(divisionDTOs);
	}
	
	public static List<GeneralInfoDTO> consumeGeneralInfoDTOs(String uri, String uriExtension){
		RestTemplate template = new RestTemplate();
		ResponseEntity<GeneralInfoDTO[]> response=template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, GeneralInfoDTO[].class);
		List<GeneralInfoDTO> generalInfoDTOs=Arrays.asList(response.getBody());
		return new ArrayList<>(generalInfoDTOs);
	}

}
