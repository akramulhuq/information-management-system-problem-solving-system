package com.easyway.communication.client.common.enums;

public enum UserRole {

	SUPER_ADMIN("ROLE_ADMIN"), INSTITUTE("ROLE_INSTITUTE"), TEACHER("ROLE_TEACHER"), STUDENT("ROLE_STUDENT"), GUEST(
			"RULE_GUEST");

	private final String role;

	private UserRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

}
