package com.easyway.communication.client.common.enums;

public enum ImageType {

	STUDENT_IMAGE(1000),

	TEACHER_IMAGE(2000),

	INSTITUTE_LOGO(3000),
	
	STUDENT_PROBLEM(5000),
	
	SOLUTION_PHOTO_1(6001),
	
	SOLUTION_PHOTO_2(6002),
	
	NOTICE_PHOTO(7000),
	
	GROUP_NOTICE_PHOTO(7001),

	INSTITUTE_COVER_PHTO(4001);

	private final int code;

	private ImageType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
