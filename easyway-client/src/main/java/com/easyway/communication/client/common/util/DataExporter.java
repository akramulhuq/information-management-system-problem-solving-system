package com.easyway.communication.client.common.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.teacher.model.FolderDTO;
import com.easyway.communication.client.teacher.model.TopicDTO;
import com.easyway.communication.client.users.model.SignUpDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

public class DataExporter {

	public static void sendSignUpDtoToApi(String uri, String uriExtension, SignUpDTO signpUpDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<SignUpDTO> request = new HttpEntity<>(signpUpDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, SignUpDTO.class);
	}

	public static void sendFolderDtoToApi(String uri, String uriExtension, FolderDTO folderDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<FolderDTO> request = new HttpEntity<>(folderDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, FolderDTO.class);
	}

	public static void sendTopicDtoToApi(String uri, String uriExtension, TopicDTO topicDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<TopicDTO> request = new HttpEntity<>(topicDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, TopicDTO.class);
	}

	public static void sendInstituteDtoToApi(String uri, String uriExtension, UserDetailsDTO userDetailsDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<UserDetailsDTO> request = new HttpEntity<>(userDetailsDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, UserDetailsDTO.class);
	}

	public static void sendGroupDtoToApi(String uri, String uriExtension, GroupDTO groupDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<GroupDTO> request = new HttpEntity<>(groupDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, GroupDTO.class);
	}

	public static ResponseEntity<GroupNoticeDTO> sendGroupNoticeDtoToApi(String uri, String uriExtension,
			GroupNoticeDTO groupNoticeDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<GroupNoticeDTO> request = new HttpEntity<>(groupNoticeDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		return template.exchange(url, HttpMethod.POST, request, GroupNoticeDTO.class);
	}

}
