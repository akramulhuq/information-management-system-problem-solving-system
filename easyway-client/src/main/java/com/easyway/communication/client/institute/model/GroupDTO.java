package com.easyway.communication.client.institute.model;

public class GroupDTO {

	private Long groupId;

	private String groupName;

	private String groupTitle;

	private int activeStatus;

	private int likeAs;

	private int join = 0;

	private InstituteDTO institute;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupTitle() {
		return groupTitle;
	}

	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public int getLikeAs() {
		return likeAs;
	}

	public void setLikeAs(int likeAs) {
		this.likeAs = likeAs;
	}

	public InstituteDTO getInstitute() {
		if (institute == null)
			institute = new InstituteDTO();
		return institute;
	}

	public void setInstitute(InstituteDTO institute) {
		this.institute = institute;
	}

	public int getJoin() {
		return join;
	}

	public void setJoin(int join) {
		this.join = join;
	}

}
