package com.easyway.communication.client.student.model;

import java.util.List;

import com.easyway.communication.client.admin.model.GeneralInfoDTO;

public class ProblemConfigDTO {

	private Long problemId;

	private String writinPost;

	private String picturePost;

	private String postDate;

	private String subjectName;

	private String topicName;

	private int activeStatus;

	private String problemPhotoBase64;
	
	private String studentPhotoBase64;

	private StudentDTO student;

	private GeneralInfoDTO department;
	
	private Long totalSolution;

	private List<ProblemSolutionDTO> ProblemSolutionDTOs;

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public String getWritinPost() {
		return writinPost;
	}

	public void setWritinPost(String writinPost) {
		this.writinPost = writinPost;
	}

	public String getPicturePost() {
		return picturePost;
	}

	public void setPicturePost(String picturePost) {
		this.picturePost = picturePost;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public StudentDTO getStudent() {
		if (student == null)
			student = new StudentDTO();
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public GeneralInfoDTO getDepartment() {
		if (department == null)
			department = new GeneralInfoDTO();
		return department;
	}

	public void setDepartment(GeneralInfoDTO department) {
		this.department = department;
	}

	public String getProblemPhotoBase64() {
		return problemPhotoBase64;
	}

	public void setProblemPhotoBase64(String problemPhotoBase64) {
		this.problemPhotoBase64 = problemPhotoBase64;
	}

	public List<ProblemSolutionDTO> getProblemSolutionDTOs() {
		return ProblemSolutionDTOs;
	}

	public void setProblemSolutionDTOs(List<ProblemSolutionDTO> problemSolutionDTOs) {
		ProblemSolutionDTOs = problemSolutionDTOs;
	}

	public Long getTotalSolution() {
		return totalSolution;
	}

	public void setTotalSolution(Long totalSolution) {
		this.totalSolution = totalSolution;
	}

	public String getStudentPhotoBase64() {
		return studentPhotoBase64;
	}

	public void setStudentPhotoBase64(String studentPhotoBase64) {
		this.studentPhotoBase64 = studentPhotoBase64;
	}
}
