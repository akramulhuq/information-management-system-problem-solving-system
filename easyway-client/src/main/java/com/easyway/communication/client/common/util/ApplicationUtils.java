/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyway.communication.client.common.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.UploadedFile;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.easyway.communication.client.common.enums.ImageType;
import com.google.common.io.ByteStreams;

/**
 *
 * @author Asus
 */
public class ApplicationUtils {

	public static final String WEEK_DAYS[] = { "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
			"Friday" };

	public static final String ADMIN_ACTIVETION_CODE = "EasyLink@ADMIN1-9";

	private static final Logger logger = Logger.getLogger(ApplicationUtils.class);

	public static Date parseStringToDate(String stringDate) {
		String name = "name";
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
		} catch (ParseException e) {
			logger.error(e);
		}
		return date;
	}

	public static String parseDateToAgeString(Date stringDate) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd").format(stringDate);
		} catch (Exception e) {
			return null;
		}
	}

	public static String convertDateToLocalDateTime(Date date) {
		try {
			return new SimpleDateFormat("dd-MMMMM-yyyy hh:mm aaa").format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static String provideStringDateTimeToStringDate(String stringDate) {
		Date date = null;
		try {
			date = new SimpleDateFormat("dd-MMMMM-yyyy hh:mm aaa").parse(stringDate);
		} catch (ParseException e) {
			logger.error(e);
		}
		return new SimpleDateFormat("dd-MM-yyyy").format(date);
	}

	public static String convertLocalDateTime(String date) {
		try {
			return new SimpleDateFormat("hh:mm a").format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static String convertDateToStringDate(Date date) {
		try {
			/* output is 17-Feb-2019 05:33 PM */
			return new SimpleDateFormat("dd-MMM-yyyy").format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static String convertDateToLocalDate(Date date) {
		try {
			return new SimpleDateFormat("dd-MM-yyyy").format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static String provideStartAndEndTime(Date date) {
		return convertDateToLocalDate(new Date()) + " " + ApplicationUtils.convertDateToStringTimeWithAmPm(date);
	}

	public static String provideCurrentAge(String date) {
		Period period = providePerodDate(date);
		String age = null;
		if (period.getYears() != 0) {
			if (period.getYears() != 0 && period.getMonths() == 0 && period.getDays() == 0) {
				age = string(period.getYears()) + " years ";
			} else if (period.getYears() != 0 && period.getMonths() != 0 && period.getDays() == 0) {
				age = string(period.getYears()) + " years " + string(period.getMonths()) + " months ";
			} else if (period.getYears() != 0 && period.getMonths() != 0 && period.getDays() != 0) {
				age = string(period.getYears()) + " years " + string(period.getMonths()) + " months "
						+ string(period.getDays()) + " days ";
			}
		} else if (period.getYears() == 0 && period.getMonths() != 0) {

			if (period.getMonths() != 0 && period.getDays() == 0) {
				age = string(period.getMonths()) + " months ";
			} else if (period.getMonths() != 0 && period.getDays() != 0) {
				age = string(period.getMonths()) + " months " + string(period.getDays()) + " days ";
			}
		} else if (period.getYears() == 0 && period.getMonths() == 0 && period.getDays() != 0) {
			age = string(period.getDays()) + " days ";
		}
		return age;
	}

	public static Date parseStringToTime(String stringTime) {
		Date date = null;
		try {
			date = new SimpleDateFormat("hh:mm:ss").parse(stringTime);
		} catch (ParseException e) {
			logger.error(e);
		}
		return date;
	}

	/*
	 * public static String provideUserBirthDate(int year, int month, int day) {
	 * int dLenght = Integer.toString(day).length(); int mLenght =
	 * Integer.toString(month).length(); String finalDay = null, finalMonth =
	 * null;
	 * 
	 * if (dLenght == 1) { finalDay = "0" + Integer.toString(day); } else {
	 * finalDay = Integer.toString(day); } if (mLenght == 1) { finalMonth = "0"
	 * + Integer.toString(month); } else { finalMonth = Integer.toString(month);
	 * }
	 * 
	 * return String.join("-", Integer.toString(year), finalMonth, finalDay); }
	 */

	public static String currentAge(String date) {
		Period period = providePerodDate(date);
		String age = string(period.getYears()) + " year ";
		return age;
	}

	public static String string(Integer integer) {
		return Integer.toString(integer);
	}

	public static Period providePerodDate(String date) {
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(date);
		Period period = Period.between(birthday, today);
		return period;
	}

	public static Date currentDate() {
		return new Date();
	}

	public static String getMonthNumberByMonthName(String monthName) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(new SimpleDateFormat("MMMM", Locale.ENGLISH).parse(monthName));
		} catch (ParseException ex) {
		}

		String monthNo = String.valueOf((cal.get(Calendar.MONTH) + 1));

		if (String.valueOf(monthNo).length() < 2) {
			monthNo = "0" + monthNo;
		}

		return monthNo;
	}

	/*
	 * public static String getCoreSettingTypeName(int typeId) { String typeName
	 * = "";
	 * 
	 * switch (typeId) { case 2101: typeName = "Academic Year"; break; case
	 * 2102: typeName = "Class"; break; case 2103: typeName = "Shift"; break;
	 * case 2104: typeName = "Section"; break; case 2105: typeName = "Group";
	 * break; case 2106: typeName = "Student Category"; break; case 2107:
	 * typeName = "Academic Session"; break; case 2601: typeName =
	 * "Designation"; break; case 2201: typeName = "Exam"; break; case 2202:
	 * typeName = "Subject"; break; case 2301: typeName = "Period"; break; case
	 * 2302: typeName = "Staff Category"; break; case 2401: typeName = "Fund";
	 * break; case 2501: typeName = "Fee head"; break; case 2502: typeName =
	 * "Fee sub head"; break; case 2503: typeName = "Waiver"; break; default:
	 * break; }
	 * 
	 * return typeName; }
	 */

	public static List<Date> getDateListByMonthAndYear(String monthName, String year) {

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.MONTH, (Month.valueOf(monthName.trim().toUpperCase()).getValue() - 1));

		cal.set(Calendar.YEAR, Integer.parseInt(year));

		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		List<Date> dateList = new ArrayList<>();

		for (int i = 0; i < maxDay; i++) {

			cal.set(Calendar.DAY_OF_MONTH, i + 1);

			Date date = cal.getTime();

			dateList.add(date);
		}

		return dateList;

	}

	public static List<Date> getDateListByYear(String year) {

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.YEAR, Integer.parseInt(year));

		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_YEAR);

		List<Date> dateList = new ArrayList<>();

		for (int i = 0; i < maxDay; i++) {

			cal.set(Calendar.DAY_OF_YEAR, i + 1);

			Date date = cal.getTime();

			dateList.add(date);
		}
		return dateList;
	}

	public static int getDayOfMonth(Date aDate) {
		int dayNo = 0;

		if (aDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aDate);
			return cal.get(Calendar.DAY_OF_MONTH);
		} else {
			return 0;
		}

	}

	public static double providePercentWithTwoDecimalDigits(int partNumber, int fullNumber) {
		if (fullNumber == 0) {
			return 0;
		}
		return Double.parseDouble(new BigDecimal(
				(Double.parseDouble(String.valueOf(partNumber)) / Double.parseDouble(String.valueOf(fullNumber))) * 100)
						.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
	}

	public static String convertDateToStringTimeWithAmPm(Date date) {
		return new SimpleDateFormat("hh:mm a").format(date);
	}

	public static String provideDayNameByDate(Date date) {
		return new SimpleDateFormat("EEEE").format(date);
	}

	public static String getImagePath(String imageFolder) {
		Properties prop = new Properties();
		InputStream inputStream = ApplicationUtils.class.getClassLoader()
				.getResourceAsStream("imagepath/image-path.properties");
		try {
			prop.load(inputStream);
			return prop.getProperty(imageFolder);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public static double convertTwoDecimalPlacedNumber(double input) {
		double output = 0;
		if (input != 0) {
			output = Double.parseDouble(new DecimalFormat("###0.##").format(input));
		}
		return output;
	}

	public static int provideSequenceStartNumber(List<Integer> sortedList, int numOfDays) {

		int counter = 0;

		int startDay = 0;

		int previousDay = 5000;

		for (int i = 0; i < sortedList.size(); i++) {
			int currentDay = sortedList.get(i);

			if ((previousDay + 1) == currentDay) {
				counter++;
			} else {
				counter = 0;
			}

			if (counter == 1) {
				startDay = sortedList.get(i);
			}

			if (counter == (numOfDays - 1)) {
				return startDay;
			}
			previousDay = sortedList.get(i);
		}

		return 0;
	}

	public static String provideStartAndEndDate(List<Integer> sortedList, int numOfDays, String year,
			String monthName) {
		String absentDays = null;
		String twoDigitStarDays = null;
		String twoDigitEndDays = null;

		if (provideSequenceStartNumber(sortedList, numOfDays) != 0) {
			int startDay = provideSequenceStartNumber(sortedList, numOfDays);
			int endDay = startDay + (numOfDays - 1);

			if (String.valueOf(startDay).length() < 2) {
				twoDigitStarDays = "0" + startDay;
			} else {
				twoDigitStarDays = String.valueOf(startDay);
			}

			if (String.valueOf(endDay).length() < 2) {
				twoDigitEndDays = "0" + endDay;
			} else {
				twoDigitEndDays = String.valueOf(endDay);
			}

			absentDays = twoDigitStarDays + "-" + getMonthNumberByMonthName(monthName) + "-" + year + "to"
					+ twoDigitEndDays + "-" + getMonthNumberByMonthName(monthName) + "-" + year;
		}

		return absentDays;
	}

	public static int[] calculateAge(String stringDate) {

		Date birthDate = parseStringToDate(stringDate);

		long differencelOng = TimeUnit.DAYS.convert((new Date().getTime() - birthDate.getTime()), TimeUnit.DAYS);

		long days = differencelOng / 86400000;

		int year = (int) days / 365;

		int month = (int) (days - (365 * year)) / 30;

		// int day = (int) (days - (365 * year + (month * 30)));
		int[] dateDiff = new int[2];
		dateDiff[0] = year;
		dateDiff[1] = month;

		return dateDiff;
	}

	public static Date formatDate(Date date) {
		Date parsedDate = null;
		SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
		String dmy = mdyFormat.format(date);
		System.out.println(dmy);

		try {
			parsedDate = mdyFormat.parse(dmy);
		} catch (ParseException e) {
			System.out.println(e);
		}
		return parsedDate;
	}

	/*
	 * public static String provideInstitueId(HttpServletRequest request) {
	 * SecurityContext securityContext = SecurityContextHolder.getContext();
	 * OAuth2Authentication requestingUser = (OAuth2Authentication)
	 * securityContext.getAuthentication(); if (requestingUser.isClientOnly()) {
	 * return request.getParameter("instituteId"); } else { return
	 * UserInfoUtils.getLoggedInUserInstitute(); } }
	 */
	/*
	 * public static boolean isClient(HttpServletRequest request) {
	 * 
	 * boolean clientStatus = false;
	 * 
	 * if (request != null) { SecurityContext securityContext =
	 * SecurityContextHolder.getContext(); OAuth2Authentication requestingUser =
	 * (OAuth2Authentication) securityContext.getAuthentication(); if
	 * (requestingUser.isClientOnly()) { clientStatus = true; } }
	 * 
	 * return clientStatus;
	 * 
	 * }
	 */

	public static String prepareAddress(String divisionName, String districtName, String thanaName, String postOffice,
			String postalCode, String villageName, String roadNo, String houseNo) {

		String divName = null;
		String disName = null;
		String thanName = null;
		String poOffice = null;
		String postCode = null;
		String villName = null;
		String rodNo = null;
		String hoseNo = null;

		if (divisionName != null) {
			divName = divisionName.trim();
		}

		if (districtName != null) {
			disName = districtName.trim();
		}

		if (thanaName != null) {
			thanName = thanaName.trim();
		}

		if (postOffice != null) {
			poOffice = postOffice.trim();
		}

		if (postalCode != null) {
			postCode = postalCode.trim();
		}

		if (villageName != null) {
			villName = villageName.trim();
		}

		if (roadNo != null) {
			rodNo = roadNo.trim();
		}

		if (houseNo != null) {
			hoseNo = houseNo.trim();
		}

		return "Bangladesh" + " ;" + divName + " ;" + disName + " ;" + thanName + " ;" + poOffice + " ;" + postCode
				+ " ;" + villName + " ;" + rodNo + " ;" + hoseNo + " ;";
	}

	public static String checkMoblNo(String moblNo) {
		String phoneNo = "";
		int moblLenth = moblNo.length();
		if (moblLenth == 11 && moblNo.startsWith("01")) {
			phoneNo = "88" + moblNo;
		} else if (moblLenth == 13 && moblNo.startsWith("8801")) {
			phoneNo = moblNo;
		} else if (moblLenth == 14 && moblNo.startsWith("+8801")) {
			phoneNo = moblNo;
		}
		return phoneNo;
	}

	public static String generateBcryptHash12(String plainPassword) {
		String generatedHash = BCrypt.hashpw(plainPassword, BCrypt.gensalt(12));
		System.out.println("Generated Bycrypt Hash12 for : " + plainPassword + " is: " + generatedHash);
		return generatedHash;
	}

	public static void main(String[] args) {
		System.out.println(generateBcryptHash12("1234"));
	}

	public static String makeEncodedAccessToken(String accessToken) throws NoSuchAlgorithmException {

		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(accessToken.getBytes());
			byte byteData[] = md.digest();

			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static String isNotNullString(String stringValue) {
		if (stringValue != null) {
			return stringValue;
		} else {
			return null;
		}
	}

	public static boolean isNotNullObject(Object object) {
		if (object != null) {
			return true;
		}
		return false;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	/*
	 * public static List<DoctorVisitingScheduleDTO> sortDayWiseDoctorVisiting(
	 * List<DoctorVisitingScheduleDTO> doctorVisitingScheduleDTOs) {
	 * 
	 * List<DoctorVisitingScheduleDTO> sortedDoctorSchedules = new
	 * ArrayList<>();
	 * 
	 * String currentDayName = provideDayNameByDate(new Date());
	 * 
	 * List<DoctorVisitingScheduleDTO> currentDaySchedules =
	 * doctorVisitingScheduleDTOs.stream() .filter(dto ->
	 * dto.getScheduleDay().equals(currentDayName)).collect(Collectors.toList())
	 * ; sortedDoctorSchedules.addAll(currentDaySchedules);
	 * 
	 * for (String day : WEEK_DAYS) { for (DoctorVisitingScheduleDTO schedule :
	 * doctorVisitingScheduleDTOs) { if (schedule.getScheduleDay().equals(day)
	 * && !currentDaySchedules.contains(schedule)) {
	 * sortedDoctorSchedules.add(schedule); } } }
	 * 
	 * 
	 * List<DoctorVisitingScheduleDTO> tempSchedules = new ArrayList<>();
	 * 
	 * tempSchedules.addAll(sortedDoctorSchedules);
	 * 
	 * DoctorVisitingScheduleDTO firstDaySchedule =
	 * sortedDoctorSchedules.get(0);
	 * 
	 * for(DoctorVisitingScheduleDTO dto : tempSchedules){
	 * 
	 * if(dto.getScheduleDay().equals(currentDayName)){
	 * sortedDoctorSchedules.remove(dto); sortedDoctorSchedules.add(0, dto);
	 * break; } } sortedDoctorSchedules.remove(firstDaySchedule);
	 * 
	 * sortedDoctorSchedules.add(sortedDoctorSchedules.size() - 1,
	 * firstDaySchedule);
	 * 
	 * return sortedDoctorSchedules; }
	 */

	public static String saveTopic(String imgPath, UploadedFile uploadFile, String topicName, Long userId) {

		String imageName = null;
		Path path = null;

		path = Paths.get(StaticValueProvider.IMAGE_PATH + topicName + "-" + userId + ".pdf");
		try {
			Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
			imageName = topicName + "-" + userId + ".pdf";
		} catch (IOException e) {
			System.out.println(e);
		}

		return imageName;
	}

	public static String saveImage(String imgPath, UploadedFile uploadFile, int imageTypeId, Long userId) {

		String imageName = null;
		Path path = null;
		switch (imageTypeId) {

		case 1000:
			path = Paths
					.get(StaticValueProvider.IMAGE_PATH + ImageType.STUDENT_IMAGE.getCode() + "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.STUDENT_IMAGE.getCode() + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		case 2000:
			path = Paths
					.get(StaticValueProvider.IMAGE_PATH + ImageType.TEACHER_IMAGE.getCode() + "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.TEACHER_IMAGE.getCode() + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		case 4001:
			path = Paths
					.get(StaticValueProvider.IMAGE_PATH + ImageType.INSTITUTE_LOGO.getCode() + "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.INSTITUTE_LOGO.getCode() + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		}
		return imageName;
	}

	public static String saveImage(String imgPath, UploadedFile uploadFile, int imageTypeId, Long userId,
			Long problemId) {

		String imageName = null;
		Path path = null;

		switch (imageTypeId) {

		case 7000:
			path = Paths.get(StaticValueProvider.IMAGE_PATH + ImageType.NOTICE_PHOTO.getCode() + "_" + problemId + "_"
					+ userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.NOTICE_PHOTO.getCode() + "_" + problemId + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;
			
		case 7001:
			path = Paths.get(StaticValueProvider.IMAGE_PATH + ImageType.GROUP_NOTICE_PHOTO.getCode() + "_" + problemId + "_"
					+ userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.GROUP_NOTICE_PHOTO.getCode() + "_" + problemId + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		case 5000:
			path = Paths.get(StaticValueProvider.IMAGE_PATH + problemId + "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = problemId + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		case 6001:
			path = Paths.get(StaticValueProvider.IMAGE_PATH + ImageType.SOLUTION_PHOTO_1.getCode() + "_" + problemId
					+ "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.SOLUTION_PHOTO_1.getCode() + "_" + problemId + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		case 6002:
			path = Paths.get(StaticValueProvider.IMAGE_PATH + ImageType.SOLUTION_PHOTO_2.getCode() + "_" + problemId
					+ "_" + userId + ".png");
			try {
				Files.write(path, ByteStreams.toByteArray(uploadFile.getInputstream()));
				imageName = ImageType.SOLUTION_PHOTO_2.getCode() + "_" + problemId + "_" + userId + ".png";
			} catch (IOException e) {
				System.out.println(e);
			}
			return imageName;

		}
		return imageName;
	}

	private static String provideCustomeId() {
		String uniqueID = UUID.randomUUID().toString();
		String finalUniqueId = uniqueID.substring(0, Math.min(uniqueID.length(), 4));
		return finalUniqueId.toUpperCase();
	}

	public static String provideBase64Image(String imageName) {
		System.out.println(imageName);
		String photoName = null;
		try {
			if (imageName != null) {
				photoName = Base64.getEncoder().encodeToString(
						FileUtils.readFileToByteArray(new File(StaticValueProvider.IMAGE_PATH + imageName)));
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		return "data:image/png;base64," + photoName;
	}

}
