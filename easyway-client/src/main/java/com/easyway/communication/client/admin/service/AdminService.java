package com.easyway.communication.client.admin.service;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.common.util.AdminApiConsumer;
import com.easyway.communication.client.common.util.AdminDataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;

@Service
public class AdminService {

	public void saveOrUpdateDivision(DivisionDTO divisionDTO) {
		AdminDataExporter.sendDivisionDtos(StaticValueProvider.LOGIN_ADMIN_URI, "save/division", divisionDTO);
	}

	public void saveOrUpdateDistrict(DistrictDTO districtDTO) {
		AdminDataExporter.sendDistrictDtos(StaticValueProvider.LOGIN_ADMIN_URI, "save/district", districtDTO);
	}

	public List<DivisionDTO> findDivisionsList() {
		List<DivisionDTO> divisionDTOs = findDivisions();
		Collections.reverse(divisionDTOs);
		return divisionDTOs;
	}

	public List<DivisionDTO> findDivisions() {
		return AdminApiConsumer.consumeDivisionDtos(StaticValueProvider.LOGIN_ADMIN_URI, "find/all/division");
	}

	public List<DistrictDTO> findDistrictsList() {
		return AdminApiConsumer.consumeDistrictDtos(StaticValueProvider.LOGIN_ADMIN_URI, "find/all/district");
	}

	public void saveGeneralInfo(GeneralInfoDTO generalInfoDTO) {
		AdminDataExporter.sendGeneralInfoDtos(StaticValueProvider.LOGIN_ADMIN_URI, "save/generalinfo", generalInfoDTO);
		
	}

	public List<GeneralInfoDTO> findGeneralInfoList() {
		List<GeneralInfoDTO> generalInfoDTOs=findGeneralInfo();
		Collections.reverse(generalInfoDTOs);
		return generalInfoDTOs;
	}
	
	public List<GeneralInfoDTO> findGeneralInfo(){
		return AdminApiConsumer.consumeGeneralInfoDTOs(StaticValueProvider.LOGIN_ADMIN_URI, "find/all/generalinfo");
	}

}
