package com.easyway.communication.client.student.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.common.enums.ImageType;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.student.model.ProblemConfigDTO;
import com.easyway.communication.client.student.model.ProblemSolutionDTO;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.student.service.StudentInfoService;
import com.easyway.communication.client.student.service.StudentProblemService;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Controller
@ManagedBean
@Scope("session")
public class StudentInfoController {

	@Autowired
	private StudentProblemService studentProblemService;

	@Autowired
	private StudentInfoService studentInfoService;

	public UserDetailsDTO studentInfos;

	public StudentDTO studentDTO;

	private List<ProblemSolutionDTO> studentProblemConfigDTOs;

	public UploadedFile studentImage;

	@PostConstruct
	public void init() {
	}

	public void updateStudentInfo() {
		try {

			if (!studentImage.getFileName().isEmpty()) {
				studentInfos.setStudent(prepareStudentImage(studentImage, studentInfos));
			}
			studentInfoService.updateStudentInfo(studentInfos);
			findStudentInfos();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private StudentDTO prepareStudentImage(UploadedFile studentImage, UserDetailsDTO studentInfos) {
		StudentDTO studentDTO = new StudentDTO();
		BeanUtils.copyProperties(studentInfos.getStudent(), studentDTO);
		studentDTO.setPhotoName(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH, studentImage,
				ImageType.STUDENT_IMAGE.getCode(), studentInfoService.provideStudentId()));
		studentDTO.setDepartment(null);
		return studentDTO;
	}

	public String findStudentInfos() {
		try {
			studentInfos = studentInfoService.findStudentInfoByHttp();
			studentInfos
					.setLogoWithBase64(ApplicationUtils.provideBase64Image(studentInfos.getStudent().getPhotoName()));
			Long studentId = studentProblemService.provideStudentId();
			studentProblemConfigDTOs = studentProblemService.findPrbSolutionByStudent(studentId);
			studentInfos.setTotalProblemSolution(studentProblemConfigDTOs.stream().count());
			for (ProblemSolutionDTO problemSolutionDTO : studentProblemConfigDTOs) {
				problemSolutionDTO.setSolutionPhotoWithBase1(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto1()));
				problemSolutionDTO.setSolutionPhotoWithBase2(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto2()));

			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return "/student/profile.xhtml?faces-redirect=true";
	}

	public UserDetailsDTO getStudentInfos() {
		if (studentInfos == null)
			studentInfos = new UserDetailsDTO();
		return studentInfos;
	}

	public void setStudentInfos(UserDetailsDTO studentInfos) {
		this.studentInfos = studentInfos;
	}

	public StudentDTO getStudentDTO() {
		if (studentDTO == null) {
			studentDTO = new StudentDTO();
		}
		return studentDTO;
	}

	public void setStudentDTO(StudentDTO studentDTO) {
		this.studentDTO = studentDTO;
	}

	public UploadedFile getStudentImage() {
		return studentImage;
	}

	public void setStudentImage(UploadedFile studentImage) {
		this.studentImage = studentImage;
	}

	public List<ProblemSolutionDTO> getStudentProblemConfigDTOs() {
		return studentProblemConfigDTOs;
	}

	public void setStudentProblemConfigDTOs(List<ProblemSolutionDTO> studentProblemConfigDTOs) {
		this.studentProblemConfigDTOs = studentProblemConfigDTOs;
	}

}
