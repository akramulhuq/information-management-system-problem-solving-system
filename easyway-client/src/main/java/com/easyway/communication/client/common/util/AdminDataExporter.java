package com.easyway.communication.client.common.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.users.model.SignUpDTO;

public class AdminDataExporter {

	public static void sendSignUpDtoToApi(String uri, String uriExtension, SignUpDTO signpUpDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<SignUpDTO> request = new HttpEntity<>(signpUpDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, SignUpDTO.class);
	}

	public static void sendDivisionDtos(String uri, String uriExtension, DivisionDTO divisionDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<DivisionDTO> request = new HttpEntity<>(divisionDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, DivisionDTO.class);
	}

	public static void sendDistrictDtos(String uri, String uriExtension, DistrictDTO districtDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<DistrictDTO> request = new HttpEntity<>(districtDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, DistrictDTO.class);
	}

	public static void sendGeneralInfoDtos(String uri, String uriExtension, GeneralInfoDTO generalInfoDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<GeneralInfoDTO> request = new HttpEntity<>(generalInfoDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, GeneralInfoDTO.class);
	}
}
