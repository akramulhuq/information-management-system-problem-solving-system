package com.easyway.communication.client.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.teacher.model.FolderDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.teacher.model.TopicDTO;
import com.easyway.communication.client.users.model.SignUpDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;
import com.easyway.communication.client.users.model.UsersDTO;
import com.easyway.communication.client.users.model.UsersRoleDTO;

public class ApiConsumer {

	public static List<SignUpDTO> consumeSignUpDTOs(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<SignUpDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				SignUpDTO[].class);
		List<SignUpDTO> studentDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(studentDTOs);
	}

	public static UsersDTO consumeLoginSuccessUser(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		String fullRequestUrl = StaticValueProvider.BASE_URL + uri + uriExtension;
		ResponseEntity<UsersDTO> response = template.getForEntity(fullRequestUrl, UsersDTO.class);
		UsersDTO userDTO = response.getBody();
		return userDTO;
	}

	public static List<UsersRoleDTO> consumeLoginUserRoles(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<UsersRoleDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, UsersRoleDTO[].class);
		List<UsersRoleDTO> userRoleDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(userRoleDTOs);
	}

	public static UserDetailsDTO findUserDetailsOfLoginUser(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<UserDetailsDTO> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, UserDetailsDTO.class);
		UserDetailsDTO userDetailsDTO = response.getBody();
		return userDetailsDTO;
	}

	public static List<TeacherDTO> findTeacherDTO(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<TeacherDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				TeacherDTO[].class);
		List<TeacherDTO> teacherDTOs = Arrays.asList(response.getBody());
		return teacherDTOs;
	}

	public static List<InstituteDTO> consumeInstituteDtos(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<InstituteDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, InstituteDTO[].class);
		return Arrays.asList(response.getBody());

	}

	public static List<TeacherDTO> consumeTeacherDtos(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<TeacherDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				TeacherDTO[].class);
		return Arrays.asList(response.getBody());

	}

	public static List<FolderDTO> consumeFolderDTO(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<FolderDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				FolderDTO[].class);
		return Arrays.asList(response.getBody());
	}

	public static List<TopicDTO> consumeTopicDtoToApi(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<TopicDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				TopicDTO[].class);
		return Arrays.asList(response.getBody());
	}

	public static List<GroupDTO> consumeGroupDto(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<GroupDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				GroupDTO[].class);
		return Arrays.asList(response.getBody());
	}

	public static List<GroupNoticeDTO> consumeGroupNoticeDto(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<GroupNoticeDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, GroupNoticeDTO[].class);
		return Arrays.asList(response.getBody());
	}

}
