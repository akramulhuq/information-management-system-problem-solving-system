package com.easyway.communication.client.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.student.model.GroupConfigDTO;
import com.easyway.communication.client.student.model.ProblemConfigDTO;
import com.easyway.communication.client.student.model.ProblemSolutionDTO;
import com.easyway.communication.client.users.model.SignUpDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;
import com.easyway.communication.client.users.model.UsersDTO;
import com.easyway.communication.client.users.model.UsersRoleDTO;

public class StudentApiConsumer {

	public static List<ProblemConfigDTO> consumeProblemConfigDTOs(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<ProblemConfigDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, ProblemConfigDTO[].class);
		List<ProblemConfigDTO> problemDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(problemDTOs);
	}

	public static List<ProblemSolutionDTO> sonsumeProblemSolutionDto(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<ProblemSolutionDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, ProblemSolutionDTO[].class);
		return Arrays.asList(response.getBody());

	}

	public static List<GroupConfigDTO> consumeGroupConfigDTO(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<GroupConfigDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, GroupConfigDTO[].class);
		return Arrays.asList(response.getBody());

	}

}
