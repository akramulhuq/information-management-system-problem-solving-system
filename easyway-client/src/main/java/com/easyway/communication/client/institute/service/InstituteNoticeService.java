package com.easyway.communication.client.institute.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.DataExporter;
import com.easyway.communication.client.common.util.InstituteApiConsumer;
import com.easyway.communication.client.common.util.InstituteDataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.common.util.StudentDataExporter;
import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.student.service.StudentInfoService;
import com.easyway.communication.client.teacher.service.TeacherInfoService;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class InstituteNoticeService {

	@Autowired
	private TeacherInfoService teacherInfoService;

	@Autowired
	private StudentInfoService studentInfoService;

	public void postGroupNotice(GroupNoticeDTO groupNoticeDTO, GroupDTO groupDTO) {
		groupNoticeDTO.setGroup(groupDTO);
		DataExporter.sendGroupNoticeDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI, "post/group/notice",
				groupNoticeDTO);
	}

	public void postGroupNotice(GroupNoticeDTO groupNoticeDTO) {
		DataExporter.sendGroupNoticeDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI, "post/group/notice/by-student",
				groupNoticeDTO);
	}

	public ResponseEntity<GroupNoticeDTO> saveGrpNoticeWithOutPhto(GroupNoticeDTO groupNoticeDTO, GroupDTO groupDTO) {
		groupNoticeDTO.setGroup(groupDTO);
		return DataExporter.sendGroupNoticeDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI,
				"post/group/notice/with-out/images", groupNoticeDTO);
	}

	public ResponseEntity<GroupNoticeDTO> saveGrpNoticeWithOutPhto(GroupNoticeDTO groupNoticeDTO) {
		return DataExporter.sendGroupNoticeDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI,
				"post/group/notice/with-out/images/by-student", groupNoticeDTO);
	}

	public List<InstituteNoticeDTO> findNoticeByTeacherId(Long teacherId) {
		List<InstituteNoticeDTO> teachereNoticeDTOs = new ArrayList<>();
		List<InstituteNoticeDTO> instituteNoticeDTOs = findAllInstitutesNotice();
		for (InstituteNoticeDTO instituteNoticeDTO : instituteNoticeDTOs) {
			if (instituteNoticeDTO.getTeacher().getTeacherId() != null) {
				if (instituteNoticeDTO.getTeacher().getTeacherId().equals(teacherId)) {
					teachereNoticeDTOs.add(instituteNoticeDTO);
				}
			}
		}
		return teachereNoticeDTOs;
	}

	public List<InstituteNoticeDTO> findNoticeByInstituteId() {
		return findAllInstitutesNotice().stream()
				.filter(notice -> notice.getInstitute().getInstituteId().equals(provideInstituteId()))
				.collect(Collectors.toList());
	}

	public List<InstituteNoticeDTO> findAllInstitutesNotice() {
		return InstituteApiConsumer.consumeInstituteNoticeDto(StaticValueProvider.LOGIN_INSTITUTE_URI,
				"find/all/institute/notice");
	}

	public List<GroupNoticeDTO> findGroupNoticeByInstitute(GroupDTO groupDTO) {
		List<GroupNoticeDTO> prepareGroupNoticeDTOs = new ArrayList<>();
		Long teacherId = teacherInfoService.provideTeacherId();
		List<GroupNoticeDTO> groupNoticeDTOs = findAllGroupNotice().stream()
				.filter(group -> group.getGroup().getGroupId().equals(groupDTO.getGroupId()))
				.collect(Collectors.toList());
		groupNoticeDTOs.forEach(groupBy -> {
			if (groupBy.getTeacher().getTeacherId() != null && groupBy.getTeacher() != null) {
				groupBy.setInstitute(null);
			}
		});

		for (GroupNoticeDTO notice : groupNoticeDTOs) {
			if (notice.getTeacher().getTeacherId() != null && teacherId != null) {
				if (notice.getTeacher().getTeacherId().equals(teacherId)) {
					prepareGroupNoticeDTOs.add(notice);
				}
			} else if (notice.getInstitute().getInstituteId() != null && provideInstituteId() != null
					&& notice.getStudent().getStudentId() == null) {
				if (notice.getInstitute().getInstituteId().equals(provideInstituteId())) {
					prepareGroupNoticeDTOs.add(notice);
				}
			}
		}
		Collections.reverse(prepareGroupNoticeDTOs);
		return prepareGroupNoticeDTOs;
	}

	public List<GroupNoticeDTO> findAllGroupNotice() {
		return ApiConsumer.consumeGroupNoticeDto(StaticValueProvider.LOGIN_INSTITUTE_URI, "find/all/group/notice");
	}

	public void createGroupByInstitute(GroupDTO groupDTO) {
		InstituteDTO institute = new InstituteDTO();
		institute.setInstituteId(provideInstituteId());
		groupDTO.setInstitute(institute);
		DataExporter.sendGroupDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI, "create/group", groupDTO);
	}

	public List<GroupDTO> findGroupByInstituteId(Long instituteId) {
		return findAllGroup().stream().filter(group -> group.getInstitute().getInstituteId().equals(instituteId))
				.collect(Collectors.toList());
	}

	public List<GroupDTO> findAllGroup() {
		return ApiConsumer.consumeGroupDto(StaticValueProvider.LOGIN_INSTITUTE_URI, "find/all/group");
	}

	public List<InstituteNoticeDTO> findInstituteNoticeById(Long instituteId) {
		List<InstituteNoticeDTO> instituteNoticeDTOs = InstituteApiConsumer.consumeInstituteNoticeDto(
				StaticValueProvider.LOGIN_INSTITUTE_URI, "find/institute/notice/by-id?instituteId=" + instituteId);
		instituteNoticeDTOs.forEach(notice -> {
			if (notice.getTeacher().getTeacherId() != null) {
				notice.setInstitute(null);
			}
		});

		Collections.reverse(instituteNoticeDTOs);
		return instituteNoticeDTOs;
	}

	public ResponseEntity<InstituteNoticeDTO> postNoticeByInstitute(InstituteNoticeDTO instituteNoticeDTO) {
		if (provideInstituteId() != null) {
			instituteNoticeDTO.setInstitute(provideInstitute());
		}
		return InstituteDataExporter.sendInstituteNoticeDtoToApi(StaticValueProvider.LOGIN_INSTITUTE_URI,
				"post/institute/notice/by-id", instituteNoticeDTO);
	}

	public InstituteDTO provideInstitute() {
		InstituteDTO instituteDTO = new InstituteDTO();
		instituteDTO.setInstituteId(provideInstituteId());
		return instituteDTO;
	}

	public UserDetailsDTO findInstituteInfoByHttp() {
		UserDetailsDTO userDetailsDTO = ApplicationUtilsMB.provideCurrentUserDetils();
		System.out.println(userDetailsDTO.getUsername());
		return userDetailsDTO;
	}

	public Long provideInstituteId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getInstitute().getInstituteId();
	}
}
