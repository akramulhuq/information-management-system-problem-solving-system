package com.easyway.communication.client.common.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.admin.service.AdminService;

@Service
public class CommonService {

	@Autowired
	private AdminService adminService;

	public List<GeneralInfoDTO> provideGeneralInfoByid(String typeId) {
		List<GeneralInfoDTO> generalInfoDTOs = adminService.findGeneralInfo();
		if (generalInfoDTOs != null) {
			return generalInfoDTOs.stream().filter(info -> info.getTypeId().equals(typeId))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}

	}

	public StreamedContent provideStreamedContent(String contentPath) {

		try {
			return new DefaultStreamedContent(
					new ByteArrayInputStream(Files.readAllBytes(new File(contentPath).toPath())));
		} catch (IOException e) {
			System.out.println(e);
			return null;
		}

	}

	public String provideTestName(String testName, String name) {
		if (testName != null && name != null) {
			return testName + ", " + name;
		} else {
			return null;
		}
	}

	public String provideTestName(String name) {
		if (name != null) {
			return name;
		} else {
			return null;
		}
	}

}
