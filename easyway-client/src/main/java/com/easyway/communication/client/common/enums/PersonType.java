package com.easyway.communication.client.common.enums;

public enum PersonType {

	HOSPITAL_DRIVER("33300", "Ambulance Driver"), HOSPITAL_CLEANER("44400", "Cleaner"), HOSPITAL_MANAGER("55500",
			"Manager"), HOSPITAL_OFFICER("66600", "Officer"), HOSPITAL_LAB_ASSISTENT("77700", "Lab Assistent");

	private final String code;

	private final String personTypeName;

	private PersonType(String code, String personTypeName) {
		this.code = code;
		this.personTypeName = personTypeName;
	}

	public String getCode() {
		return code;
	}

	public String getPersonTypeName() {
		return personTypeName;
	}
}
