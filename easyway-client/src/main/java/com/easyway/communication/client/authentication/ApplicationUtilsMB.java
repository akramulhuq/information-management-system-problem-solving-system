package com.easyway.communication.client.authentication;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.common.enums.UserRole;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Controller
@ManagedBean
@Scope("session")
public class ApplicationUtilsMB {
	public Collection<SimpleGrantedAuthority> authorities;

	public boolean userRoleAsAdmin;
	public boolean userRoleAsUser;
	public boolean userRoleAsGuest;
	public boolean userRoleInstitute;
	public boolean userRoleTeacher;
	public boolean userRoleStudent;

	@PostConstruct
	public void init() {
		authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
	}

	public String provideCurrentUser() {
		String currentUsername = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		return currentUsername;
	}

	public static UserDetailsDTO provideCurrentUserDetils() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDetailsDTO userDetailsDTO = (UserDetailsDTO) session.getAttribute("userDetails");
		return userDetailsDTO;
	}

	public boolean checkUserRole(String roleName) {
		if (authorities.toString().contains(roleName))
			return true;
		else
			return false;
	}

	public boolean isUserRoleAsGuest() {
		if (checkUserRole(UserRole.GUEST.getRole().toString()))
			userRoleAsGuest = true;
		return userRoleAsGuest;
	}

	public void setUserRoleAsGuest(boolean userRoleAsGuest) {
		this.userRoleAsGuest = userRoleAsGuest;
	}

	public boolean isUserRoleAsAdmin() {
		if (checkUserRole(UserRole.SUPER_ADMIN.getRole().toString()))
			userRoleAsAdmin = true;
		return userRoleAsAdmin;
	}

	public void setUserRoleAsUser(boolean userRoleAsUser) {
		this.userRoleAsUser = userRoleAsUser;
	}

	public boolean isUserRoleInstitute() {
		if (checkUserRole(UserRole.INSTITUTE.getRole().toString()))
			userRoleInstitute = true;
		return userRoleInstitute;
	}

	public void setUserRoleInstitute(boolean userRoleInstitute) {
		this.userRoleInstitute = userRoleInstitute;
	}

	public boolean isUserRoleTeacher() {
		if (checkUserRole(UserRole.TEACHER.getRole().toString()))
			userRoleTeacher = true;
		return userRoleTeacher;
	}

	public void setUserRoleTeacher(boolean userRoleTeacher) {
		this.userRoleTeacher = userRoleTeacher;
	}

	public boolean isUserRoleStudent() {
		if (checkUserRole(UserRole.STUDENT.getRole().toString()))
			userRoleStudent = true;
		return userRoleStudent;
	}

	public void setUserRoleStudent(boolean userRoleStudent) {
		this.userRoleStudent = userRoleStudent;
	}

}
