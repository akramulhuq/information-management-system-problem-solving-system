package com.easyway.communication.client.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@ManagedBean
@Scope("session")
public class BirthDateProvider {

	private int date;

	private int month;

	private int year;

	private List<Integer> datesList;
	private List<Integer> monthsList;
	private List<Integer> yearsList;

	public List<Integer> provideYears() {
		yearsList = new ArrayList<>();
		try {
			int yaerStart = 1900, yearEnd = 2020;
			for (int year = yaerStart; year <= yearEnd; year++) {
				yearsList.add(year);

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		Collections.reverse(yearsList);
		return yearsList;
	}

	public List<Integer> provideMonths() {
		monthsList = new ArrayList<>();
		try {
			int monthStart = 1, monthEnd = 12;
			for (int month = monthStart; month <= monthEnd; month++) {
				monthsList.add(month);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return monthsList;
	}

	public List<Integer> provideDays() {
		datesList = new ArrayList<>();
		try {
			int dayStart = 1, dayEnd = 31;
			for (int day = dayStart; day <= dayEnd; day++) {
				datesList.add(day);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return datesList;
	}

	public List<Integer> getDatesList() throws Exception {
		provideDays();
		return datesList;
	}

	public void setDatesList(List<Integer> datesList) {
		this.datesList = datesList;
	}

	public List<Integer> getMonthsList() throws Exception {
		provideMonths();
		return monthsList;
	}

	public void setMonthsList(List<Integer> monthsList) {
		this.monthsList = monthsList;
	}

	public List<Integer> getYearsList() throws Exception {
		provideYears();
		return yearsList;
	}

	public void setYearsList(List<Integer> yearsList) {
		this.yearsList = yearsList;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}
