package com.easyway.communication.client.student.model;

import com.easyway.communication.client.teacher.model.TeacherDTO;

public class ProblemSolutionDTO {

	private Long solutionId;

	private String solutionByWrite;

	private String solutionByPhoto1;

	private String solutionByPhoto2;

	private String solutionNote;

	private String solutionDate;

	private int activeStatus;

	private int solutionStatus;
	
	

	private TeacherDTO teacher;

	private StudentDTO student;

	private ProblemConfigDTO problemConfig;

	private String solutionPhotoWithBase1;

	private String solutionPhotoWithBase2;

	private String spersonPhotoWithBase2;

	public Long getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(Long solutionId) {
		this.solutionId = solutionId;
	}

	public String getSolutionByWrite() {
		return solutionByWrite;
	}

	public void setSolutionByWrite(String solutionByWrite) {
		this.solutionByWrite = solutionByWrite;
	}

	public String getSolutionByPhoto1() {
		return solutionByPhoto1;
	}

	public void setSolutionByPhoto1(String solutionByPhoto1) {
		this.solutionByPhoto1 = solutionByPhoto1;
	}

	public String getSolutionByPhoto2() {
		return solutionByPhoto2;
	}

	public void setSolutionByPhoto2(String solutionByPhoto2) {
		this.solutionByPhoto2 = solutionByPhoto2;
	}

	public String getSolutionNote() {
		return solutionNote;
	}

	public void setSolutionNote(String solutionNote) {
		this.solutionNote = solutionNote;
	}

	public String getSolutionDate() {
		return solutionDate;
	}

	public void setSolutionDate(String solutionDate) {
		this.solutionDate = solutionDate;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public TeacherDTO getTeacher() {
		if (teacher == null)
			teacher = new TeacherDTO();
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public StudentDTO getStudent() {
		if (student == null)
			student = new StudentDTO();
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public ProblemConfigDTO getProblemConfig() {
		if (problemConfig == null)
			problemConfig = new ProblemConfigDTO();
		return problemConfig;
	}

	public void setProblemConfig(ProblemConfigDTO problemConfig) {
		this.problemConfig = problemConfig;
	}

	public String getSolutionPhotoWithBase1() {
		return solutionPhotoWithBase1;
	}

	public void setSolutionPhotoWithBase1(String solutionPhotoWithBase1) {
		this.solutionPhotoWithBase1 = solutionPhotoWithBase1;
	}

	public String getSolutionPhotoWithBase2() {
		return solutionPhotoWithBase2;
	}

	public void setSolutionPhotoWithBase2(String solutionPhotoWithBase2) {
		this.solutionPhotoWithBase2 = solutionPhotoWithBase2;
	}

	public int getSolutionStatus() {
		return solutionStatus;
	}

	public void setSolutionStatus(int solutionStatus) {
		this.solutionStatus = solutionStatus;
	}

	public String getSpersonPhotoWithBase2() {
		return spersonPhotoWithBase2;
	}

	public void setSpersonPhotoWithBase2(String spersonPhotoWithBase2) {
		this.spersonPhotoWithBase2 = spersonPhotoWithBase2;
	}

}
