package com.easyway.communication.client.student.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class StudentNoticeService {

	public Long provideStudentId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getStudent().getStudentId();
	}
}
