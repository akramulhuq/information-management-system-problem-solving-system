package com.easyway.communication.client.institute.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.admin.controller.AdminController;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.common.enums.GeneralInfoType;
import com.easyway.communication.client.common.enums.ImageType;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.CommonService;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Controller
@ManagedBean
@Scope("session")
public class InstituteInfoController {

	@Autowired
	private InstituteInfoService instituteInfoService;

	@Autowired
	private CommonService commonService;

	private InstituteDTO instituteDTO;

	private InstituteNoticeDTO instituteNoticeDTO;

	private List<InstituteNoticeDTO> instituteNoticeDTOs;

	private List<GeneralInfoDTO> designationDTOs;

	private List<GeneralInfoDTO> departmentDTOs;

	private UserDetailsDTO viewInstituteInfo;

	private UploadedFile uploadedFile;

	private UploadedFile teacherPhoto;

	private TeacherDTO teacherDTO;

	private List<TeacherDTO> teacherDTOs;

	@PostConstruct
	public void init() {
		findInstituteInfo();

		designationDTOs = commonService.provideGeneralInfoByid(GeneralInfoType.DESIGNATION.getCode());
		departmentDTOs = commonService.provideGeneralInfoByid(GeneralInfoType.DEPARTMENT.getCode());
	}

	public void updateInstituteInfo() {
		FacesContext mass = FacesContext.getCurrentInstance();
		try {
			if (uploadedFile.getFileName() != null && !uploadedFile.getFileName().isEmpty()) {
				viewInstituteInfo.setLogoName(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH, uploadedFile,
						ImageType.INSTITUTE_COVER_PHTO.getCode(), instituteInfoService.provideInstituteId()));
			}
			instituteInfoService.updateInstituteInfo(viewInstituteInfo);
			findInstituteInfo();
			mass.addMessage(null, new FacesMessage("Institute info update successfully!"));
		} catch (Exception e) {
			mass.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Code not institute approve Successfully.!", ""));
		}
	}

	public String findInstituteInfo() {
		try {
			viewInstituteInfo = instituteInfoService.findInstituteInfoByHttp();
			viewInstituteInfo.setLogoWithBase64(
					ApplicationUtils.provideBase64Image(viewInstituteInfo.getInstitute().getLogoPhoto()));
		} catch (Exception e) {
			System.out.println(e);
		}
		return "/institute/institute-information.xhtml?faces-redirect=true";
	}

	public String findAllTeacherInfo() {
		teacherDTOs = instituteInfoService.findTeacherByInstituteId();
		for (TeacherDTO teacherDTO : teacherDTOs) {
			teacherDTO.setPhotoWithBase64(ApplicationUtils.provideBase64Image(teacherDTO.getPhotoName()));
		}
		return "/institute/teacher-information.xhtml?faces-redirect=true";
	}

	/* teacher registration by this method */

	public void saveTeacher(int signUpAs) {
		FacesContext mass = FacesContext.getCurrentInstance();
		ResponseEntity<TeacherDTO> request = instituteInfoService.saveTeacher(teacherDTO, signUpAs);
		if (!teacherPhoto.getFileName().isEmpty()) {
			teacherDTO.setPhotoName(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH, teacherPhoto,
					ImageType.TEACHER_IMAGE.getCode(), request.getBody().getTeacherId()));
		}
		teacherDTO.setBirthDate(ApplicationUtils.parseDateToAgeString(teacherDTO.getCreateDate()));
		teacherDTO.setTeacherId(request.getBody().getTeacherId());
		teacherDTO.setUserName(teacherDTO.getUserName());
		teacherDTO.setPassword(teacherDTO.getPassword());
		instituteInfoService.saveTeacher(teacherDTO, signUpAs);

		teacherDTO = new TeacherDTO();
		mass.addMessage(null, new FacesMessage("Teacher reegistration successfully!"));

	}

	public InstituteDTO getInstituteDTO() {
		if (instituteDTO == null) {
			instituteDTO = new InstituteDTO();
		}
		return instituteDTO;
	}

	public void setInstituteDTO(InstituteDTO instituteDTO) {
		this.instituteDTO = instituteDTO;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public UserDetailsDTO getViewInstituteInfo() {
		if (viewInstituteInfo == null)
			viewInstituteInfo = new UserDetailsDTO();
		return viewInstituteInfo;
	}

	public void setViewInstituteInfo(UserDetailsDTO viewInstituteInfo) {
		this.viewInstituteInfo = viewInstituteInfo;
	}

	public InstituteNoticeDTO getInstituteNoticeDTO() {
		return instituteNoticeDTO;
	}

	public void setInstituteNoticeDTO(InstituteNoticeDTO instituteNoticeDTO) {
		this.instituteNoticeDTO = instituteNoticeDTO;
	}

	public List<InstituteNoticeDTO> getInstituteNoticeDTOs() {
		return instituteNoticeDTOs;
	}

	public void setInstituteNoticeDTOs(List<InstituteNoticeDTO> instituteNoticeDTOs) {
		this.instituteNoticeDTOs = instituteNoticeDTOs;
	}

	public TeacherDTO getTeacherDTO() {
		if (teacherDTO == null) {
			teacherDTO = new TeacherDTO();
		}
		return teacherDTO;
	}

	public void setTeacherDTO(TeacherDTO teacherDTO) {
		this.teacherDTO = teacherDTO;
	}

	public UploadedFile getTeacherPhoto() {
		return teacherPhoto;
	}

	public void setTeacherPhoto(UploadedFile teacherPhoto) {
		this.teacherPhoto = teacherPhoto;
	}

	public List<GeneralInfoDTO> getDesignationDTOs() {
		return designationDTOs;
	}

	public void setDesignationDTOs(List<GeneralInfoDTO> designationDTOs) {
		this.designationDTOs = designationDTOs;
	}

	public List<GeneralInfoDTO> getDepartmentDTOs() {
		return departmentDTOs;
	}

	public void setDepartmentDTOs(List<GeneralInfoDTO> departmentDTOs) {
		this.departmentDTOs = departmentDTOs;
	}

	public List<TeacherDTO> getTeacherDTOs() {
		findAllTeacherInfo();
		return teacherDTOs;
	}

	public void setTeacherDTOs(List<TeacherDTO> teacherDTOs) {
		this.teacherDTOs = teacherDTOs;
	}

}
