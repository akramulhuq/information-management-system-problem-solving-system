package com.easyway.communication.client.common.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.student.model.ProblemConfigDTO;
import com.easyway.communication.client.student.model.ProblemSolutionDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

public class StudentDataExporter {

	public static ResponseEntity<ProblemConfigDTO> sendProblemConfigDtoToApi(String uri, String uriExtension,
			ProblemConfigDTO problemConfigDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<ProblemConfigDTO> request = new HttpEntity<>(problemConfigDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		return template.exchange(url, HttpMethod.POST, request, ProblemConfigDTO.class);
	}

	public static void sendProblemSolutionDtoToApi(String uri, String uriExtension,
			ProblemSolutionDTO problemSolutionDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<ProblemSolutionDTO> request = new HttpEntity<>(problemSolutionDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, ProblemSolutionDTO.class);
	}

	public static void userDetailsDtoToApi(String uri, String uriExtension, UserDetailsDTO studentInfos) {
		RestTemplate template = new RestTemplate();
		HttpEntity<UserDetailsDTO> request = new HttpEntity<>(studentInfos);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, UserDetailsDTO.class);

	}

}
