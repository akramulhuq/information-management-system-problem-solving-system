package com.easyway.communication.client.student.model;

import java.util.Date;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;

public class StudentDTO {

	private Long studentId;

	private String studentName;

	private int studentRoll;

	private String registrationId;

	private String birthDate;

	private String photoName;

	private String batchName;

	private String contactNumber;

	private String email;

	private String bloodGroup;

	private String expertiesSubject;

	private Date createDate;
	
	private String photoWithBase64;

	private GeneralInfoDTO department;

	private InstituteDTO institute;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(int studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getExpertiesSubject() {
		return expertiesSubject;
	}

	public void setExpertiesSubject(String expertiesSubject) {
		this.expertiesSubject = expertiesSubject;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public GeneralInfoDTO getDepartment() {
		if (department == null) {
			department = new GeneralInfoDTO();
		}
		return department;
	}

	public void setDepartment(GeneralInfoDTO department) {
		this.department = department;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public InstituteDTO getInstitute() {
		if (institute == null)
			institute = new InstituteDTO();
		return institute;
	}

	public void setInstitute(InstituteDTO institute) {
		this.institute = institute;
	}

	public String getPhotoWithBase64() {
		return photoWithBase64;
	}

	public void setPhotoWithBase64(String photoWithBase64) {
		this.photoWithBase64 = photoWithBase64;
	}

}
