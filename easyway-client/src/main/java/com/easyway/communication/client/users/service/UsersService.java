package com.easyway.communication.client.users.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.DataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.users.model.SignUpDTO;

@Service
public class UsersService {

	public void adminRegistration(SignUpDTO signpUpDTO, int signUpAs) {
		signpUpDTO.setPassword(ApplicationUtils.generateBcryptHash12(signpUpDTO.getPassword()));
		if (signpUpDTO.getSignedUpDate() != null) {
			signpUpDTO.setBirthDate(ApplicationUtils.parseDateToAgeString(signpUpDTO.getSignedUpDate()));
		}
		DataExporter.sendSignUpDtoToApi(StaticValueProvider.LOGIN_USER_URI,
				"registration/admin-user?signUpAs=" + signUpAs, signpUpDTO);
	}

	public List<SignUpDTO> allPendingUsers() {
		List<SignUpDTO> pendingUsers = ApiConsumer.consumeSignUpDTOs(StaticValueProvider.LOGIN_USER_URI,
				"/pending/find-all");
		return pendingUsers;
	}

	public void approveSingleUsers(SignUpDTO signUpDTO) {
		DataExporter.sendSignUpDtoToApi(StaticValueProvider.LOGIN_USER_URI, "approve/single-user", signUpDTO);
	}

}
