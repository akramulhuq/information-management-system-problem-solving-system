package com.easyway.communication.client.student.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.regexp.recompile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.DataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.common.util.StudentApiConsumer;
import com.easyway.communication.client.common.util.StudentDataExporter;
import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.institute.service.InstituteNoticeService;
import com.easyway.communication.client.student.model.GroupConfigDTO;
import com.easyway.communication.client.student.model.ProblemConfigDTO;
import com.easyway.communication.client.student.model.ProblemSolutionDTO;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class StudentProblemService {

	@Autowired
	private InstituteNoticeService instituteNoticeService;

	@Autowired
	private StudentInfoService studentInfoService;

	public List<GroupNoticeDTO> findGroupNoticeByGrpId(GroupConfigDTO groupConfigDTO) {
		List<GroupNoticeDTO> prepareGroupNoticeDTOs = new ArrayList<>();
		Long instituteId = studentInfoService.findStudentInfoByHttp().getStudent().getInstitute().getInstituteId();
		List<GroupNoticeDTO> groupNoticeDTOs = findAllGroupNotice().stream()
				.filter(notice -> notice.getGroup().getGroupId().equals(groupConfigDTO.getGroup().getGroupId()))
				.collect(Collectors.toList());
		for (GroupNoticeDTO groupNoticeDTO : groupNoticeDTOs) {
			if (groupNoticeDTO.getInstitute().getInstituteId().equals(instituteId)) {
				prepareGroupNoticeDTOs.add(groupNoticeDTO);
			}
		}
		prepareGroupNoticeDTOs.forEach(group -> {
			if (group.getTeacher().getTeacherId() != null && group.getTeacher() != null) {
				group.setInstitute(null);
				group.setStudent(null);
			} else if (group.getStudent().getStudentId() != null && group.getStudent() != null) {
				group.setInstitute(null);
				group.setTeacher(null);
			} else if (group.getStudent().getStudentId() != null && group.getStudent() != null) {
				group.setInstitute(null);
				group.setTeacher(null);
			}
		});
		Collections.reverse(prepareGroupNoticeDTOs);
		return prepareGroupNoticeDTOs;
	}

	public List<GroupNoticeDTO> findAllGroupNotice() {
		return ApiConsumer.consumeGroupNoticeDto(StaticValueProvider.LOGIN_INSTITUTE_URI, "find/all/group/notice");
	}

	public void joinOrUnJoinGroup(GroupDTO groupDTO, int likeAs) {
		DataExporter.sendGroupDtoToApi(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"joinOrUnJoin/gropu/by-student?likeAs=" + likeAs + "&studentId=" + provideStudentId(), groupDTO);
	}

	public List<GroupConfigDTO> findGroupConfigByStdnt() {
		return findAllGroupConfig().stream()
				.filter(group -> group.getStudent().getStudentId().equals(provideStudentId()))
				.filter(like -> like.getJoinAs() == 1).collect(Collectors.toList());
	}

	public List<GroupConfigDTO> findAllGroupConfig() {
		return StudentApiConsumer.consumeGroupConfigDTO(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"find/all/group-config");
	}

	public List<GroupDTO> findInstituteGroupByStdnt() {
		return instituteNoticeService.findAllGroup().stream()
				.filter(institute -> institute.getInstitute().getInstituteId().equals(
						studentInfoService.findStudentInfoByHttp().getStudent().getInstitute().getInstituteId()))
				.collect(Collectors.toList());
	}

	public void problmSolutionLikeByStdnt(ProblemSolutionDTO problemSolutionDTO, int likeAs) {
		StudentDataExporter.sendProblemSolutionDtoToApi(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"like/student/solution/problem?likeAs=" + likeAs, problemSolutionDTO);
	}

	public List<ProblemSolutionDTO> findAllActiveProblemSolution() {
		return StudentApiConsumer.sonsumeProblemSolutionDto(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"find/all/active/problem-solution");
	}

	public List<ProblemSolutionDTO> findPrbSolutionByStudent(Long studentId) {
		List<ProblemSolutionDTO> problemSolutionDTOs = findAllActiveProblemSolution().stream()
				.filter(student -> student.getStudent().getStudentId().equals(studentId))
				.filter(like -> like.getSolutionStatus() == 1).collect(Collectors.toList());
		Collections.reverse(problemSolutionDTOs);
		return problemSolutionDTOs;
	}

	public List<ProblemSolutionDTO> findProblemSolutionByPrId(Long problemId) {
		return StudentApiConsumer.sonsumeProblemSolutionDto(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"find/solution/by-problemId?problemId=" + problemId);
	}

	public void submitProblemSolution(ProblemSolutionDTO problemSolutionDTO, ProblemConfigDTO problemConfigDTO) {
		problemSolutionDTO.setStudent(provideStudent());
		problemSolutionDTO.setProblemConfig(provideProblemConfig(problemConfigDTO.getProblemId()));
		StudentDataExporter.sendProblemSolutionDtoToApi(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"solution/problem/by-student", problemSolutionDTO);
	}

	private ProblemConfigDTO provideProblemConfig(Long problemId) {
		ProblemConfigDTO problemConfigDTO = new ProblemConfigDTO();
		problemConfigDTO.setProblemId(problemId);
		return problemConfigDTO;
	}

	public ResponseEntity<ProblemConfigDTO> postProblemByStudent(ProblemConfigDTO problemConfigDTO, int postAs) {
		problemConfigDTO.setStudent(provideStudent());
		return StudentDataExporter.sendProblemConfigDtoToApi(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"post/problem/by-student", problemConfigDTO);
	}

	private StudentDTO provideStudent() {
		StudentDTO studentDTO = new StudentDTO();
		studentDTO.setStudentId(provideStudentId());
		return studentDTO;
	}

	public List<ProblemConfigDTO> findAllProblems() {
		List<ProblemConfigDTO> problemConfigDTOs = new ArrayList<>();
		problemConfigDTOs = StudentApiConsumer.consumeProblemConfigDTOs(StaticValueProvider.LOGIN_STUDENT_PROBLEM_URI,
				"find/all-problem");
		List<ProblemSolutionDTO> problemSolutionDTOs = findAllActiveProblemSolution();
		problemConfigDTOs = problemConfigDTOs.stream()
				.map(problem -> provideTotalSolution(problem, problemSolutionDTOs)).collect(Collectors.toList());
		Collections.reverse(problemConfigDTOs);
		return problemConfigDTOs;
	}

	private ProblemConfigDTO provideTotalSolution(ProblemConfigDTO problem,
			List<ProblemSolutionDTO> problemSolutionDTOs) {
		ProblemConfigDTO problemConfigDTO = new ProblemConfigDTO();
		BeanUtils.copyProperties(problem, problemConfigDTO);
		problemConfigDTO.setTotalSolution(problemSolutionDTOs.stream()
				.filter(solution -> solution.getProblemConfig().getProblemId().equals(problem.getProblemId())).count());
		return problemConfigDTO;
	}

	public List<ProblemConfigDTO> findProblemByStudent() {
		List<ProblemConfigDTO> problemConfigDTOs = findAllProblems().stream()
				.filter(student -> student.getStudent().getStudentId().equals(provideStudentId()))
				.collect(Collectors.toList());
		List<ProblemSolutionDTO> problemSolutionDTOs = findAllActiveProblemSolution();
		problemConfigDTOs = problemConfigDTOs.stream()
				.map(solution -> provideProblemSolution(solution, problemSolutionDTOs)).collect(Collectors.toList());
		return problemConfigDTOs;
	}

	private ProblemConfigDTO provideProblemSolution(ProblemConfigDTO problem,
			List<ProblemSolutionDTO> problemSolutionDTOs) {
		ProblemConfigDTO problemConfigDTO = new ProblemConfigDTO();
		BeanUtils.copyProperties(problem, problemConfigDTO);
		problemConfigDTO.setTotalSolution(problemSolutionDTOs.stream()
				.filter(solution -> solution.getProblemConfig().getProblemId().equals(problem.getProblemId())).count());
		return problemConfigDTO;
	}

	public Long provideStudentId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getStudent().getStudentId();
	}
}
