package com.easyway.communication.client.teacher.model;

import java.util.Date;

import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;

public class TeacherDTO {

	private Long teacherId;

	private String teacherName;

	private GeneralInfoDTO designation;

	private GeneralInfoDTO department;

	private String registrationId;

	private String qualification;

	private String contactNumber;

	private String email;

	private String photoName;

	private String address;

	private String gender;

	private String birthDate;

	private Date createDate;

	private String userName;

	private String password;

	private InstituteDTO institute;
	
	private String photoWithBase64;

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public GeneralInfoDTO getDesignation() {
		if (designation == null) {
			designation = new GeneralInfoDTO();
		}
		return designation;
	}

	public void setDesignation(GeneralInfoDTO designation) {
		this.designation = designation;
	}

	public GeneralInfoDTO getDepartment() {
		if (department == null) {
			department = new GeneralInfoDTO();
		}
		return department;
	}

	public void setDepartment(GeneralInfoDTO department) {
		this.department = department;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public InstituteDTO getInstitute() {
		return institute;
	}

	public void setInstitute(InstituteDTO institute) {
		this.institute = institute;
	}

	public String getPhotoWithBase64() {
		return photoWithBase64;
	}

	public void setPhotoWithBase64(String photoWithBase64) {
		this.photoWithBase64 = photoWithBase64;
	}

}
