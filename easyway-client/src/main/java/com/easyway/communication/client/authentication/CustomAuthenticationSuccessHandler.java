package com.easyway.communication.client.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.users.model.UserDetailsDTO;
import com.easyway.communication.client.users.model.UsersDTO;

@Controller
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Authentication authentication) throws IOException, ServletException {

		HttpSession httpSession = httpServletRequest.getSession();
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		userDetailsDTO = findUserDetailsOfLoginUser(authentication.getName());
		if (userDetailsDTO != null) {
			httpSession.setAttribute("userDetails", userDetailsDTO);
		}
		httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/template/template.xhtml");
	}

	public UsersDTO findLoginSuccessuser(String username) {
		UsersDTO user = new UsersDTO();
		try {
			user = ApiConsumer.consumeLoginSuccessUser(StaticValueProvider.LOGIN_USER_URI,
					"/success/user?username=" + username);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return user;

	}

	public UserDetailsDTO findUserDetailsOfLoginUser(String username) {
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		try {
			userDetailsDTO = ApiConsumer.findUserDetailsOfLoginUser(StaticValueProvider.LOGIN_USER_URI,
					"/details?username=" + username);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return userDetailsDTO;

	}
}
