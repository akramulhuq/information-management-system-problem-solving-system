package com.easyway.communication.client.users.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.service.AdminService;
import com.easyway.communication.client.common.util.AdminApiConsumer;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.student.service.StudentInfoService;
import com.easyway.communication.client.users.model.SignUpDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;
import com.easyway.communication.client.users.service.UsersService;

@Controller
@ManagedBean
@Scope("session")
public class UsersController {

	@Autowired
	private UsersService usersService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private InstituteInfoService instituteInfoService;

	@Autowired
	private StudentInfoService studentInfoService;

	private SignUpDTO signpUpDTO;

	private UserDetailsDTO userDetailsDTO;

	private List<SignUpDTO> signUpDTOs;

	private List<DistrictDTO> districtDTOs;

	private List<DivisionDTO> divisionDTOs;

	private List<InstituteDTO> instituteDTOs;

	private Long divisionId;

	private boolean instituteSignUpFrmVisibility;

	private boolean studentSignUpFrmVisibility = true;

	private boolean defaultSignUpFrmVisibility;

	private int signUpFrmVisibler;

	@PostConstruct
	public void inti() {
		divisionDTOs = adminService.findDivisionsList();
		instituteDTOs = instituteInfoService.findAllInstituteInfo();
		findStudentInfoByHttp();
	}

	public void findStudentInfoByHttp() {
		try {
			userDetailsDTO = new UserDetailsDTO();
			userDetailsDTO = studentInfoService.findStudentInfoByHttp();
			userDetailsDTO
					.setLogoWithBase64(ApplicationUtils.provideBase64Image(userDetailsDTO.getStudent().getPhotoName()));
		} catch (Exception e) {

		}
	}

	public void approveSingleUsers() {
		FacesContext mass = FacesContext.getCurrentInstance();
		try {
			usersService.approveSingleUsers(signpUpDTO);
			allPendingUsers();
			mass.addMessage(null, new FacesMessage("Institute approve Successfully!"));
		} catch (Exception e) {
			mass.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Code not institute approve Successfully.!", ""));
		}
	}

	public String allPendingUsers() {
		try {
			signUpDTOs = usersService.allPendingUsers();
			signUpDTOs.forEach(signUp -> provideSignUp(signUp));
		} catch (Exception e) {
			System.out.println(e);

		}

		return "/admin/role-manager.xhtml?faces-redirect=true";
	}

	private void provideSignUp(SignUpDTO signUp) {
		signUp.setPrepareDate(ApplicationUtils.convertDateToLocalDateTime(signUp.getSignedUpDate()));
	}

	public String saveSignedUpUsers(int signUpAs) throws Exception {
		FacesContext mass = FacesContext.getCurrentInstance();
		try {
			usersService.adminRegistration(signpUpDTO, signUpAs);
			signpUpDTO = new SignUpDTO();
			mass.addMessage(null, new FacesMessage("institute registration Successfully!"));
		} catch (Exception e) {
			mass.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Activation code was wrong.!", ""));
		}
		return "/login.xhtml";
	}

	public String saveSignedUpAdmins(int signUpAs) throws Exception {
		FacesContext mass = FacesContext.getCurrentInstance();
		if (ApplicationUtils.ADMIN_ACTIVETION_CODE.equals(signpUpDTO.getActivationCode())) {
			usersService.adminRegistration(signpUpDTO, signUpAs);
			signpUpDTO = new SignUpDTO();
			mass.addMessage(null, new FacesMessage("Admin registration Successfully!"));
		} else {
			mass.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Activation code was wrong.!", ""));
		}
		return "/login.xhtml";
	}

	public void provideDynamicSignUpFrmVisibility() {
		switch (signUpFrmVisibler) {
		case 1:
			defaultSignUpFrmVisibility = false;
			instituteSignUpFrmVisibility = true;
			studentSignUpFrmVisibility = false;
			break;

		case 2:
			defaultSignUpFrmVisibility = false;
			instituteSignUpFrmVisibility = false;
			studentSignUpFrmVisibility = true;
			break;

		default:
			studentSignUpFrmVisibility = true;
			instituteSignUpFrmVisibility = false;
			defaultSignUpFrmVisibility = false;
			break;
		}
	}

	public void findDistrictByDivision() {
		districtDTOs = new ArrayList<>();
		districtDTOs = adminService.findDistrictsList().stream()
				.filter(district -> district.getDivision().getDivisionId().equals(divisionId))
				.collect(Collectors.toList());
	}

	public boolean isInstituteSignUpFrmVisibility() {
		return instituteSignUpFrmVisibility;
	}

	public void setInstituteSignUpFrmVisibility(boolean instituteSignUpFrmVisibility) {
		this.instituteSignUpFrmVisibility = instituteSignUpFrmVisibility;
	}

	public boolean isStudentSignUpFrmVisibility() {
		return studentSignUpFrmVisibility;
	}

	public void setStudentSignUpFrmVisibility(boolean studentSignUpFrmVisibility) {
		this.studentSignUpFrmVisibility = studentSignUpFrmVisibility;
	}

	public boolean isDefaultSignUpFrmVisibility() {
		return defaultSignUpFrmVisibility;
	}

	public void setDefaultSignUpFrmVisibility(boolean defaultSignUpFrmVisibility) {
		this.defaultSignUpFrmVisibility = defaultSignUpFrmVisibility;
	}

	public int getSignUpFrmVisibler() {
		return signUpFrmVisibler;
	}

	public void setSignUpFrmVisibler(int signUpFrmVisibler) {
		this.signUpFrmVisibler = signUpFrmVisibler;
	}

	public SignUpDTO getSignpUpDTO() {
		if (signpUpDTO == null) {
			signpUpDTO = new SignUpDTO();
		}
		return signpUpDTO;
	}

	public void setSignpUpDTO(SignUpDTO signpUpDTO) {
		this.signpUpDTO = signpUpDTO;
	}

	public List<DistrictDTO> getDistrictDTOs() {
		return districtDTOs;
	}

	public void setDistrictDTOs(List<DistrictDTO> districtDTOs) {
		this.districtDTOs = districtDTOs;
	}

	public List<DivisionDTO> getDivisionDTOs() {
		return divisionDTOs;
	}

	public void setDivisionDTOs(List<DivisionDTO> divisionDTOs) {
		this.divisionDTOs = divisionDTOs;
	}

	public Long getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}

	public List<SignUpDTO> getSignUpDTOs() {
		return signUpDTOs;
	}

	public void setSignUpDTOs(List<SignUpDTO> signUpDTOs) {
		this.signUpDTOs = signUpDTOs;
	}

	public List<InstituteDTO> getInstituteDTOs() {
		return instituteDTOs;
	}

	public void setInstituteDTOs(List<InstituteDTO> instituteDTOs) {
		this.instituteDTOs = instituteDTOs;
	}

	public UserDetailsDTO getUserDetailsDTO() {
		if (userDetailsDTO == null)
			userDetailsDTO = new UserDetailsDTO();
		return userDetailsDTO;
	}

	public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
		this.userDetailsDTO = userDetailsDTO;
	}

}
