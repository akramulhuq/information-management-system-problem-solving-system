package com.easyway.communication.client.institute.controller;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.common.enums.ImageType;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.institute.service.InstituteNoticeService;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.teacher.service.TeacherInfoService;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Controller
@ManagedBean
@Scope("session")
public class InstituteNoticeController {

	@Autowired
	private InstituteNoticeService instituteNoticeService;

	@Autowired
	private TeacherInfoService teacherInfoService;

	private InstituteDTO instituteDTO;

	private UserDetailsDTO userDetailsDTO;

	private InstituteNoticeDTO instituteNoticeDTO;

	private List<InstituteNoticeDTO> instituteNoticeDTOs;

	private List<InstituteNoticeDTO> noticeDTOsByInstituteId;

	private List<GroupDTO> instituteGroupsDTO;

	private List<GroupNoticeDTO> groupNoticeDTOs;

	private GroupNoticeDTO groupNoticeDTO;

	private GroupDTO groupDTO;

	private UserDetailsDTO viewInstituteInfo;

	private UploadedFile noticePhoto;

	private UploadedFile groupNoticePhoto;

	public String findNoticeByInstituteId() {
		noticeDTOsByInstituteId = instituteNoticeService.findNoticeByInstituteId();
		for (InstituteNoticeDTO instituteNoticeDTO : noticeDTOsByInstituteId) {
			instituteNoticeDTO
					.setNoticePhotoBase64(ApplicationUtils.provideBase64Image(instituteNoticeDTO.getNoticeWithPhoto()));
			instituteNoticeDTO.setInstitutePhotoBase64(
					ApplicationUtils.provideBase64Image(instituteNoticeDTO.getInstitute().getLogoPhoto()));
		}
		return "/institute/institute-notices.xhtml?faces-redirect=true";
	}

	public void postGroupNotice(int postAs) {
		try {
			if (postAs == 1) {
				groupNoticeDTO.setInstitute(instituteNoticeService.provideInstitute());
				instituteNoticeService.postGroupNotice(groupNoticeDTO, groupDTO);
				findGroupNoticeByInstitute();
				groupNoticeDTO = new GroupNoticeDTO();
			} else if (postAs == 2) {
				InstituteNoticeDTO instituteNoticeDTO = provideInstituteNoticeDto();
				groupNoticeDTO.setTeacher(instituteNoticeDTO.getTeacher());
				groupNoticeDTO.setInstitute(instituteNoticeDTO.getInstitute());
				instituteNoticeService.postGroupNotice(groupNoticeDTO, groupDTO);
				findGroupNoticeByInstitute();
				groupNoticeDTO = new GroupNoticeDTO();
			} else if (postAs == 3) {
				groupNoticeDTO.setInstitute(instituteNoticeService.provideInstitute());
				ResponseEntity<GroupNoticeDTO> responseEntity = instituteNoticeService
						.saveGrpNoticeWithOutPhto(groupNoticeDTO, groupDTO);
				groupNoticeDTO.setNoticeWithPhoto(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
						groupNoticePhoto, ImageType.GROUP_NOTICE_PHOTO.getCode(),
						instituteNoticeService.provideInstituteId(), responseEntity.getBody().getNoticeId()));
				instituteNoticeService.postGroupNotice(groupNoticeDTO, groupDTO);
				findGroupNoticeByInstitute();
				groupNoticeDTO = new GroupNoticeDTO();
			} else if (postAs == 4) {
				InstituteNoticeDTO instituteNoticeDTO = provideInstituteNoticeDto();
				groupNoticeDTO.setTeacher(instituteNoticeDTO.getTeacher());
				groupNoticeDTO.setInstitute(instituteNoticeDTO.getInstitute());

				ResponseEntity<GroupNoticeDTO> responseEntity = instituteNoticeService
						.saveGrpNoticeWithOutPhto(groupNoticeDTO, groupDTO);
				groupNoticeDTO.setNoticeWithPhoto(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
						groupNoticePhoto, ImageType.GROUP_NOTICE_PHOTO.getCode(),
						instituteNoticeService.provideInstituteId(), responseEntity.getBody().getNoticeId()));
				instituteNoticeService.postGroupNotice(groupNoticeDTO, groupDTO);
				findGroupNoticeByInstitute();
				groupNoticeDTO = new GroupNoticeDTO();
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findGroupNoticeByInstitute() {
		groupNoticeDTOs = instituteNoticeService.findGroupNoticeByInstitute(groupDTO);
		for (GroupNoticeDTO groupNoticeDTO : groupNoticeDTOs) {
			groupNoticeDTO
					.setNoticePhotoBase64(ApplicationUtils.provideBase64Image(groupNoticeDTO.getNoticeWithPhoto()));
			groupNoticeDTO.setTeacherPhotoBase64(
					ApplicationUtils.provideBase64Image(groupNoticeDTO.getTeacher().getPhotoName()));
			groupNoticeDTO.setInstitutePhotoBase64(
					ApplicationUtils.provideBase64Image(groupNoticeDTO.getInstitute().getLogoPhoto()));
		}
		return "/institute/group-notice.xhtml?faces-redirect=true";
	}

	public void createGroupByInstitute() {
		try {
			instituteNoticeService.createGroupByInstitute(groupDTO);
			groupDTO = new GroupDTO();
			Long instituteId = instituteNoticeService.provideInstituteId();
			if (instituteId != null) {
				findGroupByInstitute(1);
			} else {
				findGroupByInstitute(2);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findGroupByInstitute(int gourpAs) {
		if (gourpAs == 1) {
			Long instituteId = instituteNoticeService.provideInstituteId();
			instituteGroupsDTO = instituteNoticeService.findGroupByInstituteId(instituteId);
		} else if (gourpAs == 2) {
			UserDetailsDTO userDetailsDTO = teacherInfoService.findTeacherInfoByHttp();
			instituteGroupsDTO = instituteNoticeService
					.findGroupByInstituteId(userDetailsDTO.getTeacher().getInstitute().getInstituteId());
		}

		return "/institute/group.xhtml?faces-redirect=true";
	}

	/* this notice value show from this method */

	public String findInstituteNoticeById(int personId) {
		try {
			if (personId == 1) {
				instituteNoticeDTOs = instituteNoticeService
						.findInstituteNoticeById(instituteNoticeService.provideInstituteId());
			}
			if (personId == 2) {
				instituteNoticeDTOs = instituteNoticeService
						.findInstituteNoticeById(userDetailsDTO.getStudent().getInstitute().getInstituteId());
			}
			if (personId == 3) {
				instituteNoticeDTOs = instituteNoticeService
						.findInstituteNoticeById(userDetailsDTO.getTeacher().getInstitute().getInstituteId());
			}
			for (InstituteNoticeDTO instituteNoticeDTO : instituteNoticeDTOs) {
				instituteNoticeDTO.setNoticePhotoBase64(
						ApplicationUtils.provideBase64Image(instituteNoticeDTO.getNoticeWithPhoto()));
				instituteNoticeDTO.setTeacherPhotoBase64(
						ApplicationUtils.provideBase64Image(instituteNoticeDTO.getTeacher().getPhotoName()));
				instituteNoticeDTO.setInstitutePhotoBase64(
						ApplicationUtils.provideBase64Image(instituteNoticeDTO.getInstitute().getLogoPhoto()));
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return "/institute/notice.xhtml?faces-redirect=true";
	}

	public void postNoticeByInstitute(int postAs) {
		try {

			if (postAs == 1) {
				ResponseEntity<InstituteNoticeDTO> responseEntity = instituteNoticeService
						.postNoticeByInstitute(instituteNoticeDTO);
				if (!noticePhoto.getFileName().isEmpty() && noticePhoto != null) {
					instituteNoticeDTO.setNoticeWithPhoto(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
							noticePhoto, ImageType.NOTICE_PHOTO.getCode(), instituteNoticeService.provideInstituteId(),
							responseEntity.getBody().getNoticeId()));
				}
				instituteNoticeService.postNoticeByInstitute(instituteNoticeDTO);
				findInstituteNoticeById(1);
			}
			if (postAs == 2) {
				instituteNoticeService.postNoticeByInstitute(instituteNoticeDTO);
				findInstituteNoticeById(1);
			}
			if (postAs == 3) {
				InstituteNoticeDTO instituteNotice = provideInstituteNoticeDto();
				instituteNoticeDTO.setInstitute(instituteNotice.getInstitute());
				instituteNoticeDTO.setTeacher(instituteNotice.getTeacher());
				instituteNoticeService.postNoticeByInstitute(instituteNoticeDTO);
				findInstituteNoticeById(3);
			}
			if (postAs == 4) {
				ResponseEntity<InstituteNoticeDTO> responseEntity = instituteNoticeService
						.postNoticeByInstitute(instituteNoticeDTO);
				if (!noticePhoto.getFileName().isEmpty() && noticePhoto != null) {
					instituteNoticeDTO.setNoticeWithPhoto(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
							noticePhoto, ImageType.NOTICE_PHOTO.getCode(), teacherInfoService.provideTeacherId(),
							responseEntity.getBody().getNoticeId()));
				}
				InstituteNoticeDTO instituteNotice = provideInstituteNoticeDto();
				InstituteDTO instituteDTO = new InstituteDTO();
				UserDetailsDTO userDetailsDTO = teacherInfoService.findTeacherInfoByHttp();
				instituteDTO.setInstituteId(userDetailsDTO.getTeacher().getInstitute().getInstituteId());

				instituteNoticeDTO.setNoticeId(responseEntity.getBody().getNoticeId());
				instituteNoticeDTO.setInstitute(instituteDTO);
				instituteNoticeDTO.setTeacher(instituteNotice.getTeacher());
				instituteNoticeService.postNoticeByInstitute(instituteNoticeDTO);
				findInstituteNoticeById(3);
			}

			instituteNoticeDTO = new InstituteNoticeDTO();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private InstituteNoticeDTO provideInstituteNoticeDto() {
		InstituteNoticeDTO instituteNotice = new InstituteNoticeDTO();
		TeacherDTO teacherDTO = new TeacherDTO();
		teacherDTO.setTeacherId(teacherInfoService.provideTeacherId());

		InstituteDTO instituteDTO = new InstituteDTO();
		UserDetailsDTO userDetailsDTO = teacherInfoService.findTeacherInfoByHttp();
		instituteDTO.setInstituteId(userDetailsDTO.getTeacher().getInstitute().getInstituteId());

		instituteNotice.setInstitute(instituteDTO);
		instituteNotice.setTeacher(teacherDTO);
		return instituteNotice;
	}

	public String findInstituteInfo() {
		try {
			viewInstituteInfo = instituteNoticeService.findInstituteInfoByHttp();
			viewInstituteInfo.setLogoWithBase64(
					ApplicationUtils.provideBase64Image(viewInstituteInfo.getInstitute().getLogoPhoto()));
		} catch (Exception e) {
			System.out.println(e);
		}
		return "/institute/institute-information.xhtml?faces-redirect=true";
	}

	public InstituteDTO getInstituteDTO() {
		if (instituteDTO == null) {
			instituteDTO = new InstituteDTO();
		}
		return instituteDTO;
	}

	public void setInstituteDTO(InstituteDTO instituteDTO) {
		this.instituteDTO = instituteDTO;
	}

	public UploadedFile getNoticePhoto() {
		return noticePhoto;
	}

	public void setNoticePhoto(UploadedFile noticePhoto) {
		this.noticePhoto = noticePhoto;
	}

	public UserDetailsDTO getViewInstituteInfo() {
		if (viewInstituteInfo == null)
			viewInstituteInfo = new UserDetailsDTO();
		return viewInstituteInfo;
	}

	public void setViewInstituteInfo(UserDetailsDTO viewInstituteInfo) {
		this.viewInstituteInfo = viewInstituteInfo;
	}

	public InstituteNoticeDTO getInstituteNoticeDTO() {
		if (instituteNoticeDTO == null) {
			instituteNoticeDTO = new InstituteNoticeDTO();
		}
		return instituteNoticeDTO;
	}

	public void setInstituteNoticeDTO(InstituteNoticeDTO instituteNoticeDTO) {
		this.instituteNoticeDTO = instituteNoticeDTO;
	}

	public List<InstituteNoticeDTO> getInstituteNoticeDTOs() {
		return instituteNoticeDTOs;
	}

	public void setInstituteNoticeDTOs(List<InstituteNoticeDTO> instituteNoticeDTOs) {
		this.instituteNoticeDTOs = instituteNoticeDTOs;
	}

	public UserDetailsDTO getUserDetailsDTO() {
		if (userDetailsDTO == null)
			userDetailsDTO = new UserDetailsDTO();
		return userDetailsDTO;
	}

	public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
		this.userDetailsDTO = userDetailsDTO;
	}

	public List<GroupDTO> getInstituteGroupsDTO() {
		return instituteGroupsDTO;
	}

	public void setInstituteGroupsDTO(List<GroupDTO> instituteGroupsDTO) {
		this.instituteGroupsDTO = instituteGroupsDTO;
	}

	public GroupDTO getGroupDTO() {
		if (groupDTO == null)
			groupDTO = new GroupDTO();
		return groupDTO;
	}

	public void setGroupDTO(GroupDTO groupDTO) {
		this.groupDTO = groupDTO;
	}

	public List<GroupNoticeDTO> getGroupNoticeDTOs() {
		return groupNoticeDTOs;
	}

	public void setGroupNoticeDTOs(List<GroupNoticeDTO> groupNoticeDTOs) {
		this.groupNoticeDTOs = groupNoticeDTOs;
	}

	public GroupNoticeDTO getGroupNoticeDTO() {
		if (groupNoticeDTO == null)
			groupNoticeDTO = new GroupNoticeDTO();
		return groupNoticeDTO;
	}

	public void setGroupNoticeDTO(GroupNoticeDTO groupNoticeDTO) {
		this.groupNoticeDTO = groupNoticeDTO;
	}

	public List<InstituteNoticeDTO> getNoticeDTOsByInstituteId() {
		return noticeDTOsByInstituteId;
	}

	public void setNoticeDTOsByInstituteId(List<InstituteNoticeDTO> noticeDTOsByInstituteId) {
		this.noticeDTOsByInstituteId = noticeDTOsByInstituteId;
	}

	public void setGroupNoticePhoto(UploadedFile groupNoticePhoto) {
		this.groupNoticePhoto = groupNoticePhoto;
	}

	public UploadedFile getGroupNoticePhoto() {
		return groupNoticePhoto;
	}

}
