package com.easyway.communication.client.admin.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.admin.service.AdminService;

@Controller
@ManagedBean
@Scope("session")
public class AdminController {

	@Autowired
	private AdminService adminService;

	private List<DivisionDTO> divisionDTOs;

	private List<DistrictDTO> districtDTOs;

	private DivisionDTO divisionDTO;

	private DistrictDTO districtDTO;

	private Long divisionId;

	private GeneralInfoDTO generalInfoDTO;

	private List<GeneralInfoDTO> generalInfoDTOs;

	@PostConstruct
	public void init() {
		showDivisonAddForm();
	}

	public String showDivisonAddForm() {
		divisionDTOs = adminService.findDivisionsList();
		districtDTOs = adminService.findDistrictsList();
		districtDTO = new DistrictDTO();
		divisionDTO = new DivisionDTO();
		return "/admin/add-address.xhtml?faces-redirect=true";
	}

	public void addOrUpdateDivision() {
		adminService.saveOrUpdateDivision(divisionDTO);
		showDivisonAddForm();
		divisionDTO = new DivisionDTO();
	}

	public String findAllDivisions() {
		divisionDTOs = adminService.findDivisionsList();
		return "/admin/view-division.xhtml?faces-redirect=true";
	}

	public void addOrUpdateDistrict() {
		adminService.saveOrUpdateDistrict(districtDTO);
		districtDTO = new DistrictDTO();
	}

	public String findAllDistricts() {
		divisionDTOs = adminService.findDivisionsList();
		districtDTOs = adminService.findDistrictsList();
		return "/admin/view-address.xhtml?faces-redirect=true";
	}

	public String saveGeneralInfo() {

		adminService.saveGeneralInfo(generalInfoDTO);
		if (generalInfoDTO.getId() == null) {
			generalInfoDTO = new GeneralInfoDTO();
			return "/admin/general-info.xhtml?faces-redirect=true";
		} else {
			generalInfoDTO = new GeneralInfoDTO();
			return "/admin/view-general-info.xhtml?faces-redirect=true";
		}

	}

	public String findAllGeneralInfo() {
		generalInfoDTOs = adminService.findGeneralInfoList();
		return "/admin/view-general-info.xhtml?faces-redirect=true";
	}

	public List<DivisionDTO> getDivisionDTOs() {
		return divisionDTOs;
	}

	public void setDivisionDTOs(List<DivisionDTO> divisionDTOs) {
		this.divisionDTOs = divisionDTOs;
	}

	public List<DistrictDTO> getDistrictDTOs() {
		return districtDTOs;
	}

	public void setDistrictDTOs(List<DistrictDTO> districtDTOs) {
		this.districtDTOs = districtDTOs;
	}

	public DivisionDTO getDivisionDTO() {
		if (divisionDTO == null)
			divisionDTO = new DivisionDTO();
		return divisionDTO;
	}

	public void setDivisionDTO(DivisionDTO divisionDTO) {
		this.divisionDTO = divisionDTO;
	}

	public DistrictDTO getDistrictDTO() {
		if (districtDTO == null)
			districtDTO = new DistrictDTO();
		return districtDTO;
	}

	public void setDistrictDTO(DistrictDTO districtDTO) {
		this.districtDTO = districtDTO;
	}

	public Long getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}

	public GeneralInfoDTO getGeneralInfoDTO() {
		if (generalInfoDTO == null) {
			generalInfoDTO = new GeneralInfoDTO();
		}
		return generalInfoDTO;
	}

	public void setGeneralInfoDTO(GeneralInfoDTO generalInfoDTO) {
		this.generalInfoDTO = generalInfoDTO;
	}

	public List<GeneralInfoDTO> getGeneralInfoDTOs() {
		findAllGeneralInfo();
		return generalInfoDTOs;
	}

	public void setGeneralInfoDTOs(List<GeneralInfoDTO> generalInfoDTOs) {
		this.generalInfoDTOs = generalInfoDTOs;
	}

}
