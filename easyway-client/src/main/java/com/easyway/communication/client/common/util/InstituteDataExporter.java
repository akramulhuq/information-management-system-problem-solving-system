package com.easyway.communication.client.common.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.users.model.SignUpDTO;

public class InstituteDataExporter {

	public static void sendSignUpDtoToApi(String uri, String uriExtension, SignUpDTO signpUpDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<SignUpDTO> request = new HttpEntity<>(signpUpDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		template.exchange(url, HttpMethod.POST, request, SignUpDTO.class);
	}

	public static ResponseEntity<InstituteNoticeDTO> sendInstituteNoticeDtoToApi(String uri, String uriExtension,
			InstituteNoticeDTO instituteNoticeDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<InstituteNoticeDTO> request = new HttpEntity<>(instituteNoticeDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		return template.exchange(url, HttpMethod.POST, request, InstituteNoticeDTO.class);

	}

	public static ResponseEntity<TeacherDTO> sendInstituteTeacherDtoToApi(String uri, String uriExtension,
			TeacherDTO teacherDTO) {
		RestTemplate template = new RestTemplate();
		HttpEntity<TeacherDTO> request = new HttpEntity<TeacherDTO>(teacherDTO);
		String url = StaticValueProvider.BASE_URL + uri + uriExtension;
		return template.exchange(url, HttpMethod.POST, request, TeacherDTO.class);

	}

}
