package com.easyway.communication.client.teacher.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.stereotype.Service;

import com.easyway.communication.client.authentication.ApplicationUtilsMB;
import com.easyway.communication.client.common.util.ApiConsumer;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.DataExporter;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.common.util.StudentDataExporter;
import com.easyway.communication.client.teacher.model.FolderDTO;
import com.easyway.communication.client.teacher.model.TopicDTO;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Service
public class TeacherInfoService {

	public void addTopicByTeacher(TopicDTO topicDTO, FolderDTO folderDTO) {
		topicDTO.setFolder(provideFolder(folderDTO));
		DataExporter.sendTopicDtoToApi(StaticValueProvider.LOGIN_TEACHER_URI, "add/topic/by-teacher", topicDTO);
	}

	private FolderDTO provideFolder(FolderDTO folderDTO) {
		FolderDTO folder = new FolderDTO();
		folder.setForlderId(folderDTO.getForlderId());
		return folder;
	}

	public List<TopicDTO> findTopicByTeacher(Long folderId) {
		return ApiConsumer
				.consumeTopicDtoToApi(StaticValueProvider.LOGIN_TEACHER_URI,
						"find/topic/by-folder?folderId=" + folderId)
				.stream().filter(teacher -> teacher.getFolder().getTeacher().getTeacherId().equals(provideTeacherId()))
				.collect(Collectors.toList());
	}

	public void createFolderByTeacher(FolderDTO folderDTO) {
		DataExporter.sendFolderDtoToApi(StaticValueProvider.LOGIN_TEACHER_URI,
				"create/folder/by-teacher?teacherId=" + provideTeacherId(), folderDTO);
	}

	public List<FolderDTO> findAllFolderByTeacherId(Long teacherId) {
		return ApiConsumer.consumeFolderDTO(StaticValueProvider.LOGIN_TEACHER_URI,
				"find/folder/by-teacher?techerId=" + provideTeacherId());
	}

	public UserDetailsDTO findTeacherInfoByHttp() {
		UserDetailsDTO userDetailsDTO = ApplicationUtilsMB.provideCurrentUserDetils();
		if (userDetailsDTO == null) {
			userDetailsDTO = new UserDetailsDTO();
		}
		userDetailsDTO.setPrepareName(provideInstitute(userDetailsDTO.getTeacher().getInstitute().getInstituteName()));
		return userDetailsDTO;
	}

	private String provideInstitute(String provideName) {
		String[] arryOfName = provideName.split(" ");
		String singleName = Arrays.asList(arryOfName).stream().findFirst().orElse(null);
		return singleName;
	}

	public Long provideTeacherId() {
		return ApplicationUtilsMB.provideCurrentUserDetils().getTeacher().getTeacherId();
	}

}
