package com.easyway.communication.client.student.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.sound.midi.Soundbank;

import org.eclipse.jdt.internal.compiler.lookup.ProblemMethodBinding;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.common.enums.GeneralInfoType;
import com.easyway.communication.client.common.enums.ImageType;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.BaseResponse;
import com.easyway.communication.client.common.util.CommonService;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.student.model.ProblemConfigDTO;
import com.easyway.communication.client.student.model.ProblemSolutionDTO;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.student.service.StudentProblemService;

@Controller
@ManagedBean
@Scope("session")
public class StudentProblemController {

	@Autowired
	private StudentProblemService studentProblemService;

	@Autowired
	private CommonService commonService;

	private List<ProblemConfigDTO> allProblemDTOs;

	private List<GeneralInfoDTO> departmentDTOs;

	private List<ProblemSolutionDTO> problemSolutionDTOs;

	private List<ProblemSolutionDTO> studentSolutionDTOs;

	private List<ProblemSolutionDTO> studentSolvedProblemDTOs;

	private List<ProblemConfigDTO> studentProblemConfigDTOs;

	private ProblemConfigDTO problemConfigDTO;

	private ProblemSolutionDTO problemSolutionDTO;

	private UploadedFile problemPhoto;

	private UploadedFile solutionPhoto1;

	private UploadedFile solutionPhoto2;

	private boolean hidden = false;

	private Long shownProblemid;

	@PostConstruct
	public void init() {
		departmentDTOs = commonService.provideGeneralInfoByid(GeneralInfoType.DEPARTMENT.getCode());
	}

	public void problmSolutionLikeByStdnt(int likeAs) {
		try {
			studentProblemService.problmSolutionLikeByStdnt(problemSolutionDTO, likeAs);
			problemSolutionDTO = null;
			findProblemSolutionByPrblm();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findPrbSolutionByStudent() {
		try {
			studentProblemConfigDTOs = studentProblemService.findProblemByStudent();
			for (ProblemConfigDTO problemConfigDTO : studentProblemConfigDTOs) {
				problemConfigDTO
						.setProblemPhotoBase64(ApplicationUtils.provideBase64Image(problemConfigDTO.getPicturePost()));
				problemConfigDTO.setStudentPhotoBase64(
						ApplicationUtils.provideBase64Image(problemConfigDTO.getStudent().getPhotoName()));
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return "/student/problem-solution.xhtml?faces-redirect=true";
	}

	public void findProblemSolutionByPrblm() {
		try {
			problemSolutionDTOs = studentProblemService.findProblemSolutionByPrId(problemConfigDTO.getProblemId());
			for (ProblemSolutionDTO problemSolutionDTO : problemSolutionDTOs) {
				problemSolutionDTO.setSolutionPhotoWithBase1(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto1()));
				problemSolutionDTO.setSolutionPhotoWithBase2(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto2()));
				problemSolutionDTO.setSpersonPhotoWithBase2(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getStudent().getPhotoName()));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findProblemSolutionByPrId() {
		try {
			problemSolutionDTOs = studentProblemService.findProblemSolutionByPrId(problemConfigDTO.getProblemId());
			for (ProblemSolutionDTO problemSolutionDTO : problemSolutionDTOs) {
				problemSolutionDTO.setSolutionPhotoWithBase1(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto1()));
				problemSolutionDTO.setSolutionPhotoWithBase2(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getSolutionByPhoto2()));
				problemSolutionDTO.setSpersonPhotoWithBase2(
						ApplicationUtils.provideBase64Image(problemSolutionDTO.getStudent().getPhotoName()));
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return "/student/problem-solution-by-id.xhtml?faces-redirect=true";
	}

	public void submitProblemSolution() {
		FacesContext mass = FacesContext.getCurrentInstance();
		try {
			if (!solutionPhoto1.getFileName().isEmpty()) {
				problemSolutionDTO.setSolutionByPhoto1(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
						solutionPhoto1, ImageType.SOLUTION_PHOTO_1.getCode(), studentProblemService.provideStudentId(),
						problemConfigDTO.getProblemId()));
			}
			if (!solutionPhoto2.getFileName().isEmpty()) {
				problemSolutionDTO.setSolutionByPhoto2(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
						solutionPhoto2, ImageType.SOLUTION_PHOTO_2.getCode(), studentProblemService.provideStudentId(),
						problemConfigDTO.getProblemId()));
			}

			studentProblemService.submitProblemSolution(problemSolutionDTO, problemConfigDTO);

			problemSolutionDTO = new ProblemSolutionDTO();
			problemConfigDTO = new ProblemConfigDTO();
			mass.addMessage(null, new FacesMessage("Problem solution submit successfully!"));
		} catch (Exception e) {
			System.out.println(e);
			mass.addMessage(null, new FacesMessage("Code not problem solution submit successfully..!"));
		}
	}

	public void postProblemByStudent(int postAs) {
		FacesContext mass = FacesContext.getCurrentInstance();
		try {
			if (postAs == 1) {
				ResponseEntity<ProblemConfigDTO> response = studentProblemService.postProblemByStudent(problemConfigDTO,
						postAs);
				System.out.println(response.getBody().getTopicName());
				findAllProblems();
				mass.addMessage(null, new FacesMessage("Problem post successfully inserted!"));
			}
			if (postAs == 2) {
				if (!problemPhoto.getFileName().isEmpty()) {
					ResponseEntity<ProblemConfigDTO> reponse = studentProblemService
							.postProblemByStudent(problemConfigDTO, postAs);
					Long problemId = reponse.getBody().getProblemId();
					problemConfigDTO.setPicturePost(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
							problemPhoto, ImageType.STUDENT_PROBLEM.getCode(), studentProblemService.provideStudentId(),
							problemId));
					studentProblemService.postProblemByStudent(problemConfigDTO, postAs);
					findAllProblems();
					mass.addMessage(null, new FacesMessage("Problem post successfully inserted!"));
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			mass.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Code not problem post successfully.!", ""));
		}

		problemConfigDTO = new ProblemConfigDTO();
	}

	public String findAllProblems() {
		allProblemDTOs = studentProblemService.findAllProblems();
		for (ProblemConfigDTO problemConfigDTO : allProblemDTOs) {
			problemConfigDTO
					.setProblemPhotoBase64(ApplicationUtils.provideBase64Image(problemConfigDTO.getPicturePost()));
			problemConfigDTO.setStudentPhotoBase64(
					ApplicationUtils.provideBase64Image(problemConfigDTO.getStudent().getPhotoName()));
		}
		return "/student/find-problem.xhtml?faces-redirect=true";
	}

	private void provideBase64(ProblemConfigDTO student) {
		StudentDTO studentDTO = new StudentDTO();
		ProblemConfigDTO problemConfigDTO = new ProblemConfigDTO();

		studentDTO.setPhotoWithBase64(ApplicationUtils.provideBase64Image(student.getStudent().getPhotoName()));
		System.out.println(problemConfigDTO.getProblemPhotoBase64());

	}

	public ProblemConfigDTO getProblemConfigDTO() {
		if (problemConfigDTO == null)
			problemConfigDTO = new ProblemConfigDTO();
		return problemConfigDTO;
	}

	public void setProblemConfigDTO(ProblemConfigDTO problemConfigDTO) {
		this.problemConfigDTO = problemConfigDTO;
	}

	public List<ProblemConfigDTO> getAllProblemDTOs() {
		return allProblemDTOs;
	}

	public void setAllProblemDTOs(List<ProblemConfigDTO> allProblemDTOs) {
		this.allProblemDTOs = allProblemDTOs;
	}

	public List<GeneralInfoDTO> getDepartmentDTOs() {
		return departmentDTOs;
	}

	public void setDepartmentDTOs(List<GeneralInfoDTO> departmentDTOs) {
		this.departmentDTOs = departmentDTOs;
	}

	public UploadedFile getProblemPhoto() {
		return problemPhoto;
	}

	public void setProblemPhoto(UploadedFile problemPhoto) {
		this.problemPhoto = problemPhoto;
	}

	public List<ProblemSolutionDTO> getProblemSolutionDTOs() {
		return problemSolutionDTOs;
	}

	public void setProblemSolutionDTOs(List<ProblemSolutionDTO> problemSolutionDTOs) {
		this.problemSolutionDTOs = problemSolutionDTOs;
	}

	public ProblemSolutionDTO getProblemSolutionDTO() {
		if (problemSolutionDTO == null)
			problemSolutionDTO = new ProblemSolutionDTO();
		return problemSolutionDTO;
	}

	public void setProblemSolutionDTO(ProblemSolutionDTO problemSolutionDTO) {
		this.problemSolutionDTO = problemSolutionDTO;
	}

	public UploadedFile getSolutionPhoto1() {
		return solutionPhoto1;
	}

	public void setSolutionPhoto1(UploadedFile solutionPhoto1) {
		this.solutionPhoto1 = solutionPhoto1;
	}

	public UploadedFile getSolutionPhoto2() {
		return solutionPhoto2;
	}

	public void setSolutionPhoto2(UploadedFile solutionPhoto2) {
		this.solutionPhoto2 = solutionPhoto2;
	}

	public List<ProblemSolutionDTO> getStudentSolutionDTOs() {
		return studentSolutionDTOs;
	}

	public void setStudentSolutionDTOs(List<ProblemSolutionDTO> studentSolutionDTOs) {
		this.studentSolutionDTOs = studentSolutionDTOs;
	}

	public List<ProblemConfigDTO> getStudentProblemConfigDTOs() {
		return studentProblemConfigDTOs;
	}

	public void setStudentProblemConfigDTOs(List<ProblemConfigDTO> studentProblemConfigDTOs) {
		this.studentProblemConfigDTOs = studentProblemConfigDTOs;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public Long getShownProblemid() {
		return shownProblemid;
	}

	public void setShownProblemid(Long shownProblemid) {
		this.shownProblemid = shownProblemid;
	}

	public List<ProblemSolutionDTO> getStudentSolvedProblemDTOs() {
		return studentSolvedProblemDTOs;
	}

	public void setStudentSolvedProblemDTOs(List<ProblemSolutionDTO> studentSolvedProblemDTOs) {
		this.studentSolvedProblemDTOs = studentSolvedProblemDTOs;
	}

}
