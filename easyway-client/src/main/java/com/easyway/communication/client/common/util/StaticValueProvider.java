package com.easyway.communication.client.common.util;

public class StaticValueProvider {

	public static final String BASE_URL = "http://localhost:8081/";

	public static final String LOGIN_ADMIN_URI = "admins/";

	public static final String LOGIN_USER_URI = "users/";

	public static final String LOGIN_INSTITUTE_URI = "institutes/";

	public static final String LOGIN_TEACHER_URI = "teachers/";

	public static final String LOGIN_STUDENT_URI = "students/";

	public static final String LOGIN_STUDENT_PROBLEM_URI = "students/problem/";

	public static final String IMAGE_PATH = "D:/easylink/";

}
