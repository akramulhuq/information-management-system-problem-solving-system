package com.easyway.communication.client.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.easyway.communication.client.addressdto.DistrictDTO;
import com.easyway.communication.client.addressdto.DivisionDTO;
import com.easyway.communication.client.admin.model.GeneralInfoDTO;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.users.model.SignUpDTO;

public class InstituteApiConsumer {
	public static List<SignUpDTO> consumeSignUpDTOs(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<SignUpDTO[]> response = template.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension,
				SignUpDTO[].class);
		List<SignUpDTO> studentDTOs = Arrays.asList(response.getBody());
		return new ArrayList<>(studentDTOs);
	}

	public static List<InstituteNoticeDTO> consumeInstituteNoticeDto(String uri, String uriExtension) {
		RestTemplate template = new RestTemplate();
		ResponseEntity<InstituteNoticeDTO[]> response = template
				.getForEntity(StaticValueProvider.BASE_URL + uri + uriExtension, InstituteNoticeDTO[].class);
		return Arrays.asList(response.getBody());

	}

}
