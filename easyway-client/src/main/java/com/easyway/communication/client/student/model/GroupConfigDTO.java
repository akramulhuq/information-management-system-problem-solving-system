package com.easyway.communication.client.student.model;

import com.easyway.communication.client.institute.model.GroupDTO;

public class GroupConfigDTO {

	private Long configId;

	private String addDate;

	private int joinAs;

	private String photoWithBase64;

	private String instituteWithBase64;

	private String teacherWithBase64;

	private StudentDTO student;

	private GroupDTO group;

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public int getJoinAs() {
		return joinAs;
	}

	public void setJoinAs(int joinAs) {
		this.joinAs = joinAs;
	}

	public StudentDTO getStudent() {
		if (student == null)
			student = new StudentDTO();
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public GroupDTO getGroup() {
		if (group == null)
			group = new GroupDTO();
		return group;
	}

	public void setGroup(GroupDTO group) {
		this.group = group;
	}

	public String getPhotoWithBase64() {
		return photoWithBase64;
	}

	public void setPhotoWithBase64(String photoWithBase64) {
		this.photoWithBase64 = photoWithBase64;
	}

	public String getInstituteWithBase64() {
		return instituteWithBase64;
	}

	public void setInstituteWithBase64(String instituteWithBase64) {
		this.instituteWithBase64 = instituteWithBase64;
	}

	public String getTeacherWithBase64() {
		return teacherWithBase64;
	}

	public void setTeacherWithBase64(String teacherWithBase64) {
		this.teacherWithBase64 = teacherWithBase64;
	}

}
