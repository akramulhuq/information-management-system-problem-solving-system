package com.easyway.communication.client.teacher.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.institute.service.InstituteNoticeService;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.teacher.model.FolderDTO;
import com.easyway.communication.client.teacher.model.TeacherDTO;
import com.easyway.communication.client.teacher.model.TopicDTO;
import com.easyway.communication.client.teacher.service.TeacherInfoService;
import com.easyway.communication.client.users.model.UserDetailsDTO;
import com.lowagie.text.pdf.PdfDocument;

@Controller
@ManagedBean
@Scope("session")
public class TeacherInfoController {

	@Autowired
	private TeacherInfoService teacherInfoService;

	@Autowired
	private InstituteNoticeService instituteNoticeService;

	public UserDetailsDTO userDetailsDTO;

	public StudentDTO studentDTO;

	private TeacherDTO teacherDTO;

	private FolderDTO folderDTO;

	private TopicDTO topicDTO;

	private List<FolderDTO> folderDTOs;

	private List<TopicDTO> topicDTOs;

	private List<InstituteNoticeDTO> noticeDTOsByTeacherId;

	public UploadedFile studentImage;

	private UploadedFile topicPdf;

	private StreamedContent streamedContent;

	String pdf;

	@PostConstruct
	public void init() {
		findTeacherInfoByHttp();
		findAllFolderByTeacherId();
		findTopicByTeacher();
	}

	public String findNoticeByTeacherId() {
		noticeDTOsByTeacherId = instituteNoticeService.findNoticeByTeacherId(teacherInfoService.provideTeacherId());
		for (InstituteNoticeDTO instituteNoticeDTO : noticeDTOsByTeacherId) {
			instituteNoticeDTO
					.setNoticePhotoBase64(ApplicationUtils.provideBase64Image(instituteNoticeDTO.getNoticeWithPhoto()));
			instituteNoticeDTO.setTeacherPhotoBase64(
					ApplicationUtils.provideBase64Image(instituteNoticeDTO.getTeacher().getPhotoName()));
		}
		return "/teacher/teacher-notices.xhtml?faces-redirect=true";
	}

	public void addTopicByTeacher() {
		try {
			if (!topicPdf.getFileName().isEmpty()) {
				topicDTO.setTopicName(ApplicationUtils.saveTopic(StaticValueProvider.IMAGE_PATH, topicPdf,
						topicDTO.getTopicTitle(), teacherInfoService.provideTeacherId()));
			}
			teacherInfoService.addTopicByTeacher(topicDTO, folderDTO);
			findTopicByTeacher();
		} catch (Exception e) {
			System.out.println();
		}
	}

	public String findTopicByTeacher() {
		try {
			topicDTOs = teacherInfoService.findTopicByTeacher(folderDTO.getForlderId());
			for (TopicDTO topicDTO : topicDTOs) {
				topicDTO.setStreamedContent(createStream(topicDTO.getTopicName()));
				StreamedContent streamedContent = createStream(topicDTO.getTopicName());
				topicDTO.setInputStream(streamedContent.getStream());
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return "/teacher/view-curriculum.xhtml?faces-redirect=true";
	}

	private StreamedContent createStream(String fileName) throws FileNotFoundException, URISyntaxException {
		File file;
		file = new File("D:/easylink/" + fileName);
		InputStream inputStream = new FileInputStream(file);
		StreamedContent streamedContent = new DefaultStreamedContent(inputStream, "application/pdf", fileName);
		return streamedContent;
	}

	private InputStream getData(String fileName) {
		File file = new File("D:/easylink/" + fileName);
		InputStream is = null;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return is;

	}

	public void createFolderByTeacher() {
		try {
			teacherInfoService.createFolderByTeacher(folderDTO);
			findAllFolderByTeacherId();
			folderDTO = new FolderDTO();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findAllFolderByTeacherId() {
		try {
			folderDTOs = teacherInfoService.findAllFolderByTeacherId(teacherInfoService.provideTeacherId());
		} catch (Exception e) {
			System.out.println(e);
		}

		return "/teacher/add-curriculum.xhtml?faces-redirect=true";
	}

	public void findTeacherInfoByHttp() {
		try {
			userDetailsDTO = new UserDetailsDTO();
			userDetailsDTO = teacherInfoService.findTeacherInfoByHttp();
			userDetailsDTO
					.setLogoWithBase64(ApplicationUtils.provideBase64Image(userDetailsDTO.getTeacher().getPhotoName()));
		} catch (Exception e) {

		}
	}

	/* This is Method is Created By Akramul Huq Atif */
	public String findTeacherInfos() {
		try {
			userDetailsDTO = teacherInfoService.findTeacherInfoByHttp();
			userDetailsDTO
					.setLogoWithBase64(ApplicationUtils.provideBase64Image(userDetailsDTO.getTeacher().getPhotoName()));
		} catch (Exception e) {
			System.out.println(e);
		}
		return "/teacher/profile.xhtml?faces-redirect=true";
	}

	public UserDetailsDTO getUserDetailsDTO() {
		if (userDetailsDTO == null) {
			userDetailsDTO = new UserDetailsDTO();
		}
		return userDetailsDTO;
	}

	public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
		this.userDetailsDTO = userDetailsDTO;
	}

	public FolderDTO getFolderDTO() {
		if (folderDTO == null)
			folderDTO = new FolderDTO();
		return folderDTO;
	}

	public void setFolderDTO(FolderDTO folderDTO) {
		this.folderDTO = folderDTO;
	}

	public TopicDTO getTopicDTO() {
		if (topicDTO == null)
			topicDTO = new TopicDTO();
		return topicDTO;
	}

	public void setTopicDTO(TopicDTO topicDTO) {
		this.topicDTO = topicDTO;
	}

	public List<FolderDTO> getFolderDTOs() {
		return folderDTOs;
	}

	public void setFolderDTOs(List<FolderDTO> folderDTOs) {
		this.folderDTOs = folderDTOs;
	}

	public List<TopicDTO> getTopicDTOs() {
		return topicDTOs;
	}

	public void setTopicDTOs(List<TopicDTO> topicDTOs) {
		this.topicDTOs = topicDTOs;
	}

	public UploadedFile getTopicPdf() {
		return topicPdf;
	}

	public void setTopicPdf(UploadedFile topicPdf) {
		this.topicPdf = topicPdf;
	}

	public TeacherDTO getTeacherDTO() {
		return teacherDTO;
	}

	public void setTeacherDTO(TeacherDTO teacherDTO) {
		this.teacherDTO = teacherDTO;
	}

	public UploadedFile getStudentImage() {
		return studentImage;
	}

	public void setStudentImage(UploadedFile studentImage) {
		this.studentImage = studentImage;
	}

	public List<InstituteNoticeDTO> getNoticeDTOsByTeacherId() {
		return noticeDTOsByTeacherId;
	}

	public void setNoticeDTOsByTeacherId(List<InstituteNoticeDTO> noticeDTOsByTeacherId) {
		this.noticeDTOsByTeacherId = noticeDTOsByTeacherId;
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

	public String getPdf() {
		pdf = ApplicationUtils.provideBase64Image("object-26.pdf");
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

}
