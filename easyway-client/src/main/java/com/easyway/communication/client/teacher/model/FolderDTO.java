package com.easyway.communication.client.teacher.model;

import org.primefaces.model.StreamedContent;

public class FolderDTO {

	private Long forlderId;

	private String folderName;

	private String createDate;

	private TeacherDTO teacher;
	
	private StreamedContent streamedContent;

	public Long getForlderId() {
		return forlderId;
	}

	public void setForlderId(Long forlderId) {
		this.forlderId = forlderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public TeacherDTO getTeacher() {
		if (teacher == null)
			teacher = new TeacherDTO();
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

}
