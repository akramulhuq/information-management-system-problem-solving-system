package com.easyway.communication.client.student.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.easyway.communication.client.common.enums.ImageType;
import com.easyway.communication.client.common.util.ApplicationUtils;
import com.easyway.communication.client.common.util.StaticValueProvider;
import com.easyway.communication.client.institute.model.GroupDTO;
import com.easyway.communication.client.institute.model.GroupNoticeDTO;
import com.easyway.communication.client.institute.model.InstituteDTO;
import com.easyway.communication.client.institute.model.InstituteNoticeDTO;
import com.easyway.communication.client.institute.service.InstituteInfoService;
import com.easyway.communication.client.institute.service.InstituteNoticeService;
import com.easyway.communication.client.student.model.GroupConfigDTO;
import com.easyway.communication.client.student.model.StudentDTO;
import com.easyway.communication.client.student.service.StudentInfoService;
import com.easyway.communication.client.student.service.StudentNoticeService;
import com.easyway.communication.client.student.service.StudentProblemService;
import com.easyway.communication.client.users.model.UserDetailsDTO;

@Controller
@ManagedBean
@Scope("session")
public class StudentINoticeController {

	@Autowired
	private InstituteNoticeService instituteNoticeService;

	@Autowired
	private StudentProblemService studentProblemService;

	@Autowired
	private StudentInfoService studentInfoService;

	private List<InstituteNoticeDTO> instituteNoticeDTOs;

	private List<GroupDTO> groupDTOs;

	private List<GroupDTO> likedGroupDTOs;

	private List<GroupConfigDTO> groupConfigDTOs;

	private List<GroupNoticeDTO> groupNoticeDTOs;

	private GroupDTO groupDTO;

	private GroupConfigDTO groupConfigDTO;

	private GroupNoticeDTO groupNoticeDTO;

	private UserDetailsDTO userDetailsDTO;

	private UploadedFile groupNoticePhoto;

	public void postGroupNotice(int postAs) {
		try {
			InstituteDTO instituteDTO = new InstituteDTO();
			if (postAs == 1) {
				instituteDTO.setInstituteId(groupConfigDTO.getGroup().getInstitute().getInstituteId());
				StudentDTO studentDTO = studentInfoService.findStudentInfoByHttp().getStudent();
				groupNoticeDTO.setStudent(studentDTO);
				groupNoticeDTO.setInstitute(instituteDTO);
				groupNoticeDTO.setGroup(groupConfigDTO.getGroup());
				instituteNoticeService.postGroupNotice(groupNoticeDTO);
				findGroupNoticeById();
				groupNoticeDTO = new GroupNoticeDTO();
			} else if (postAs == 2) {
				instituteDTO.setInstituteId(groupConfigDTO.getGroup().getInstitute().getInstituteId());
				StudentDTO studentDTO = studentInfoService.findStudentInfoByHttp().getStudent();
				groupNoticeDTO.setStudent(studentDTO);
				groupNoticeDTO.setInstitute(instituteDTO);
				groupNoticeDTO.setGroup(groupConfigDTO.getGroup());
				ResponseEntity<GroupNoticeDTO> responseEntity = instituteNoticeService
						.saveGrpNoticeWithOutPhto(groupNoticeDTO);
				groupNoticeDTO.setNoticeWithPhoto(ApplicationUtils.saveImage(StaticValueProvider.IMAGE_PATH,
						groupNoticePhoto, ImageType.GROUP_NOTICE_PHOTO.getCode(),
						instituteNoticeService.provideInstituteId(), responseEntity.getBody().getNoticeId()));
				instituteNoticeService.postGroupNotice(groupNoticeDTO);
				findGroupNoticeById();
				groupNoticeDTO = new GroupNoticeDTO();
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findGroupNoticeById() {
		groupNoticeDTOs = studentProblemService.findGroupNoticeByGrpId(groupConfigDTO);
		for (GroupNoticeDTO groupNoticeDTO : groupNoticeDTOs) {
			groupNoticeDTO
					.setNoticePhotoBase64(ApplicationUtils.provideBase64Image(groupNoticeDTO.getNoticeWithPhoto()));
			groupNoticeDTO.setTeacherPhotoBase64(
					ApplicationUtils.provideBase64Image(groupNoticeDTO.getTeacher().getPhotoName()));
			groupNoticeDTO.setInstitutePhotoBase64(
					ApplicationUtils.provideBase64Image(groupNoticeDTO.getInstitute().getLogoPhoto()));
			groupNoticeDTO.setStudentPhotoBase64(
					ApplicationUtils.provideBase64Image(groupNoticeDTO.getStudent().getPhotoName()));
		}
		return "/student/group-notice.xhtml?faces-redirect=true";
	}

	public void joinOrUnJoinGroup(int joinAs) {
		try {
			studentProblemService.joinOrUnJoinGroup(groupDTO, joinAs);
			findInstituteGroup();
			groupDTO = null;
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String findInstituteGroup() {
		try {
			groupConfigDTOs = studentProblemService.findGroupConfigByStdnt();
			groupDTOs = studentProblemService.findInstituteGroupByStdnt().stream()
					.map(joisAs -> provideJoinAs(joisAs, groupConfigDTOs)).collect(Collectors.toList());

		} catch (Exception e) {
			System.out.println(e);
		}
		return "/student/group.xhtml?faces-redirect=true";
	}

	private GroupDTO provideJoinAs(GroupDTO groupDTO, List<GroupConfigDTO> groupConfigDTOs) {
		GroupDTO group = new GroupDTO();
		BeanUtils.copyProperties(groupDTO, group);
		GroupConfigDTO groupConfigDTO = groupConfigDTOs.stream()
				.filter(join -> join.getGroup().getGroupId().equals(groupDTO.getGroupId())).findFirst()
				.orElse(new GroupConfigDTO());
		if (groupConfigDTO.getJoinAs() == 1 && groupConfigDTO != null) {
			group.setJoin(groupConfigDTO.getJoinAs());
		} else {
			group.setJoin(0);
		}
		return group;
	}

	public String findInstituteNoticeById() {
		try {
			instituteNoticeDTOs = instituteNoticeService
					.findInstituteNoticeById(userDetailsDTO.getStudent().getInstitute().getInstituteId());

		} catch (Exception e) {
			System.out.println(e);
		}

		return "/institute/notice.xhtml?faces-redirect=true";
	}

	public UserDetailsDTO getUserDetailsDTO() {
		if (userDetailsDTO == null) {
			userDetailsDTO = new UserDetailsDTO();
		}
		return userDetailsDTO;
	}

	public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
		this.userDetailsDTO = userDetailsDTO;
	}

	public List<InstituteNoticeDTO> getInstituteNoticeDTOs() {
		return instituteNoticeDTOs;
	}

	public void setInstituteNoticeDTOs(List<InstituteNoticeDTO> instituteNoticeDTOs) {
		this.instituteNoticeDTOs = instituteNoticeDTOs;
	}

	public List<GroupDTO> getGroupDTOs() {
		return groupDTOs;
	}

	public void setGroupDTOs(List<GroupDTO> groupDTOs) {
		this.groupDTOs = groupDTOs;
	}

	public GroupDTO getGroupDTO() {
		if (groupDTO == null)
			groupDTO = new GroupDTO();
		return groupDTO;
	}

	public void setGroupDTO(GroupDTO groupDTO) {
		this.groupDTO = groupDTO;
	}

	public List<GroupConfigDTO> getGroupConfigDTOs() {
		return groupConfigDTOs;
	}

	public void setGroupConfigDTOs(List<GroupConfigDTO> groupConfigDTOs) {
		this.groupConfigDTOs = groupConfigDTOs;
	}

	public List<GroupDTO> getLikedGroupDTOs() {
		return likedGroupDTOs;
	}

	public void setLikedGroupDTOs(List<GroupDTO> likedGroupDTOs) {
		this.likedGroupDTOs = likedGroupDTOs;
	}

	public List<GroupNoticeDTO> getGroupNoticeDTOs() {
		return groupNoticeDTOs;
	}

	public void setGroupNoticeDTOs(List<GroupNoticeDTO> groupNoticeDTOs) {
		this.groupNoticeDTOs = groupNoticeDTOs;
	}

	public GroupConfigDTO getGroupConfigDTO() {
		if (groupConfigDTO == null)
			groupConfigDTO = new GroupConfigDTO();
		return groupConfigDTO;
	}

	public void setGroupConfigDTO(GroupConfigDTO groupConfigDTO) {
		this.groupConfigDTO = groupConfigDTO;
	}

	public GroupNoticeDTO getGroupNoticeDTO() {
		if (groupNoticeDTO == null) {
			groupNoticeDTO = new GroupNoticeDTO();
		}
		return groupNoticeDTO;
	}

	public void setGroupNoticeDTO(GroupNoticeDTO groupNoticeDTO) {
		this.groupNoticeDTO = groupNoticeDTO;
	}

	public UploadedFile getGroupNoticePhoto() {
		return groupNoticePhoto;
	}

	public void setGroupNoticePhoto(UploadedFile groupNoticePhoto) {
		this.groupNoticePhoto = groupNoticePhoto;
	}

}
