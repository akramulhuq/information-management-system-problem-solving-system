package com.easyway.communication.client.common.enums;

public enum GeneralInfoType {

	DESIGNATION("1100", "Designation"), DEPARTMENT("2200", "Department");

	private final String code;
	private final String typeName;

	GeneralInfoType(String code, String typeName) {
		this.code = code;
		this.typeName = typeName;
	}

	public String getCode() {
		return code;
	}

	public String getTypeName() {
		return typeName;
	}

}
