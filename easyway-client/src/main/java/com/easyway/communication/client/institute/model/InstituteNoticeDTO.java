package com.easyway.communication.client.institute.model;

import com.easyway.communication.client.teacher.model.TeacherDTO;

public class InstituteNoticeDTO {

	private Long noticeId;

	private String noticType;

	private String noticeDate;

	private String noticeTitle;

	private String noticeBody;

	private String noticeWithPhoto;

	private int activeStatus;

	private TeacherDTO teacher;

	private InstituteDTO institute;

	private String noticePhotoBase64;

	private String teacherPhotoBase64;

	private String institutePhotoBase64;

	public Long getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getNoticType() {
		return noticType;
	}

	public void setNoticType(String noticType) {
		this.noticType = noticType;
	}

	public String getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeBody() {
		return noticeBody;
	}

	public void setNoticeBody(String noticeBody) {
		this.noticeBody = noticeBody;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public TeacherDTO getTeacher() {
		if (teacher == null)
			teacher = new TeacherDTO();
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public InstituteDTO getInstitute() {
		if (institute == null)
			institute = new InstituteDTO();
		return institute;
	}

	public void setInstitute(InstituteDTO institute) {
		this.institute = institute;
	}

	public String getNoticeWithPhoto() {
		return noticeWithPhoto;
	}

	public void setNoticeWithPhoto(String noticeWithPhoto) {
		this.noticeWithPhoto = noticeWithPhoto;
	}

	public String getNoticePhotoBase64() {
		return noticePhotoBase64;
	}

	public void setNoticePhotoBase64(String noticePhotoBase64) {
		this.noticePhotoBase64 = noticePhotoBase64;
	}

	public String getTeacherPhotoBase64() {
		return teacherPhotoBase64;
	}

	public void setTeacherPhotoBase64(String teacherPhotoBase64) {
		this.teacherPhotoBase64 = teacherPhotoBase64;
	}

	public String getInstitutePhotoBase64() {
		return institutePhotoBase64;
	}

	public void setInstitutePhotoBase64(String institutePhotoBase64) {
		this.institutePhotoBase64 = institutePhotoBase64;
	}

}
